---
layout: page
title: Documentation
permalink: /documentation/
---

This page is an entry point to the ns-3 documentation maintained by the project (other tutorials exist on external blogs and on YouTube but are not typically listed here).

Documentation is available for the [current release](/releases/latest/), [older releases](/documentation/older/), and also our [development tree](/documentation/development-tree/).

## Getting started

*   **Tutorial:** The tutorial (including a quick-start installation guide) for our latest release is available in **[HTML](/docs/release/3.44/tutorial/html/index.html)** and **[PDF](/docs/release/3.44/tutorial/ns-3-tutorial.pdf)** formats.
*   **Installation:** We recently added a new installation guide, in
**[HTML](/docs/release/3.44/installation/html/index.html)** and **[PDF](/docs/release/3.44/installation/ns-3-installation.pdf)** formats.
*   **Models:** The ns-3 model library documentation (latest release), in
**[HTML](/docs/release/3.44/models/html/index.html)** and **[PDF](/docs/release/3.44/models/ns-3-model-library.pdf)** formats.
*   **Mailing lists:** We have several [mailing lists](/support/mailing-lists/), but in particular, the [ns-3-users Google Group forum](https://groups.google.com/forum/#!forum/ns-3-users), answers many questions from people trying to get started.
*   **Chat:** ns-3 discussions also take place in a [Zulip](https://ns-3.zulipchat.com) chatroom.  We do not use Discord, Slack, or IRC, but Zulip instead.
*   **Videos:** The ns-3 Consortium has offered training and tutorial sessions at its [annual meeting](/research/wns3/), and recorded videos from prior sessions can be found from [this training page](/consortium/activities/training/).  Note that due to the pandemic, in-person training sessions have not been offered since 2019.  For some additional recorded sessions, please see also the [videos](/documentation/videos/) page.

## Development

Most users will need to write new simulation scripts and possibly modify or extend the ns-3 libraries to conduct their work. The three main resources for this are our reference manual, model library documentation, and our Doxygen.

*   We maintain a **reference manual** on the ns-3 core, and a separate **model library** documentation set, also in [several formats](/ns-3-38/documentation/) for our latest release.
*   All of our APIs are documented using [**Doxygen**](/docs/release/3.44/doxygen/index.html)
*   The ns-3 [coding style](/docs/contributing/html/coding-style.html) documentation is maintained on this site.

## Technical reports

Some technical reports about the ns-3 models are listed below.

### Wi-Fi models

* [Technical report on ns-3 IEEE 802.11ax models](https://depts.washington.edu/funlab/wp-content/uploads/2018/11/11ax-final-report.pdf) Note: ns-3 802.11ax code discussed in this report is in preparation for future public release.
* [Validation of Wi-Fi DCF in saturation](https://depts.washington.edu/funlab/wp-content/uploads/2015/03/ns3-TR.pdf)
* [Validation of Wi-Fi 802.11n error model](https://depts.washington.edu/funlab/wp-content/uploads/2017/05/Technical-report-on-validation-of-error-models-for-802.11n.pdf)
* [Validation of Wi-Fi OFDM error model](https://www.nsnam.org/~pei/80211ofdm.pdf)
* [Validation of Wi-Fi 802.11b error model](https://www.nsnam.org/~pei/80211b.pdf)

## Related projects

*   **Direct Code Execution**: Documentation on the ns-3 [Direct Code Execution](/about/projects/direct-code-execution) environment is also linked from this site.
*   **[Netanim](/wiki/NetAnim)**: A network animator for ns-3.
*   **[Bake](/docs/bake/tutorial/html/)**: The package management tool for advanced ns-3 builds.
*   **[ns-2](/support/faq/ns2-ns3/)**

## Miscellaneous

Not finding what you are looking for? Have a look at the [ns-3 wiki](/wiki), where such topics as current development, release planning, summer projects, contributed code, FAQs and HOWTOs. Not everything is linked from the masthead so try entering keywords in the search box.

We have a number of [**other archived documents**](/documentation/presentations/) such as older tutorials or talks presented about ns-3.
