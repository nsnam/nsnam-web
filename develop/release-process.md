---
title: Release process
layout: page
permalink: /develop/release-process/
---
ns-3 releases are based on date-driven schedules: rather than target a set of features for a specific version number, we aim instead for a release date and ship whatever is ready by that date. Sometimes releases are delayed, but we aim to release three times per year (January, May, September).

An ns-3 release manager manages each release. Following the ns-3.X release, a new release manager is selected for the ns-3.X+1 release. The old ns-3.X release manager is typically responsible for any maintenance ns-3.X.Y releases. The Release Manager is allowed to veto and remove any new feature addition if it begins to cause problems and looks like it threatens the stability of the release at any time in the release process.

Each release is roughly structured as follows: large major intrusive changes that are the most destabilizing to the codebase should be merged first early on during the cycle. As we approach the release deadline, the number and the size of the changes should decrease until only high-priority bugs are fixed during the _coding freeze_ period.  Release candidates are published for final testing.

[<img alt="The ns-3 release process" class="size-full wp-image-350" height="294" src="http://www.nsnam.org/wp-content/uploads/2011/01/release-process.png" title="The ns-3 release process" width="510" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2011/01/release-process.png 510w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2011/01/release-process-300x172.png 300w" sizes="(max-width: 510px) 100vw, 510px" />](http://www.nsnam.org/wp-content/uploads/2011/01/release-process.png)
