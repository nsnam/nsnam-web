---
title: Mailing lists
layout: page
permalink: /develop/tools/mailing-lists/
---

People interested in ns-3 development should subscribe to and read the ns-developers mailing list, a Google Group since November 2022: [Google Groups page](https://groups.google.com/g/ns-developers)

For user-oriented mail (getting help with using ns-3) there is also the [ns-3-users Google Groups](https://groups.google.com/forum/?fromgroups#!forum/ns-3-users) page mailing list.

A few additional developer-oriented lists predate our use of GitLab notifications, but the archives may still be useful:

* ns-3-reviews: ns-3 code reviews. [Archives](http://groups.google.com/group/ns-3-reviews)
