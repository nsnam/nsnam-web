FROM ruby:latest

WORKDIR /app

WORKDIR /app-build
COPY Gemfile ./
RUN bundle install

EXPOSE 4000
ENTRYPOINT [ \
    "bundle", "exec", "jekyll", "serve", \
    "--disable-disk-cache", \
    "--source", "/app", \
    "--host", "0.0.0.0" \
    ]
