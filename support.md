---
title: Support
layout: page
permalink: /support/
---
ns-3 is an open source project maintained by volunteers, not backed by any specific company.  Most help to users is provided through interaction on mailing lists, Zulip chat, or through the issue tracker.  The first line of support is to search our users mailing list, Stack Overflow, issue tracker, or just make a generic web search to try to answer your question.  If you have questions that are not answered here or by searching, consider looking at our fairly extensive [documentation](/documentation) or asking your question on our [ns-3-users mailing-list](/support/mailing-lists).

