---
layout: post
title:  "ns-3.33 released"
date:   2021-01-09 10:28:00 -0800
categories: news
excerpt_separator: <!--more-->
---

The ns-3.33 release is now available, due to contributions from [twenty three authors](https://www.nsnam.org/releases/ns-3-33/authors/).  ns-3.33 contains the following new features:
 * TCP CUBIC congestion control, and alignment of ECN handling with Linux behavior
 * New error models for Wi-Fi, based on link simulation results
 * A channel condition model for vehicular scenarios
<!--more-->
 * Wi-Fi 802.11ax PHY layer models for HE MU and HE RU
 * A new Length class to represent lengths (distances) with an explicit unit
 * A CsvReader class to input node positions from a csv file
 * An option to configure the Wi-Fi OFDM transmit spectral mask
 * LR-WPAN models handle differences between RFC4944- and RFC6282-style IPv6 address expansion.
 * Support for user-provided MPI_Communicator for MPI distributed simulation
The release includes numerous bug fixes and small improvements, listed in the [RELEASE_NOTES](https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.33/RELEASE_NOTES).
