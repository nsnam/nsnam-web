---
id: 3729
title: ns-3 participating in SOCIS 2017
date: 2017-05-26T14:42:30+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3729
permalink: /news/ns-3-participating-in-socis-2017/
categories:
  - News
---
ns-3 has been invited to participate in the 2017 edition of [ESA Summer of Code in Space](http://sophia.estec.esa.int/socis/). This summer coding program is intended to improve space-related open source software. Interested students must submit an application through the form available at the above link, by 31 May.
