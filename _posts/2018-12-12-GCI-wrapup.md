---
layout: post
title:  "Google Code-In Wrap-up"
date:   2018-12-12 12:58:24 -0700
categories: news
excerpt_separator: <!--more-->
---
The seven-week [Google Code-In 2018 contest](https://codein.withgoogle.com) wrapped up today, with 380 tasks successfully completed by 46 students.  Thanks are due to **Mohit Tahiliani** for leading ns-3's effort, for the many mentors who guided the work, and to the many student contributors whose efforts will be folded into ns-3.
