---
layout: post
title:  "WNS3 2018"
date:   2017-11-17 13:55:18 -0700
categories: news
excerpt_separator: <!--more-->
---

The Workshop on ns-3 (WNS3) is a one and one-half day workshop to be held on June 13-14, 2018, hosted by the National Institute of Technology Karnataka (NITK) Surathkal, Mangalore, India.<!--more--> The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities. The Call for Papers is published on a separate page, and an annual meeting flyer is available.
