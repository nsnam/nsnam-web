---
id: 3972
title: Google Summer of Code wrapup
date: 2018-08-24T00:01:07+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3972
permalink: /news/google-summer-of-code-wrapup-2/
categories:
  - News
---
Google Summer of Code has formally concluded and we are happy to announce that all five of our students have passed and completed part or all of their proposed projects. Once ns-3.29 is released, we will move towards merging or making available their contributions, and in some cases, the students are continuing the project. Below are brief summaries and links to their work.

  * ECN is a hot topic in Internet transport protocols, and Wenying Dai added two new ECN features (AccECN and ECN++) to our internet module. We will work towards merging this code to the mainline. Wenying&#8217;s final report is [here](https://ecnpp-accecn-gsoc-2018.github.io/)
  * Muhammad Iqbal CR has ported the LAA/Wi-Fi coexistence code to the latest ns-3-dev and, in so doing, made some mainline improvements to the wifi module. Some code has already been merged to ns-3-dev; we&#8217;ll make this updated LAA/Wi-Fi coexistence module available in our app store after ns-3.29. Muhammad&#8217;s final report is [here](https://www.nsnam.org/wiki/GSOC2018Coexistence#Project_Summary_and_Final_Report)
  * Sourabh Jain has been working on DCE upgrades to allow the Linux Kernel Library (LKL) to be used with DCE, with the hope that future DCE maintenance will be easier after such a change. Work is ongoing after GSoC to finish this capability. Sourabh&#8217;s report can be found [here](https://ns-3-dce-gsoc-2018.github.io/)
  * Davide Magrin wrote a new Python library to orchestrate the execution of an ns-3 simulation campaign. We can publish this extension through our app store shortly. Davide&#8217;s library [documentation](https://simulationexecutionmanager.readthedocs.io/en/develop/).
  * Jude Niroshan worked on a trust-based routing framework for MANET routing, and its application to the AODV routing protocol. Tommaso plans to continue work on this extension before making it available through the app store in the future. Jude&#8217;s final report is found [here](https://medium.com/@jude.niroshan11/gsoc-2018-trust-based-routing-protocols-framework-in-ns-3-dffeee692c24)
