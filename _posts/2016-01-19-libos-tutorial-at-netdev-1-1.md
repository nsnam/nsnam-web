---
id: 3364
title: libOS tutorial at netdev 1.1
date: 2016-01-19T21:32:06+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3364
permalink: /events/libos-tutorial-at-netdev-1-1/
categories:
  - Events
---
Hajime Tazaki will give a <a href="http://netdevconf.org/1.1/tutorial-libos-regression-test-framework-linux-networking-hajime-tazaki.html" target="_blank">tutorial on libOS</a> at the upcoming <a href="http://netdevconf.org/1.1/index.html" target="_blank">netdev 1.1 conference</a> in Seville. <a href="https://github.com/thehajime/blog/issues/1" target="_blank">libOS</a> is a generalization of the <a href="https://www.nsnam.org/about/projects/direct-code-execution/" target="_blank">ns-3 Direct Code Execution framework</a> allowing users to run applications and the Linux networking kernel in ns-3 simulations.

**Update:** [Archived video](https://www.youtube.com/watch?v=FNGkMFmboEc)
