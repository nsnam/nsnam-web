---
layout: post
title:  "ns-3.28 released"
date:   2018-03-22 13:55:18 -0700
categories: news
---

ns-3.28 was released on 22 March 2018 and features the following additions:

* IPv6 support for LTE: LTE UEs can now use IPv6 to send and receive traffic.
* TCP Pacing and TCP Low Priority (LP): Added TCP-LP congestion control, and added the core features of TCP Pacing.
* IP support for UAN: The UAN module now supports an IP stack, and added some examples for running raw, IPv4, IPv6, and 6LoWPAN over UAN.
* Extended addressing for LR-WPAN: Extended addressing mode is now supported.
* FIFO and TBF queue discs: Added a FIFO and Token Bucket Filter queue disc for the traffic control module.

Finally, the release includes numerous bug fixes and small improvements, listed in the [RELEASE_NOTES](#).
