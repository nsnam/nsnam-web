---
title: ns-3 GSoC students announced
date: 2020-05-04T18:00:00+00:00
author: tomh
layout: post
permalink: /news/ns-3-gsoc-students-announced-2020/
categories:
  - News
---
We are pleased to announce that four students will join the ns-3 project for the [2020 Google Summer of Code](https://www.nsnam.org/wiki/Summer_Projects#Google_Summer_of_Code_2020)!

The selected students and projects are:

* Shivamani Patil, App Store improvements
* Ananthakrishan S, NetDevice up/down consistency and event chain
* Bhaskar Kataria, SCE AQMs and TCP along with CNQ-CodelAF and LFQ
* Deepak K, TCP Prague model for ns-3

The following people have volunteered to mentor these projects:  Zoraze Ali, Abhijith Anilkumar, Ankit Deepak, Tom Henderson, Vivek Jain, Viyom Mittal, Tommaso Pecorella, Mohit Tahiliani, and Joe Dizhi Zhou.
