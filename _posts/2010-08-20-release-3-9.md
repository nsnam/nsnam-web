---
id: 474
title: Release 3.9
date: 2010-08-20T07:59:16+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=474
permalink: /news/release-3-9/
categories:
  - News
  - ns-3 Releases
---
## Availability

This release is immediately available from [here](http://www.nsnam.org/release/ns-allinone-3.9.tar.bz2).

## Supported platforms

ns-3.9 has been tested on the following platforms:

  * Ubuntu 10.04 LTS 32/64-bit gcc-4.4.3
  * Fedora Core 12 64-bit gcc-4.4.3
  * Ubuntu 9.10 32-bit gcc-4.4.1
  * Ubuntu 8.04 LTS 64-bit gcc-4.2.4
  * Fedora Core 5 32-bit gcc-4.1.1
  * Mac OS X Leopard powerpc gcc-4.0.1
  * Windows Cygwin 1.7.5

Not all ns-3 options are available on all platforms; consult the [Installation](/wiki/index.php/Installation) page for more information

## New user-visible features

  * A reorganisation of the construction of Wi-Fi transmit rates has been undertaken with the aim of simplifying the task of supporting further IEEE 802.11 PHYs. This work has been completed under the auspices of Bug 871. A consequence of this work is that WifiModes have a new (hopefully future-proof) naming, and simulations which specify rates by name will need updating. The list of new rate names is given in the error message produced when an attempt is made to use any invalid rate name (including those that were previously valid).
  * A new OFDM error rate model for WiFi (NistErrorRateModel); this model has been validated in clear-channel testbed tests. For 802.11b, it uses the same underlying model as the YansErrorRateModel, but it differs from YansErrorRateModel for OFDM modes (802.11a/g). The default YansWifiPhyHelper has been changed to use this model.
  * A new NS-2 mobility trace reader supports BonnMotion, SUMO, TraNS, etc. traces.
  * An energy model for nodes and devices, including an energy source model and device energy models allowing energy-aware devices to notify the energy source about energy consumption.
  * [Rocketfuel](http://www.cs.washington.edu/research/networking/rocketfuel/) topology dataset support for existing topology reader
  * Underwater Acoustic Network (UAN) model, split in to three parts, the channel, PHY, and MAC models to enable researchers to model a variety of underwater network scenarios.
  * the Spectrum framework, which provides support for spectrum-aware Channel and PHY layer modeling. It features a set of classes that allows the mathematical representation of frequency dependent phenomena. Some example channel, propagation and device implementations that make use of this framework are also included.

## Bugs fixed

The following lists many of the bugs fixed or small feature additions since ns-3.8, in many cases referencing the Bugzilla bug number

  * IPv4 global routing code; stub networks were assumed to be /24 and added as /24 networks even if the mask is actually different
  * OLSR was generating messages on non-OLSR interfaces
  * ICMPv4 objects not properly registered; affected serialization code
  * bug [231](/bugzilla/show_bug.cgi?id=231) &#8211; SocketAddressTag needs to be removed from a packet before forwarding the packet to the user
  * bug [385](/bugzilla/show_bug.cgi?id=385) &#8211; Add a generic "sequence number" class
  * bug [473](/bugzilla/show_bug.cgi?id=473) &#8211; [PATCH] Alternative ns-2 trace reader
  * bug [665](/bugzilla/show_bug.cgi?id=665) &#8211; Need Tutorial Sectino on Config Path and Tracing Use
  * bug [671](/bugzilla/show_bug.cgi?id=671) &#8211; add packet-info-tag.cc for IP\_PKTINFO/IPV6\_PKTINFO
  * bug [702](/bugzilla/show_bug.cgi?id=702) &#8211; Global routing doesn't respond to interface events
  * bug [799](/bugzilla/show_bug.cgi?id=799) &#8211; Interference helper is too slow
  * bug [800](/bugzilla/show_bug.cgi?id=800) &#8211; Erroneous packet sender while using aloha or cw mac in uan module
  * bug [802](/bugzilla/show_bug.cgi?id=802) &#8211; Minstrel algorithm causes segmentation fault
  * bug [826](/bugzilla/show_bug.cgi?id=826) &#8211; Using uint64_t instead of Time in DcfManager
  * bug [828](/bugzilla/show_bug.cgi?id=828) &#8211; PacketSocket::Close does not unregister protocol handler
  * bug [842](/bugzilla/show_bug.cgi?id=842) &#8211; ns-3-dev crashes using block acks
  * bug [843](/bugzilla/show_bug.cgi?id=843) &#8211; Most wifi examples change BeaconInterval to unrealistic values
  * bug [844](/bugzilla/show_bug.cgi?id=844) &#8211; YansWifiPhy::GetPowerDbm off-by-one problem when calculating Tx power
  * bug [846](/bugzilla/show_bug.cgi?id=846) &#8211; packet.cc triggers deprecated warning
  * bug [853](/bugzilla/show_bug.cgi?id=853) &#8211; Rates for Wi-Fi control responses are incorrectly selected
  * bug [854](/bugzilla/show_bug.cgi?id=854) &#8211; Support DROP_QUEUE reason-code in Ipv4FlowProbe
  * bug [858](/bugzilla/show_bug.cgi?id=858) &#8211; support MSG_PEEK in IPv4/IPv6 raw socket
  * bug [861](/bugzilla/show_bug.cgi?id=861) &#8211; Forwarding drops (due to no route found) were not being logged in IPv4 or IPv6 ascii traces
  * bug [869](/bugzilla/show_bug.cgi?id=869) &#8211; suggested test framework enhancements
  * bug [871](/bugzilla/show_bug.cgi?id=871) &#8211; naming for WifiPhyStandard
  * bug [873](/bugzilla/show_bug.cgi?id=873) &#8211; Queue occupancy counter not decremented in WifiMacQueue::Remove()
  * bug [874](/bugzilla/show_bug.cgi?id=874) &#8211; wrong modulation type is selected in the forwardBurst method
  * bug [875](/bugzilla/show_bug.cgi?id=875) &#8211; "frame includes FCS" flag should be set in Radiotap frame header
  * bug [879](/bugzilla/show_bug.cgi?id=879) &#8211; source address selection for AODV using DeferredRouteRequest
  * bug [881](/bugzilla/show_bug.cgi?id=881) &#8211; Reorganise to allow wider use of WifiInformationElement
  * bug [890](/bugzilla/show_bug.cgi?id=890) &#8211; several rate adaptation algorithms not tested by test.py
  * bug [900](/bugzilla/show_bug.cgi?id=900): RawTextConfigLoad::Default does not load configurations
  * bug [901](/bugzilla/show_bug.cgi?id=901) &#8211; Optimize Mac48Address < != and ==
  * bug [902](/bugzilla/show_bug.cgi?id=902) &#8211; TCP: handle out-of-order packets during connection shutdown
  * bug [905](/bugzilla/show_bug.cgi?id=905) &#8211; WimaxNetDevice loses packet uid, tags, and memory optimization
  * bug [906](/bugzilla/show_bug.cgi?id=906) &#8211; NSC TCP socket fork did not copy txbuffersize over
  * bug [908](/bugzilla/show_bug.cgi?id=908) &#8211; test.py should exit if waf dies
  * bug [910](/bugzilla/show_bug.cgi?id=910) &#8211; Change Wi-Fi "AccessClass" to something closer to the standard
  * bug [911](/bugzilla/show_bug.cgi?id=911) &#8211; IPv4/v6 multicast forwarding not going to all output interfaces
  * bug [913](/bugzilla/show_bug.cgi?id=913) &#8211; Queue Enqueue/Drop trace sources behavior unintuitive
  * bug [916](/bugzilla/show_bug.cgi?id=916) &#8211; EnableAsciiAll ("prefix") does not work for YansWifiPhyHelper
  * bug [918](/bugzilla/show_bug.cgi?id=918) &#8211; samples/main-packet-header.cc is broken
  * bug [919](/bugzilla/show_bug.cgi?id=919) &#8211; minstrel does not pass valgrind tests
  * bug [921](/bugzilla/show_bug.cgi?id=921) &#8211; Inconsistent declaration of class/struct Object in object.h
  * bug [922](/bugzilla/show_bug.cgi?id=922) &#8211; Inconsistent declaration of class/struct in wifi-remote-station-manager.h
  * bug [923](/bugzilla/show_bug.cgi?id=923) &#8211; Inconsistent declaration of class/struct in mac-low.h
  * bug [924](/bugzilla/show_bug.cgi?id=924) &#8211; Inconsistent declaration of class/struct in <xxx>-wifi-managers </xxx>
  * bug [925](/bugzilla/show_bug.cgi?id=925) &#8211; Various IPv6 cc files trigger deprecated warning
  * bug [926](/bugzilla/show_bug.cgi?id=926) &#8211; olsr handling of multicast packets
  * bug [927](/bugzilla/show_bug.cgi?id=927) &#8211; SimpleOfdmWimaxChannel RxPower computation
  * bug [930](/bugzilla/show_bug.cgi?id=930) &#8211; examples/topology-read/topology-example-sim.cc uses variable length array
  * bug [931](/bugzilla/show_bug.cgi?id=931) &#8211; Abnormal exit reports SIGSEGV on failure
  * bug [932](/bugzilla/show_bug.cgi?id=932) &#8211; Support IP_HDRINCL option for Ipv4RawSocket
  * bug [933](/bugzilla/show_bug.cgi?id=933) &#8211; Flushing ostream and files on abnormal program exit (ASSERT, ABORT and FATAL_ERROR)
  * bug [936](/bugzilla/show_bug.cgi?id=936) &#8211; Waf build error for python bindings on "AccessClass"
  * bug [937](/bugzilla/show_bug.cgi?id=937) &#8211; bugs in ns-3 wimax
  * bug [939](/bugzilla/show_bug.cgi?id=939) &#8211; EmuNetDevice uses too much memory when reading packet bursts
  * bug [940](/bugzilla/show_bug.cgi?id=940) &#8211; AODV fails to set up a correct path
  * bug [941](/bugzilla/show_bug.cgi?id=941) &#8211; Wifi Dcf attributes not reachable through configuration namespace
  * bug [943](/bugzilla/show_bug.cgi?id=943) &#8211; Add a SO_BROADCAST socket option
  * bug [944](/bugzilla/show_bug.cgi?id=944) &#8211; change default ErrorRateModel from YansErrorRateModel to NistErrorRateModel
  * bug [946](/bugzilla/show_bug.cgi?id=946) &#8211; Rocketfuel topology dataset support for topology reader
  * bug [949](/bugzilla/show_bug.cgi?id=949) &#8211; Node::NonPromiscReceiveFromDevice reports a meaningless destination address to user callbacks
  * bug [950](/bugzilla/show_bug.cgi?id=950) &#8211; PointToPointNetDevice says promisc support is not implemented but it is
  * bug [955](/bugzilla/show_bug.cgi?id=955) &#8211; Install NSC for NS3 in a Itanium Architecture (ia64) with Linux 2.6.16.6
  * bug [956](/bugzilla/show_bug.cgi?id=956) &#8211; Bindings failure in core (traced-value) with older gcc
  * bug [958](/bugzilla/show_bug.cgi?id=958) &#8211; WiFi uses wrong default values for MTU, FragmentationThreshold and RtsCtsThreshold
  * bug [959](/bugzilla/show_bug.cgi?id=959) &#8211; Simulation never finishes when using RealtimeSimulatorImpl
  * bug [960](/bugzilla/show_bug.cgi?id=960) &#8211; V4Ping does not generate checksum
  * bug [961](/bugzilla/show_bug.cgi?id=961) &#8211; Problem with MPI activation
  * bug [964](/bugzilla/show_bug.cgi?id=964) &#8211; AODV does not work with host addressed interfaces
  * bug [965](/bugzilla/show_bug.cgi?id=965) &#8211; Problem building /src/core/callback.cc
  * bug [966](/bugzilla/show_bug.cgi?id=966) &#8211; AODV originates new RREQ on each packet, when doesn't have valid route
  * bug [969](/bugzilla/show_bug.cgi?id=969) &#8211; No SocketList is present in UdpL4Protocol class
  * bug [971](/bugzilla/show_bug.cgi?id=971) &#8211; fix AODV header Print functions
  * bug [972](/bugzilla/show_bug.cgi?id=972) &#8211; [flow monitor] assertion fails in pv4-flow-probe.cc

## Known issues

There are no known issues.
