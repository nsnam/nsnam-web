---
title: ns-3 GSoC selections announced
date: 2024-05-01T11:00:00+00:00
author: tomh
layout: post
permalink: /news/ns-3-gsoc-contributors-announced-2024/
categories:
  - News
---
Three students will work with the ns-3 project for the [2024 Google Summer of Code](https://www.nsnam.org/wiki/Summer_Projects#Google_Summer_of_Code_2024)!

The selected students, projects, and project mentors are:

* **Joao Albuquerque**, 5G NR Module Benchmark and Analysis for Distinct Channel Models, mentored by Biljana Bojovic, Amir Ashtari, and Gabriel Ferreira
* **Kavya Bhat**, DHCPv6, mentored by Tommaso Pecorella, Alberto Gallegos Ramonet, and Manoj Kumar Rana
* **Hyerin Kim**, Enhancement of RL Approach Accessibility in NR, mentored by Amir Ashtari, Katerina Koutlia, Biljana Bojovic, and Gabriel Ferreira
