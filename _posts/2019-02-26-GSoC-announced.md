---
layout: post
title:  "ns-3 selected for Google Summer of Code"
date:   2019-02-26 12:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---
We are pleased to again be selected for Google Summer of Code, our tenth year in the program.  Student applications are due on April 9.  More information about ns-3 and GSoC can be found [on our wiki page](https://www.nsnam.org/wiki/GSOC2019StudentGuide).
