---
id: 2180
title: WNS3 2013 preliminary program posted
date: 2013-02-15T17:47:45+00:00
author: WNS3 Chairs
layout: post
guid: http://www.nsnam.org/?p=2180
permalink: /events/wns3-2013-preliminary-program-posted/
categories:
  - Events
tags:
  - simutools
  - wns3
---
[Preliminary program](http://www.nsnam.org/wns3/wns3-2013/program/ "Accepted Papers") for [WNS3 2013](http://www.nsnam.org/wns3/wns3-2013/ "WNS3 2013") has been posted on the website.
