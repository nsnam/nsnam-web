---
id: 3429
title: ns-3 and nanoscale simulations
date: 2016-03-04T17:45:51+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3429
permalink: /news/ns-3-and-nanoscale-simulations/
categories:
  - News
---
The IEEE has recently published the <a href="http://standards.ieee.org/news/swire/#std15" target="_blank">IEEE 1906.1™-2015, Recommended Practice for Nanoscale and Molecular Communication Framework</a> to establish a common conceptual model and framework for nanoscale communications with biomedical applications. The group has established an <a href="http://standards.ieee.org/downloads/1906/1906.1/P1906.1/" target="_blank">ns-3-based simulation framework</a> (also <a href="https://github.com/ieee-p1906-1-reference-code" target="_blank">GitHub link</a>) for this field. For more information, please read also <a href="http://www.academia.edu/20079084/Defining_Communication_at_the_Bottom" target="_blank">this paper</a>.
