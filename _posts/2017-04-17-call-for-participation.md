---
id: 3718
title: Call for participation
date: 2017-04-17T14:02:39+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3718
permalink: /events/call-for-participation/
categories:
  - Events
---
The <a href="https://www.nsnam.org/overview/wns3/wns3-2017/" target="_blank">ns-3 annual meeting</a> will be held from June 12-14, 2017, at the Faculty of Engineering, University of Porto (FEUP), Portugal. The first day will feature <a href="https://www.nsnam.org/docs/consortium/training/ns-3-training-2017.pdf" target="_blank">ns-3 training</a>, and the annual Workshop on ns-3, held on Tuesday and Wednesday, will include <a href="https://www.nsnam.org/overview/wns3/wns3-2017/program/" target="_blank">eighteen paper presentations and a poster/demo session</a>. The annual meeting of the <a href="https://www.nsnam.org/overview/consortium" target="_blank">ns-3 Consortium</a> will wrap up the meeting on the afternoon of Wednesday June 14.
