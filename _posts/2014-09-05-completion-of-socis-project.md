---
id: 2915
title: Completion of SOCIS project
date: 2014-09-05T18:12:01+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2915
permalink: /news/completion-of-socis-project/
categories:
  - News
---
In addition to Google Summer of Code, the ns-3 project was able to mentor a summer student project in the [ESA Summer of Code in Space (SOCIS)](http://sophia.estec.esa.int/socis/) program. Natale Patriciello, mentored by Tommaso Pecorella, added new Satellite-friendly TCP flavours to ns-3. During SOCIS &#8217;14, he provided code for TCP CUBIC, Hybla, HighSpeed, and Noordwijk (TCP BIC is being developed following the project). Moreover, he reviewed and expanded the code proposed for TCP Options support, with further work on the Timestamp and Window Scaling options, and test suites for TCP options code. The new TCP features will be extremely beneficial to simulate TCP in high bandwidth-delay product networks, such as satellite links and high-speed optical cables.
