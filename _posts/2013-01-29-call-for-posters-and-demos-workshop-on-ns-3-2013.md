---
id: 2075
title: 'Call for posters and demos: Workshop on ns-3 2013'
date: 2013-01-29T16:10:52+00:00
author: WNS3 Chairs
layout: post
guid: http://www.nsnam.org/?p=2075
permalink: /events/call-for-posters-and-demos-workshop-on-ns-3-2013/
categories:
  - Events
tags:
  - wns3
---
Dear ns-3 user, contributors and developers,

you are hereby invited to submit proposals for posters and demos for WNS3 2013.

You should propose either a poster or demo. In case you want to propose a poster, you should describe it in your proposal; you are not expected to send the poster right away. In case you want to propose a demo, your description should also include details about the physical resources needed (if any).

There are no strict requirements for using a specific template for submission. Also, please note that demos and posters are submitted directly to \_both\_ TPC chairs, on mail addresses listed at [WNS3 TPC site](https://www.nsnam.org/wns3/wns3-2013/program-comittee/).

Deadline for submission is **February 13, 2013**.
