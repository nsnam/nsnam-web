---
id: 1271
title: Ubiquisys and CTTC are working together on an LTE simulation framework based on ns-3.
date: 2011-02-02T10:00:30+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1271
permalink: /news/ubiquisys-and-cttc-are-working-together-on-an-lte-simulation-framework-based-on-ns-3/
categories:
  - News
---
Ubiquisys and CTTC announced a joint effort in the development of a product-oriented LTE simulation framework based on ns-3. [Read the official press release here.](http://www.ubiquisys.com/femtocell-media-press-releases-id-203.htm)
