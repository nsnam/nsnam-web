---
title: Annual meeting wrapup
date: 2022-06-25T14:00:00+00:00
author: tomh
layout: post
permalink: /news/annual-meeting-wrapup-2022/
excerpt_separator: <!--more-->
categories:
  - Events
  - News
---
Over two hundred registered attendees attended the <a href="https://www.nsnam.org/research/wns3/wns3-2022/" target="_blank">14th annual Workshop on ns-3</a>, ns-3 tutorial sessions, and a small hackathon.  The meeting was organized by the <a href="https://www.nsnam.org/consortium/" target="_blank">ns-3 Consortium</a>, which held its annual plenary meeting on June 24. <!--more--> The Workshop on ns-3 featured seventeen paper presentations and nine lightning talks.  Next year's annual meeting is planned to be an in-person meeting (dates and venue TBD).
