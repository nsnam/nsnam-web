---
layout: post
title:  "ns-3.34 released"
date:   2021-07-14 10:28:00 -0800
categories: news
excerpt_separator: <!--more-->
---

The ns-3.34 release is now available, featuring a significantly refactored and improved Wi-Fi MAC and PHY design, enabling both 802.11ax uplink and downlink OFDMA models and a multi-user OFDMA scheduler.  ns-3.34 also contains a new phased array model, a new TCP congestion control model, and more.  Find out more about ns-3.34 on its [release page](https://www.nsnam.org/releases/ns-3-34).
