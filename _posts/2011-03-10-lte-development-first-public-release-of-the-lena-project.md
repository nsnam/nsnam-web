---
id: 1285
title: 'LTE development: first public release of the LENA project'
date: 2011-03-10T12:32:06+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1285
permalink: /news/lte-development-first-public-release-of-the-lena-project/
categories:
  - News
---
As officially announced in a [press release earlier this month](http://www.ubiquisys.com/femtocell-media-press-releases-id-203.htm), CTTC and Ubiquisys are working together on a LTE simulation model for ns-3. This project is codenamed LENA (LTE/EPC Network simulAtor).

CTTC now [announced the first public release of the LENA project](http://mailman.isi.edu/pipermail/ns-developers/2011-March/008734.html). The LENA code is based on the GSoC 2010 project code by Giuseppe Piro, which was merged into ns-3-dev on December 2010. With respect to the GSoC code, the main modifications that have been performed are:

  * the MAC has been entirely rewritten to support the [MAC Scheduler API defined by the Femto Forum](http://www.femtoforum.org/femto/technical.php). A Round Robin scheduler implementation based on this API is also provided.
  * the RLC API has been rewritten. At this stage only the API definition is in place, together with a simplified RLC implementation.
  * the RRC has been entirely rewritten
  * network layer functionality has been temporarily disabled; it will be re-enabled when either RLC/UM or RLC/AM is available.

The CTTC team clarifies that this is still work-in-progress, and that in the near future they will be working on finalizing the uplink functionality and the inter-cell interference model, which are currently supported only to a limited degree. In the mid term, the plan to introduce the RLC functionality to enable IP networking. In the long term, it is foreseen that some EPC elements will be included as well.
