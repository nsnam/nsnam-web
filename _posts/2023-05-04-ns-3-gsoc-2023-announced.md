---
title: ns-3 GSoC selections announced
date: 2023-05-04T11:00:00+00:00
author: tomh
layout: post
permalink: /news/ns-3-gsoc-contributors-announced-2023/
categories:
  - News
---
Three students will work with the ns-3 project for the [2023 Google Summer of Code](https://www.nsnam.org/wiki/Summer_Projects#Google_Summer_of_Code_2023)!

The selected students, projects, and project mentors are:

* **Giovanni Grieco**, [IUNS-3 5G NR: Improving the Usability of ns-3's 5G NR Module](https://summerofcode.withgoogle.com/programs/2023/projects/oeDFknbT), mentored by Tom Henderson, Katerina Koutlia, and Biljana Bojovic
* **Raghuram Kannan**, [Dynamic device registration for NetAnim simulation animations](https://summerofcode.withgoogle.com/programs/2023/projects/EpacH0ct), mentored by Tommaso Pecorella and Manoj Kumar Rana
* **Muyuan Shen**, [ns3-ai enhancements](https://summerofcode.withgoogle.com/programs/2023/projects/A4KZ7dxo), mentored by Collin Brady and Hao Yin
