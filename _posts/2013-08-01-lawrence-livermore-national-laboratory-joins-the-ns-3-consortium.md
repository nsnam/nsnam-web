---
id: 2521
title: Lawrence Livermore National Laboratory joins the NS-3 Consortium
date: 2013-08-01T17:54:53+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2521
permalink: /news/lawrence-livermore-national-laboratory-joins-the-ns-3-consortium/
categories:
  - News
---
We&#8217;re pleased to announce that <a target="_blank" href="https://www.llnl.gov">Lawrence Livermore National Laboratory</a> has joined the <a target="_blank" href="http://www.nsnam.org/consortium/about/">NS-3 Consortium</a> as its first regular Consortium Member. LLNL is actively working on improving the ns-3 core, particularly for parallel simulations, and has published on this work in recent SIMUTools and WNS3 conferences.
