---
layout: post
title:  "NSF awards ns-3 wireless project"
date:   2020-08-18 21:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---
The U.S. National Science Foundation (NSF) has awarded a community research infrastructure (CRI) program to the University of Washington and the Georgia Institute of Technology (Georgia Tech).<!--more-->  The program, led by Prof. Sumit Roy at UW and Prof. Douglas Blough at Ga. Tech, is a multi-year effort to upgrade the scalability of ns-3 wireless models for next-generation Wi-Fi and 5G mmWave simulations.  Another focus will be to perform additional community outreach and to improve ns-3 usability and educational support.  For more information, please see the <a href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=2016379">award announcement</a>.
