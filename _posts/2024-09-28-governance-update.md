---
title: ns-3 project governance posted
date: 2024-09-28T11:00:00+00:00
author: tomh
layout: post
permalink: /news/ns-3-governance-structure-2024/
categories:
  - News
---
A governance structure for the open source project has been posted under the [About/Governance](/about/governance/) section of the website.  This includes project bylaws, a code of conduct, a newly-organized Steering Council (chaired by Eduardo Almeida), and a list of other project roles and policies.
