---
id: 1667
title: 'Google Summer of Code 2012: List of Accepted Students Announced'
date: 2012-04-23T19:55:31+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=1667
permalink: /news/google-summer-of-code-2012-list-of-accepted-students-announced/
categories:
  - News
---
The [Google Summer of Code 2012](http://www.google-melange.com/gsoc/homepage/google/gsoc2012) results are out, and we&#8217;re happy to announce that we have the following three students who will be working with us over the summer:

  * **Dizhi Zhou**, mentored by Nicola Baldo, working on &#8220;LTE Scheduling with the FemtoForum MAC Scheduler API&#8221;.
  * **Mudit Raj Gupta**, mentored by Tommaso Pecorella, working on &#8220;HLA interfaces for ns-3&#8221;.
  * **V. Sindhuja**, mentored by Tom Henderson, working on &#8220;Network Address and Port Translation (NAT) models&#8221;.

Congratulations to the accepted students! This is ns-3&#8217;s fourth participation in the Google Summer of Code programme, and we&#8217;re really excited to be working with another batch of students who will produce important contributions to the project.

We sincerely thank all students who put in time and effort into applying to us. We hope that all of you will continue to stay in touch with the ns-3 community.

Stay tuned on our [developers list](http://mailman.isi.edu/mailman/listinfo/ns-developers) to keep track of our summer of code projects and other development discussions.
