---
id: 3179
title: ns-3 GSoC 2015 students announced
date: 2015-04-27T19:13:36+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3179
permalink: /news/3179/
categories:
  - Events
  - News
---
Four student projects have been selected for the 2015 [Google Summer of Code program](http://www.google-melange.com/gsoc/homepage/google/gsoc2015):

  * Melchiorre Danilo Abrignani: Carrier Aggregation support for the LTE module
  * Matthieu Coudron: Implementing multipath TCP (MPTCP) in ns3
  * Natale Patriciello: TCP layer refactoring with automated test on RFC compliance and validation
  * Vishwesh Rege: 802.15.4 realistic MAC and Energy Model

Detailed project plans and descriptions will be posted in the near future.
