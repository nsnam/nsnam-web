---
layout: post
title:  "ns-3 joins SPI"
date:   2020-10-12 17:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---

ns-3 has joined [Software in the Public Interest (SPI)](https://www.spi-inc.org/) as an Associated Project.
