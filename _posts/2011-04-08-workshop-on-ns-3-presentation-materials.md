---
id: 1081
title: Workshop on ns-3 presentation materials
date: 2011-04-08T21:21:47+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=1081
permalink: /events/workshop-on-ns-3-presentation-materials/
categories:
  - Events
---
The presentation materials for the <a title="Workshop on ns-3" href="http://www.nsnam.org/wiki/index.php/Wns3-2011" target="_blank">Workshop on ns-3</a>, held in Barcelona Spain on 25 March 2011, have been uploaded to the wiki.  This was the first workshop that included a formal paper track, and the papers will eventually appear in the <a title="ACM Digital Library" href="http://portal.acm.org/" target="_blank">ACM Digital Library</a>.  Special thanks to the technical program chairs Nicola Baldo and Ruben Merz for coordinating a very nice program, and to the technical program committee for reviewing the submissions.
