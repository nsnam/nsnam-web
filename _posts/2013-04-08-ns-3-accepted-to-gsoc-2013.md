---
id: 2251
title: ns-3 accepted to GSoC 2013
date: 2013-04-08T21:03:46+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2251
permalink: /news/ns-3-accepted-to-gsoc-2013/
categories:
  - Events
  - News
---
Our project was pleased to learn today that we will be one of 177 organizations participating in the 2013 [Google Summer of Code](http://www.google-melange.com/gsoc/homepage/google/gsoc2013). This program is a great opportunity for students to learn a bit about software engineering and open source projects, and for our project community to grow. Interested students are encouraged to interact with the project through the main project mailing list, [ns-developers@isi.edu](http://www.nsnam.org/developers/tools/mailing-lists/), and to review our [wiki](http://www.nsnam.org/wiki/GSOC2013StudentGuide). Students will have until May 3 to learn about ns-3, develop a project proposal, and submit it to Google.
