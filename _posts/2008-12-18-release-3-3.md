---
id: 488
title: Release 3.3
date: 2008-12-18T08:15:36+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=488
permalink: /news/release-3-3/
categories:
  - News
  - ns-3 Releases
---
## Availability

This release is immediately available from [here](http://www.nsnam.org/release/ns-3.3.tar.bz2).

## Supported platforms

ns-3.3 has been tested on the following platforms:

  * linux x86 gcc 4.2, 4.1, and, 3.4.6.
  * linux x86_64 gcc 4.3.2, 4.2.3, 4.2.1, 4.1.3, 3.4.6
  * MacOS X ppc and x86
  * cygwin gcc 3.4.4 (debug only)

Not all ns-3 options are available on all platforms; consult the [Installation](/wiki/Installation) page for more information.

## New user-visible features

  * Emulated Net Device: A new net device has been added as enabling technology for ns-3 emulation scenarios. See src/devices/emu and examples/emu-udp-echo.cc for details.
  * ICMP: Support for several ICMP messages has been added to ns-3. See src/internet-stack/icmpv4.h for details.
  * IPv6 Address: new clases to support IPv6 addresses has been added to the system. This is enabling technology for fuller IPv6 support scheduled for ns-3.4.
  * A flow-id tag has been added to the contributed code section
  * Star topologies can be created from the topology helper functions
  * The global routing code has been made dynamic (not just limited to (pre-simulation computation) and supports stub network interfaces and bridge net devices
  * A MatchContainer has been added to the configuration subsystem

## Known issues

ns-3 build is known to fail on the following platforms:

  * gcc 3.3 and earlier
  * optimized builds on gcc 3.4.4 and 3.4.5
  * optimized builds on linux x86 gcc 4.0.x
  * optimized builds on Ubuntu 8.10 alpha 5 x86 gcc4.3.2
  * MinGW
