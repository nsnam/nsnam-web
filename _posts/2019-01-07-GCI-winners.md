---
layout: post
title:  "Google Code-In winners announced"
date:   2019-01-07 12:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---
Google has announced ns-3's winners of the 2018 [Google Code-In 2018 contest](https://codein.withgoogle.com), and they are **Parth Pratim** and **barteche**, with honorable mention to finalists **Asanali**, **Parth Pandya**, **sikfeng**, and **Victor Hu**.  The winners will receive a trip this year to Google HQ.
