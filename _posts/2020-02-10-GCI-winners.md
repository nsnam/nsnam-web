---
layout: post
title:  "Google Code-In awardees announced"
date:   2020-02-10 12:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---
Google has announced ns-3's winners for the [Google Code-In 2019 contest](https://codein.withgoogle.com), and they are **Aditya Vardhan Singh** and **Kartik Agarwala**.  The winners will receive a trip later this year to Google headquarters. We also wish to recognize our runners up **Sai Putravu** and **Stanislaw Howard**, and our finalists **Jackson Lewis** and **Peter Terpstra**.  The students amazed us with their work in the contest.
