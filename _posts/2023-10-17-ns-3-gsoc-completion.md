---
title: ns-3 GSoC program concludes
date: 2023-10-17T11:00:00+00:00
author: tomh
layout: post
permalink: /news/ns-3-gsoc-concludes-2023/
categories:
  - News
excerpt_separator: <!--more-->
---
Yesterday, we formally concluded our 2023 Google Summer of Code program.  We were fortunate to be able to mentor three successful projects and students, whose final reports are posted on our [wiki](https://www.nsnam.org/wiki/Summer_Projects#Google_Summer_of_Code_2023)
<!--more-->
**Giovanni Grieco**, mentored by Katerina, Biljana, and me, wrote an introductory tutorial for the ns-3 nr initial demo program, and in the course of doing so, improved the logging support for the typical packet flow through the RAN part of the scenario.  This tutorial will be included in the forthcoming nr release aligned with ns-3.40.

**Raghuram Kannan**, mentored by Tommaso and Manoj Kumar Rana, worked on the problem of decoupling, at compile time, various ns-3 modules from the netanim network animator.  Raghuram has generated several MRs that have been the subject of a lot of design feedback and corresponding refactoring, and he was able in the end to provide a working version for CSMA and PointToPoint device types.  We would like to keep pushing on this to finish all of our device types and resolve MR comments before the next release.

**Muyuan Shen**, mentored by Hao Yin and Collin Brady, made a number of improvements to the popular ns3-ai module, including support for passing std::vector data types across the shared memory interface, addition of a Gymnasium API, and updating of examples and documentation to correspond to recent ns-3 releases.  He also performed benchmarking of his extensions.  These improvements have already been merged into ns3-ai.

Thanks to all of the students, mentors, and other ns-3 reviewers who helped make this a successful summer.
