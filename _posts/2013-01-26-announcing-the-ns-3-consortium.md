---
id: 2081
title: Announcing the NS-3 Consortium
date: 2013-01-26T15:15:40+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2081
permalink: /news/announcing-the-ns-3-consortium/
categories:
  - News
---
As part of long-term planning for the continued growth and sustainment of ns-3 and related tools, INRIA and the University of Washington have formed a consortium, separate but in support of the open source project. The consortium is mainly intended to raise funding for the project, but also to provide some guidance and organizational support for administrative issues and related matters.

The founding members are INRIA and the University of Washington, but we will be expanding the steering committee to provide a broader representation of organizations and individuals who have been and will be supporting ns-3. We will be actively seeking industrial members or other professional organizations to participate. More details are available at [http://www.nsnam.org/consortium](http://www.nsnam.org/consortium/).
