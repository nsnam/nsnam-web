---
id: 2831
title: ns-3 GSoC 2014 students announced
date: 2014-04-21T19:09:47+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2831
permalink: /news/ns-3-gsoc-2014-students-announced/
categories:
  - Events
  - News
---
Four student projects have been selected for the 2014 [Google Summer of Code program](http://www.nsnam.org/wiki/GSOC2014AcceptedProjects):

  * Piotr Gawlowicz: LTE Fractional Frequency Reuse algorithms.
  * Rubén Martínez: Licklider Transmission Protocol (LTP).
  * Truc Anh N Nguyen: Understanding Bufferbloat Through Simulations in ns-3.
  * Krishna Teja Yadavalli: Multicast IPv6 traffic support.

Detailed project plans and descriptions are linked from the [project wiki page](http://www.nsnam.org/wiki/GSOC2014AcceptedProjects).
