---
layout: post
title: ns-3 in ESA Summer of Code in Space (SOCIS) program
date:   2019-04-04 12:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---
The ns-3 project has been selected to participate in the 2019 edition of the European Space Agency (ESA) [Summer of Code in Space (SOCIS) program](https://socis.esa.int/). This marks the sixth year that ns-3 has been selected for SOCIS.  Eligible students may apply by May 4 to one of three space networking projects; more information is found on the [ns-3 SOCIS page](https://www.nsnam.org/wiki/SOCIS2019).
