---
id: 3318
title: 2016 Workshop on ns-3 announced
date: 2015-10-23T05:02:20+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3318
permalink: /news/2016-workshop-on-ns-3-announced/
categories:
  - Events
  - News
---
The NS-3 Consortium is organizing the 2016 edition of the [Workshop on ns-3 (WNS3)](http://www.nsnam.org/overview/wns3/wns3-2016/), to be held on June 15-16 2016, at the [University of Washington](http://www.ee.washington.edu/) in Seattle, Washington. The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities.

WNS3 invites authors to submit original high quality papers presenting different aspects of developing and using ns-3. The Workshop is making arrangements with the ACM Digital Library and possibly other digital libraries to publish the final papers. Papers and slides from last year&#8217;s presentations can be found [here](https://www.nsnam.org/overview/wns3/wns3-2015/program/).

WNS3 will be part of a week-long series of events in Seattle including ns-3 training, the consortium annual meeting, and developer meetings. More information is found in the <a href="https://www.nsnam.org/wp-content/uploads/2016/01/ns-3-2016-save-the-date.pdf" target="_blank">meeting flyer</a>.
