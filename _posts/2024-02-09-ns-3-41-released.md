---
layout: post
title:  "ns-3.41 released"
date:   2024-02-09 09:00:00 -0800
categories: news
excerpt_separator: <!--more-->
---

The ns-3.41 release [has been published](https://www.nsnam.org/releases/ns-3-41/). This release provides a new cellular MIMO antenna model, additional Wi-Fi model upgrades for the Wi-Fi 7 standard, and LR-WPAN improvements, among many other small feature improvements and bug fixes.
