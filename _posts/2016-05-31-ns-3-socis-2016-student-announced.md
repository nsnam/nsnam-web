---
id: 3528
title: ns-3 SOCIS 2016 student announced
date: 2016-05-31T17:22:00+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3528
permalink: /news/ns-3-socis-2016-student-announced/
categories:
  - News
---
Michael Di Perna has been selected to participate in the 2016 [European Space Agency Summer of Code in Space (SOCIS)](http://sophia.estec.esa.int/socis2016/). Michael&#8217;s project will focus on optical satellite systems such as planned for the [European Data Relay System (EDRS)](http://icsos2014.nict.go.jp/contents/pdf/S1-3.pdf). This is the ns-3 project&#8217;s third year of participation in SOCIS.
