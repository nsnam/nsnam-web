---
id: 2525
title: Google Summer of Code wrapup
date: 2013-10-11T18:46:56+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2525
permalink: /news/google-summer-of-code-wrapup/
categories:
  - News
---
On behalf of the ns-3 project and mentoring team, we&#8217;d like to congratulate our two students, Junling Bu and Budiarto Herman, for a job well done this year with <a target="_blank" href="https://developers.google.com/open-source/soc/?csw=1">Google Summer of Code (GSoC)</a>.

Junling Bu&#8217;s <a target="_blank" href="https://www.nsnam.org/wiki/index.php/GSOC2013WAVE_MAC">project</a> focused on MAC enhancements to WiFi to support vehicular networking scenarios. Among the several standards for Vehicular Ad-Hoc Networks (VANETs), Junling chose to implement the American standard, which is known as &#8220;WAVE&#8221;, and he started a new &#8216;src/wave&#8217; module for the WiFi modifications (most of which reside at the MAC layer or higher). He produced over 9000 new lines of code and documentation. Presently, he has two outstanding patches. The first patch, basic 802.11p support, allows users to create a basic IEEE 802.11p NetDevice that behaves similarly to ns-3&#8217;s adhoc WiFi mode. This patch seems to be nearly ready to merge, after further review of how it is designed to fit with the existing WiFi module. The second patch, alternating channel access according to how WAVE works, requires more testing and validation before merging, but we are planning to work with Junling this fall to complete this merge as well. This code can be used as a building block for vehicular network simulations; further work is needed to integrate vehicular mobility, propagation, and application models.

Budiarto Herman&#8217;s <a target="_blank" href="https://www.nsnam.org/wiki/index.php/GSOC2013UeMeasurementActiveIdle">project</a> focused on developing improvements to three mobility management features of the ns-3 LTE module, namely UEMeasurements, Initial Cell Selection, and Handover. In particular, he designed and implemented additional reporting configurations to the UEmeasurement model, a UE mechanism to automatically determine the best eNodeB to attach to based on RSRP, and a new API for handover triggering at the eNB https://www.nsnam.org/wiki/index.php/GSOC2013UeMeasurementActiveIdlewith two handover algorithm implementations. The new code comes with an additional set of tests that verify the correct functionality of the new features, as well as with extensive documentation. Budiarto&#8217;s work was reviewed several times during the GSoC, and is currently undergoing a final review for merge in the ns-3 development tree.

Thanks are due also to our primary mentors (Nicola, Marco, and Jaime for Budi&#8217;s project, and Guillaume for Junling&#8217;s project), and to others on the ns-3 project for helping with the student selection process and for reviewing code.
