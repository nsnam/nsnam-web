---
layout: post
title:  "Google Code-In Wrap-up"
date:   2020-01-27 15:00:00 -0700
categories: news
excerpt_separator: <!--more-->
---
The seven-week [Google Code-In 2019 contest](https://codein.withgoogle.com) wrapped up today, with 292 tasks successfully completed by 42 students.  Thanks are due to **Mohit Tahiliani** for leading ns-3's effort, for the many mentors who guided the work, and to the many student contributors whose efforts will be folded into ns-3.
