---
id: 3164
title: National Workshop on ns-3 announced
date: 2015-04-05T17:49:06+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3164
permalink: /events/national-workshop-on-ns-3-announced/
categories:
  - Events
---
The Department of Electronics and Communication of Vimal Jyothi Engineering College, Chemperi is organizing a Two Days National Workshop on “Network Simulator-3“ during April 11-12, 2015. This workshop, open to students, research scholars, and faculty members, is is limited to the first 60 registered participants on a first come, first served basis. Registration fees are posted on the <a href="http://www.vjec.ac.in/public_downloads/news-events/uploads_original/2015-03-30/NS3-workshop-April-2015.pdf" target="_blank">workshop announcement</a>, and boarding and lodging are available.
