---
id: 200
title: ns-3 developers meeting
date: 2011-01-13T16:00:46+00:00
author: tomh
layout: post
guid: http://www2.nsnam.org/?p=200
permalink: /news/ns-3-developers-meeting/
categories:
  - News
---
A face-to-face developers meeting is planned for end of March, collocated with Simutools 2011 and WNS3 2011, in Barcelona, Spain. The list of topics and attendees is maintained in the [wiki](/wiki/index.php/DevelMeetingMarch2011).
