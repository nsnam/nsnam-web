---
title: ns-3 GSoC program concludes
date: 2024-10-15T11:00:00+00:00
author: tomh
layout: post
permalink: /news/ns-3-gsoc-concludes-2024/
categories:
  - News
excerpt_separator: <!--more-->
---
Our three 2024 Google Summer of Code students have successfully completed their programs and posted final reports, linked from our [wiki](https://www.nsnam.org/wiki/Summer_Projects#Google_Summer_of_Code_2024)
<!--more-->
**Kavya Bhat**, mentored by Tommaso Pecorella, Alberto Gallegos Ramonet, and Manoj Kumar Rana, implemented a model for DHCP for IPv6 networks (DHCPv6).  The addition of this code is helpful for ns-3 because many modern networks use DHCPv6 SLAAC both at enterprises and IoT home networks (e.g., Thread stack). This model
allows ns-3 users to test practical scenarios, particularly where DHCPv6 might fail to either acquire an address or detect an address collision.

**Hyerin Kim**, mentored by mentored by Katerina Koutlia, Amir Ashtari, Biljana Bojovic, and Gabriel Ferreira, completed a project on extending the 5G NR module (5G-LENA) with AI/ML capabilities, through the integration with the ns3-gym and the implementation of RL-based MAC schedulers.  This project was motivated by the optimization possibilities for the NR QoS schedulers to increase the total user throughput and to improve user fairness.

**Joao Albuquerque**, mentored by Biljana Bojovic, Amir Ashtari, and Gabriel Ferreira, focused on extending the NR module (5G-LENA) to support legacy non-spatial channel models, adding support for the NYUSIM channel model, and creating a new channel helper to facilitate channel setups. The channel helper also checks for valid calibrated configurations, preventing users from using unsupported configurations unless explicitly configured.

Thanks to all of the students, mentors, and other ns-3 reviewers who helped make this a successful summer.
