---
id: 2829
title: ns-3 accepted to SOCIS 2014
date: 2014-04-30T13:03:39+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=2829
permalink: /news/ns-3-accepted-to-socis-2014/
categories:
  - Events
  - News
---
ns-3 has been selected to participate in the 2014 [European Space Agency Summer of Code](http://sophia.estec.esa.int/socis2014/) program. This program is another opportunity for students to participate in open source for the summer, and with this program, all projects should have a connection to space application. The application deadline is May 15. Please note that participation is restricted to students enrolled at universites in selected countries; please see [the eligibility criteria](http://sophia.estec.esa.int/socis2014/faq#socis_elig_student_who). Please subscribe to the ns-developers mailing list and monitor  [this wiki page](http://www.nsnam.org/wiki/SOCIS2014Projects) for more information.
