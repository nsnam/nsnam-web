---
id: 3258
title: Two more summer students announced
date: 2015-06-23T20:11:45+00:00
author: tomh
layout: post
guid: http://www.nsnam.org/?p=3258
permalink: /news/two-summer-students-announced/
categories:
  - News
---
In addition to the four students participating in ns-3&#8217;s Google Summer of Code program, the project is mentoring two additional summer students. Nikita Akazin is our  [ESA Summer of Code in Space](http://sophia.estec.esa.int/socis/) student and will be working with a mentoring team of Tommaso Pecorella, Jani Puttonen, and Budiato Herman on the topic of [satellite channel models](https://www.nsnam.org/wiki/SOCIS2015). Saswat Mishra is working on a  [mentored summer project](https://www.nsnam.org/wiki/Summer_Projects#Mentored_summer_projects) on IP neighbor discovery enhancements.
