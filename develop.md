---
layout: page
title: Develop
permalink: /develop/
---

Many users of ns-3 focus on using existing models to conduct simulations. However, when the existing models lack a specific feature or when a new un-implemented model is needed, some ns-3 users turn to modify more or less heavily the existing models and, sometimes, even implement brand new models from scratch.

The considerable time investment of these heavy users in ns-3 typically leads them to interact closely with the community of those who contribute to the development and maintenance of the ns-3 releases. Some of them eventually contribute back their own modifications or new models to the ns-3 community because they see value in being integrated within the ns-3 development process:

* More testing and validation performed by other users
* Contributions from other users to further improve their new models or modifications
* Less work to track API changes in ns-3 because the ns-3 developers who change APIs are responsible for updating all models integrated within the ns-3 releases
* More visibility and exposure for their research results

The following sections attempt to answer common questions to help these new developers understand how ns-3 releases are managed:

*   [Release Process](/develop/release-process): The process and the timeline we follow to put together every new release.
*   [Roadmap](/develop/roadmap): Long term development goals and directions.
*   [Maintainers](/develop/maintainers): Who is responsible for the maintenance, evolution, and development of which models.
*   [Contributing Code](/develop/contributing-code): How to contribute modifications or new models to an ns-3 release.
*   [Tools](/develop/tools): What tools are available to develop ns-3, and to communicate with other maintainers and developers.
