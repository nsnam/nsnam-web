---
title: Download
layout: page
permalink: /releases/ns-3-41/download/
---
Please click the following link to download ns-3.41, released February 9, 2024:

  * [ns-allinone-3.41](/releases/ns-allinone-3.41.tar.bz2) (compressed source code archive)

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.41.tar.bz2 release archive is 12265002b4902b75d52075baecdbd95b5910a53f
