---
title: Download
layout: page
permalink: /releases/ns-3-32/download/
---
Please click the following link to download ns-3.32, released October 7, 2020:

  * [ns-allinone-3.32](/releases/ns-allinone-3.32.tar.bz2) (compressed source code archive)

For users of the ns-3.31 release, a patch is available to update code to ns-3.32, at [this link](/release/patches/ns-3.31-to-ns-3.32.patch).  Other patches to migrate older versions of ns-3 (back to ns-3.17) to subsequent release versions can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.32.tar.bz2 release archive is ce9c09e2b7b93cf12fa61b7fdbb12c49d2302e8c.
