---
title: Authors
layout: page
permalink: /releases/ns-3-40/authors/
---
The following people made source code contributions (commits) during the ns-3.40 development period:

* Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
* André Apitzsch <andre.apitzsch@etit.tu-chemnitz.de>
* Ben Lee <limjcst@163.com>
* Eduardo Almeida <enmsa@outlook.pt>
* Gabriel Ferreira <gabrielcarvfer@gmail.com>
* Lauri Sormunen <lauri.sormunen@magister.fi>
* Pierre Wendling <pierre.wendling.4@gmail.com>
* Sébastien Deronne <sebastien.deronne@gmail.com>
* Sharan Naribole <sharan.naribole@gmail.com>
* Stefano Avallone <stavallo@unina.it>
* Tom Henderson <tomh@tomh.org>
* Tommaso Pecorella <tommaso.pecorella@unifi.it>
