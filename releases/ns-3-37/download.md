---
title: Download
layout: page
permalink: /releases/ns-3-37/download/
---
Please click the following link to download ns-3.37, released November 1, 2022:

  * [ns-allinone-3.37](/releases/ns-allinone-3.37.tar.bz2) (compressed source code archive)

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.37.tar.bz2 release archive is 2242b5a90ba9d85914c4b305d1853bfab9592160
