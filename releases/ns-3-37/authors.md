---
title: Authors
layout: page
permalink: /releases/ns-3-37/authors/
---
The following people made source code contributions (commits) during the ns-3.37 development period:

* Alberto Gallegos Ramonet
* Alexander Krotov
* André Apitzsch
* Biljana Bojovic
* Eduardo Almeida
* Gabriel Ferreira
* Gard Spreemann
* Heun
* Katerina Koutlia
* Lars Moons
* Lars Toenning
* Nils Kattenbeck
* Pavinberg
* Peter Barnes
* Sachin Nayak
* Sebastien Deronne
* Sharan Naribole
* Stefano Avallone
* Tolik Zinovyev
* Tom Henderson
* Tommaso Pecorella
* Zhiheng Dong
* Zoraze Ali
