---
title: Authors
layout: page
permalink: /releases/ns-3-28/authors/
---
The following people made code contributions (commits) during the ns-3.28 development period:

  * Alexander Afanasyev <alexander.afanasyev@ucla.edu>
  * Alexander Krotov <krotov@iitp.ru>
  * Ankit Deepak <adadeepak8@gmail.com>
  * Biljana Bojovic <biljana.bojovic@gmail.com>
  * Charitha Sangaraju <charitha29193@gmail.com>
  * Getachew Redieteab <getachew.redieteab@orange.com>
  * Hossam Khader <hossamkhader@gmail.com>
  * Jakub Rewienski <jrewienski@gmail.com>
  * Manoj Kumar Rana <manoj24.rana@gmail.com>
  * Manuel Requena <manuel.requena@cttc.es>
  * Mathias Ettinger <mettinger@toulouse.viveris.com>
  * Matias Richart <mrichart@fing.edu.uy>
  * Mohit P. Tahiliani <tahiliani@nitk.edu.in>
  * Natale Patriciello <natale.patriciello@gmail.com>
  * Peter D. Barnes, Jr. <pdbarnes@llnl.gov>
  * Robert Ammon <ammo6818@vandals.uidaho.edu>
  * Ryan Mast <mast9@llnl.gov>
  * Sébastien Deronne <sebastien.deronne@gmail.com>
  * Stefano Avallone <stavallo@unina.it>
  * Surya Seetharaman <suryaseetharaman.9@gmail.com>
  * Tom Henderson <tomh@tomh.org>
  * Tommaso Pecorella <tommaso.pecorella@unifi.it>
  * Vivek Jain <jain.vivek.anand@gmail.com>
  * Viyom <viyommittal@gmail.com>
  * Zoraze Ali <zoraze.ali@cttc.es>
