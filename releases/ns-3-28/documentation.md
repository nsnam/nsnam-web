---
title: Documentation
layout: page
permalink: /releases/ns-3-28/documentation/
---
  * **Tutorial:** an introduction into downloading, setting up, and using builtin models: [pdf](/docs/release/3.28/tutorial/ns-3-tutorial.pdf), [html (single page)](/docs/release/3.28/tutorial/singlehtml/index.html), [html (split page)](/docs/release/3.28/tutorial/html/index.html)
  * **Manual:** an in-depth coverage of the architecture and core of ns-3: [pdf](/docs/release/3.28/manual/ns-3-manual.pdf), [html (single page)](/docs/release/3.28/manual/singlehtml/index.html), [html (split page)](/docs/release/3.28/manual/html/index.html)
  * **Model Library:** documentation on individual protocol and device models that build on the ns-3 core: [pdf](/docs/release/3.28/models/ns-3-model-library.pdf), [html (single page)](/docs/release/3.28/models/singlehtml/index.html), [html (split page)](/docs/release/3.28/models/html/index.html)
  * [Release Errata](/wiki/Errata)
  * [API Documentation](/docs/release/3.28/doxygen/index.html):&nbsp; Coverage of the C++ APIs using Doxygen.
  * Documentation of the [Bake](/docs/bake/tutorial/html/index.html) integration tool
  * Documentation of the [Direct Code Execution](/about/projects/direct-code-execution/) 1.9 release
    * [API Documentation](/docs/dce/release/1.9/doxygen/index.html):&nbsp; Coverage of the APIs using Doxygen.
    * Manual: [html (single page)](/docs/dce/release/1.9/manual/singlehtml/index.html), [html (split page)](/docs/dce/release/1.9/manual/html/index.html)
