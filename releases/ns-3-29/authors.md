---
title: Authors
layout: page
permalink: /releases/ns-3-29/authors/
---
The following people made code contributions (commits) during the ns-3.29 development period:

  * Akin Soysal <akinsoysal@gmail.com>
  * Alexander Krotov <krotov@iitp.ru>
  * Ankit Deepak <adadeepak8@gmail.com>
  * Budiarto Herman <budiarto.herman@magister.fi>
  * Federico Guerra <federico@guerra-tlc.com>
  * Fernando J. Cintrón <fernando.cintron@nist.gov>
  * Getachew Redieteab <redieteab.orange@gmail.com>
  * Gustavo Carneiro <gjcarneiro@gmail.com>
  * Jörg Christian Kirchhof <christian.kirchhof@rwth-aachen.de>
  * Manish Kumar B. <bmanish15597@gmail.com>
  * Manuel Requena <manuel.requena@cttc.es>
  * Matias Richart <mrichart@fing.edu.uy>
  * Matthew Bradbury <matt-bradbury@live.co.uk>
  * Michele Polese <michele.polese@gmail.com>
  * Mohit P. Tahiliani <tahiliani@nitk.edu.in>
  * Muhammad Iqbal CR <muh.iqbal.cr@gmail.com>
  * Natale Patriciello <natale.patriciello@gmail.com>
  * Nicola Baldo <nbaldo@cttc.es>
  * Pasquale Imputato <p.imputato@gmail.com>
  * Peter D. Barnes, Jr. <barnes26@llnl.gov>
  * Robert Ammon <ammo6818@vandals.uidaho.edu>
  * Roman Naumann <roman.naumann@hu-berlin.de>
  * Sébastien Deronne <sebastien.deronne@gmail.com>
  * Shefali Gupta <shefaligups11@gmail.com>
  * Shravya Ks <shravya.ks0@gmail.com>
  * Stefano Avallone <stavallo@unina.it>
  * Steve Smith <smith84@llnl.gov>
  * Tom Henderson <tomh@tomh.org>
  * Tommaso Pecorella <tommaso.pecorella@unifi.it>
  * Vivek Jain <jain.vivek.anand@gmail.com>
  * Viyom Mittal <viyommittal@gmail.com>
  * Zoraze Ali <zoraze.ali@cttc.es>
