---
title: Older Releases
layout: page
permalink: /releases/older/
---
All ns-3 releases are archived: the short links below provide quick access to the source code, releases notes, and documentation of every past release:

| Release | Date |
| ------- | ---- |
| [ns-3.43](/releases/ns-3-43) | October 2024 |
| [ns-3.42](/releases/ns-3-42) | May 2024 |
| [ns-3.41](/releases/ns-3-41) | February 2024 |
| [ns-3.40](/releases/ns-3-40) | September 2023 |
| [ns-3.39](/releases/ns-3-39) | July 2023 |
| [ns-3.38](/releases/ns-3-38) | March 2023 |
| [ns-3.37](/releases/ns-3-37) | November 2022 |
| [ns-3.36](/releases/ns-3-36) | April 2022 |
| [ns-3.35](/releases/ns-3-35) | October 2021 |
| [ns-3.34](/releases/ns-3-34) | July 2021 |
| [ns-3.33](/releases/ns-3-33) | January 2021 |
| [ns-3.32](/releases/ns-3-32) | October 2020 |
| [ns-3.31](/releases/ns-3-31) | June 2020 |
| [ns-3.30](/releases/ns-3-30) | August 2019 |
| [ns-3.29](/releases/ns-3-29) | September 2018 |
| [ns-3.28](/releases/ns-3-28) | March 2018 |
| [ns-3.27](/releases/ns-3-27) | October 2017 |
| [ns-3.26](/releases/ns-3-26) | October 2016 |
| [ns-3.25](/releases/ns-3-25) | March 2016 |
| [ns-3.24](/releases/ns-3-24) | September 2015 |
| [ns-3.23](/releases/ns-3-23) | May 2015 |
| [ns-3.22](/releases/ns-3-22) | February 2015 |
| [ns-3.21](/releases/ns-3-21) | September 2014 |
| [ns-3.20](/releases/ns-3-20) | June 2014 |
| [ns-3.19](/releases/ns-allinone-3.19.tar.bz2) | December 2013 |
| [ns-3.18](/releases/ns-allinone-3.18.2.tar.bz2) | August 2013 |
| [ns-3.17](/releases/ns-allinone-3.17.tar.bz2) | May 2013 |
| [ns-3.16](/releases/ns-allinone-3.16.tar.bz2) | December 2012 |
| [ns-3.15](/releases/ns-allinone-3.15.tar.bz2) | August 2012 |
| [ns-3.14](/releases/ns-allinone-3.14.1.tar.bz2) | June 2012 |
| [ns-3.13](/releases/ns-allinone-3.13.tar.bz2) | December 2011 |
| [ns-3.12](/releases/ns-allinone-3.12.1.tar.bz2) | August 2011 |
| [ns-3.11](/releases/ns-allinone-3.11.tar.bz2) | May 2011 |
| [ns-3.10](/releases/ns-allinone-3.10.tar.bz2) | January 2011 |
| [ns-3.9](/releases/ns-allinone-3.9.tar.bz2) | August 2010 |
| [ns-3.8](/releases/ns-allinone-3.8.tar.bz2) | May 2010 |
| [ns-3.7](/releases/ns-allinone-3.7.1.tar.bz2) | January 2010 |
| [ns-3.6](/releases/ns-allinone-3.6.tar.bz2) | October 2009 |
| [ns-3.5](/releases/ns-allinone-3.5.1.tar.bz2) | July 2009 |
| [ns-3.4](/releases/ns-allinone-3.4.tar.bz2) | April 2009 |
| [ns-3.3](https://code.nsnam.org/ns-3-dev/archive/2efae18e7379.tar.bz2) | December 2008 |
| [ns-3.2](http://code.nsnam.org/ns-3-dev/archive/2ecac911b3ec.tar.bz2) | September 2008 |
| [ns-3.1](http://code.nsnam.org/ns-3-dev/archive/5768685f9fdb.tar.bz2) | July 2008 |

The full directory of releases is available [here](/release).
