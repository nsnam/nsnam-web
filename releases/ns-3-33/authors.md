---
title: Authors
layout: page
permalink: /releases/ns-3-33/authors/
---
The following people made source code contributions (commits) during the ns-3.33 development period:

  * Aditya Chaudhary <adityach2002@gmail.com>
  * Alexander Krotov <krotov@iitp.ru>
  * Anna Poon <poon2@llnl.gov>
  * Biljana Bojovic <biljana.bojovic@cttc.es>
  * Christophe Delahaye <christophe.delahaye@orange.com>
  * Jordan Dorham <dorham1@llnl.gov>
  * Katerina Koutlia <katerina.koutlia@cttc.es>
  * Mathew Bielejeski <bielejeski1@llnl.gov>
  * Michele Abruzzese <oniricha04@gmail.com>
  * Natale Patriciello <natale.patriciello@gmail.com>
  * Parikshit Gune <parikshitgune@gmail.com>
  * Peter D. Barnes, Jr <barnes26@llnl.gov>
  * Rediet <getachew.redieteab@orange.com>
  * Rémy Grünblatt <remy@grunblatt.org>
  * Robert Hartung <hartung@ibr.cs.tu-bs.de>
  * Sébastien Deronne <sebastien.deronne@gmail.com>
  * Stefano Avallone <stavallo@unina.it>
  * Steven Smith <smith84@llnl.gov>
  * Theodore Zhang <harryzwh@gmail.com>
  * Tom Henderson <tomh@tomh.org>
  * Tommaso Pecorella <tommaso.pecorella@unifi.it>
  * Tommaso Zugno <tommasozugno@gmail.com>
  * Vivek Jain <jain.vivek.anand@gmail.com>
