---
title: Download
layout: page
permalink: /releases/ns-3-31/download/
---
Please click the following link to download ns-3.31, released June 27, 2020:

  * [ns-allinone-3.31](/releases/ns-allinone-3.31.tar.bz2) (compressed source code archive)

For users of the ns-3.30.1 release, a patch is available to update code to ns-3.31, at [this link](/release/patches/ns-3.30.1-to-ns-3.31.patch).  Other patches to migrate older versions of ns-3 (back to ns-3.17) to subsequent release versions can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-3.31 release file is e7cce38ce4f81b62cbabce4990c4e1ede5daa552.
