---
title: Download
layout: page
permalink: /releases/ns-3-34/download/
---
Please click the following link to download ns-3.34, released July 14, 2021:

  * [ns-allinone-3.34](/releases/ns-allinone-3.34.tar.bz2) (compressed source code archive)

For users of the ns-3.33 release, a patch is available to update code to ns-3.34, at [this link](/release/patches/ns-3.33-to-ns-3.34.patch).  Other patches to migrate older versions of ns-3 (back to ns-3.17) to subsequent release versions can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.34.tar.bz2 release archive is 405b4301c3f7d9078196ee98473cb3dafdb21e3d.
