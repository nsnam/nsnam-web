---
title: Documentation
layout: page
permalink: /releases/ns-3-42/documentation/
---
  * **Tutorial:** an introduction to downloading and building ns-3 and running example programs: **[HTML](/docs/release/3.42/tutorial/html/index.html)** format, **[PDF](/docs/release/3.42/tutorial/ns-3-tutorial.pdf)** format,  **[HTML single-page](/docs/release/3.42/tutorial/singlehtml/index.html)** format
  * **Installation Guide:** more complete documentation on how to install ns-3 including optional components: **[HTML](/docs/release/3.42/installation/html/index.html)** format, **[PDF](/docs/release/3.42/installation/ns-3-installation.pdf)** format, **[HTML single-page](/docs/release/3.42/installation/singlehtml/index.html)** format
  * **Manual:** an in-depth coverage of the architecture and core of ns-3:
**[HTML](/docs/release/3.42/manual/html/index.html)** format, **[PDF](/docs/release/3.42/manual/ns-3-manual.pdf)** format, **[HTML single-page](/docs/release/3.42/manual/singlehtml/index.html)** format
  * **Model Library:** documentation on individual protocol and device models that build on the ns-3 core: **[HTML](/docs/release/3.42/models/html/index.html)** format, **[PDF](/docs/release/3.42/models/ns-3-model-library.pdf)** format, **[HTML single-page](/docs/release/3.42/models/singlehtml/index.html)** format
  * **Contributing Guide:** documentation on how to contribute to ns-3: **[HTML](/docs/release/3.42/contributing/html/index.html)** format, **[PDF](/docs/release/3.42/contributing/ns-3-contributing.pdf)** format, **[HTML single-page](/docs/release/3.42/contributing/singlehtml/index.html)** format
  * **[Doxygen API Documentation](/docs/release/3.42/doxygen/index.html)**:&nbsp; Coverage of the C++ APIs using Doxygen.
  * Documentation of the [Bake](https://www.nsnam.org/docs/bake/tutorial/html/index.html) build tool
  * [Release Errata](/wiki/Errata)
