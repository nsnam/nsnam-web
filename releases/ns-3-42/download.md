---
title: Download
layout: page
permalink: /releases/ns-3-42/download/
---
Please click the following link to download ns-3.42, released May 29, 2024:

  * [ns-allinone-3.42](/releases/ns-allinone-3.42.tar.bz2) (compressed source code archive)

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.42.tar.bz2 release archive is e246672ab42716e497c181f11f38ccb77ac76deb
