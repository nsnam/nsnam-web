---
title: Authors
layout: page
permalink: /releases/ns-3-42/authors/
---
The following people made source code and documentation contributions (commits) during the ns-3.42 development period:

* Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
* Alessio Bugetti <alessiobugetti98@gmail.com>
* André Apitzsch <andre.apitzsch@etit.tu-chemnitz.de>
* Chan Ruihua <chenrh20@mails.tsinghua.edu.cn>
* Davide Magrin <magrin.davide@gmail.com>
* Eduardo Almeida <enmsa@outlook.pt>
* Gabriel Ferreira <gabrielcarvfer@gmail.com>
* Jacob Janzen <jacob.a.s.janzen@gmail.com>
* Juan Leon <juanleon@uw.edu>
* Mattia Sandri <sandrimatt@dei.unipd.it>
* Menglei Zhang <menglei@nyu.edu>
* Muyuan Shen <shmy315@gmail.com>
* Matteo Pagin <mattpagg@gmail.com>
* Peter D. Barnes, Jr <barnes26@llnl.gov>
* Rami Abdallah <abdallah.rami@gmail.com>
* Rui Chen <rui@chenrui.dev>
* Sébastien Deronne <sebastien.deronne@gmail.com>
* Stefano Avallone <stavallo@unina.it>
* Tolik Zinovyev <tolik@bu.edu>
* Tom Henderson <tomh@tomh.org>
* Tommaso Pecorella <tommaso.pecorella@unifi.it>
* Vivek Jain <jain.vivek.anand@gmail.com>
* Wojtek Kosior <koszko@koszko.org>
* Zili Meng <zilim@ieee.org>
