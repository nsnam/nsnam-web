---
title: Authors
layout: page
permalink: /releases/ns-3-35/authors/
---
The following people made source code contributions (commits) during the ns-3.35 development period:

 * Aditya Chirania
 * Alexander Krotov
 * Ameya Deshpande
 * Caleb Bowers
 * Davide Magrin
 * Eduardo Almeida
 * Gabriel Arrobo
 * Gabriel Ferreira
 * Gauri Prasad
 * Michael Scoggin
 * Peter D. Barnes, Jr
 * Philip Hönnecke
 * Sandra Lagen
 * Sébastien Deronne
 * Sharan Naribole
 * Stefano Avallone
 * Szymon Szott
 * Tom Henderson
 * Tommaso Pecorella
