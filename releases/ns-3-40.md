---
title: ns-3.40
layout: page
permalink: /releases/ns-3-40/
---

**ns-3.40** was released on September 27, 2023, due to contributions from [twelve authors](https://www.nsnam.org/releases/ns-3-40/authors/).  This release features continued progress in the IEEE 802.11 models for the <a href="https://arxiv.org/pdf/2201.07499.pdf">multi-link operation (MLO)</a> Wi-Fi 7 feature.  Specifically, the support for MLO in STR (Simultaneous Transmit Receive) mode has been refined and enriched with the TID-to-Link Mapping support, a feature that allows distributing traffic flows of different Access Categories over different setup links.  Initial support at the PHY layer for MU MIMO was also added.  New <a href="https://dl.acm.org/doi/10.1145/3592149.3592156">energy source models</a> extend the capability of the energy module to support three new battery chemistries, and additional small improvements to the 802.15.4 LR-WPAN models have been made.  Many additional improvements and bug fixes are listed in the [RELEASE_NOTES](https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.40/RELEASE_NOTES.md) and [CHANGES](https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.40/CHANGES.md) files.

# Download

The ns-3.40 release download is available from <a href="/releases/ns-allinone-3.40.tar.bz2">this link</a>.  This download is a source archive that contains some additional tools (bake, netanim) in addition to the ns-3.40 source.  The ns-3 source code by itself can also be checked out of our Git repository by referencing the tag 'ns-3.40'.

# Documentation

The documentation is available in several formats from <a href="/releases/ns-3-40/documentation">this link</a>.

  <ul>
    <li>
      What has changed since ns-3.39? Consult the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.40/CHANGES.md"> changes</a> and <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.40/RELEASE_NOTES.md"> RELEASE_NOTES</a> pages.
    </li>
    <li>
      Errata containing any late-breaking information about the release can be found <a href="/wiki/Errata">here</a>
    </li>
    <li>
      A patch to upgrade from ns-3.39 to ns-3.40 can be found <a href="/releases/patches/ns-3.39-to-ns-3.40.patch">here</a>
    </li>
  </ul>
