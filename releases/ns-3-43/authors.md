---
title: Authors
layout: page
permalink: /releases/ns-3-43/authors/
---
The following people made source code and documentation contributions (commits) during the ns-3.43 development period:

* Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
* Alessio Bugetti <alessiobugetti98@gmail.com>
* André Apitzsch <andre.apitzsch@etit.tu-chemnitz.de>
* Andreas Boltres <andreas.boltres@partner.kit.edu>
* Aniket Singh <aniketsingh84646@gmail.com>
* Eduardo Almeida <enmsa@outlook.pt>
* Federico Guerra <federico@guerra-tlc.com>
* Gabriel Ferreira <gabrielcarvfer@gmail.com>
* Jay C. Surani <jaysurani333@gmail.com>
* Jonas Dittrich <j.dittrich2303@gmail.com>
* Kavya Bhat <kavyabhat@gmail.com>
* Nakayama Kenjiro <nakayamakenjiro@gmail.com>
* Peter Barnes <barnes26@llnl.gov>
* Peter D. Barnes, Jr <barnes26@llnl.gov>
* Rui Chen <rui@chenrui.dev>
* sanjaybhat2004 <sanjubhat2004@gmail.com>
* Satyarth Ratnani <satyarthratnani@gmail.com>
* Sébastien Deronne <sebastien.deronne@gmail.com>
* Silviu-Leonard Vatamanelu <silviu48vatamanelu@gmail.com>
* Stefano Avallone <stavallo@unina.it>
* Tolik Zinovyev <tolik@bu.edu>
* Tom Henderson <tomh@tomh.org>
* Tommaso Pecorella <tommaso.pecorella@unifi.it>
* Vikas Gaur <vikasgaurdws@gmail.com>
* Vivek Jain <jain.vivek.anand@gmail.com>
* wzfelix <wzfhrb.cn@gmail.com>
* @Ghostyhands at GitLab.com
* @donghuiyong at GitLab.com
