---
title: Download
layout: page
permalink: /releases/ns-3-43/download/
---
Please click the following link to download ns-3.43, released October 9, 2024:

  * [ns-allinone-3.43](/releases/ns-allinone-3.43.tar.bz2) (compressed source code archive)

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.43.tar.bz2 release archive is 31876b628be23a7b39a86d6828459760590f3e53
