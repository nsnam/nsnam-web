---
title: Download
layout: page
permalink: /releases/ns-3-39/download/
---
Please click the following link to download ns-3.39, released July 5, 2023:

  * [ns-allinone-3.39](/releases/ns-allinone-3.39.tar.bz2) (compressed source code archive)

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.39.tar.bz2 release archive is c0aa37adeedf18723202592271ffa43c4752f2ea
