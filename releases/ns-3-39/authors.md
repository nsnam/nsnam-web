---
title: Authors
layout: page
permalink: /releases/ns-3-39/authors/
---
The following people made source code contributions (commits) during the ns-3.39 development period:

* Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
* André Apitzsch <andre.apitzsch@etit.tu-chemnitz.de>
* Biljana Bojovic <biljana.bojovic@cttc.es>
* Carlos Natalino <carlos.natalino@chalmers.se>
* Davide Magrin <magrin.davide@gmail.com>
* Eduardo Almeida <enmsa@outlook.pt>
* Gabriel Ferreira <gabrielcarvfer@gmail.com>
* German Aizek <germanaizek@yandex.ru>
* Giovanni Grieco <giovanni.grieco@poliba.it>
* Juan Leon <juanvleonr@gmail.com>
* Karsten Heimann
* Katerina Koutlia <katerina.koutlia@cttc.es>
* Matteo Pagin <mattpagg@gmail.com>
* Raghuram Kannan <raghuramkannan40@gmail.com>
* Robert-ML
* Sébastien Deronne <sebastien.deronne@gmail.com>
* Stefano Avallone <stavallo@unina.it>
* Tom Henderson <tomh@tomh.org>
* Tommaso Pecorella <tommaso.pecorella@unifi.it>
* Yikun Wang <yikunwang6@gmail.com>
