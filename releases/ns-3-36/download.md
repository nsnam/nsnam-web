---
title: Download
layout: page
permalink: /releases/ns-3-36/download/
---
Please click the following link to download ns-3.36.1, released May 23, 2022:

  * [ns-allinone-3.36.1](/releases/ns-allinone-3.36.1.tar.bz2) (compressed source code archive)

For users of the original ns-3.36 release (April 30, 2022), a patch is available to update code to ns-3.36.1 from ns-3.36, at [this link](/release/patches/ns-3.36-to-ns-3.36.1.patch).

For users of the ns-3.35 release, a patch is available to update code to ns-3.36, at [this link](/release/patches/ns-3.35-to-ns-3.36.patch).  Other patches to migrate older versions of ns-3 (back to ns-3.17) to subsequent release versions can be found in the same directory; they must be applied sequentially to upgrade across multiple releases.

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.36.1.tar.bz2 release archive is da19007b17a2d426a9acab1c3dd9308b6b9efa42

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.36.tar.bz2 release archive is 207b2278270e05659091ae73083d6212090b9aa5
