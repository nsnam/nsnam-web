---
title: Download
layout: page
permalink: /releases/ns-3-44/download/
---
Please click the following link to download ns-3.44, released March 9, 2025:

  * [ns-allinone-3.44](/releases/ns-allinone-3.44.tar.bz2) (compressed source code archive)

The [sha1sum](https://linux.die.net/man/1/sha1sum) of the ns-allinone-3.44.tar.bz2 release archive is a90572b210326017ce144e9bf87768ca2aea4997
