---
title: Authors
layout: page
permalink: /releases/ns-3-44/authors/
---
The following people made source code and documentation contributions (commits) during the ns-3.44 development period:

* Anna-Birgitta Burmeister <anna-birgitta.burmeister@student.hpi.de>
* Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
* Alessio Bugetti <alessiobugetti98@gmail.com>
* André Apitzsch <andre.apitzsch@etit.tu-chemnitz.de>
* Amir Ashtari <aashtari@cttc.es>
* Eduardo Almeida <enmsa@outlook.pt>
* Gabriel Ferreira <gabrielcarvfer@gmail.com>
* Jared Ivey <jivey9@stl.gtri.org>
* Martin Belanger <martin.belanger@dell.com>
* Michael R. Davis <mrdvt92@yahoo.com>
* Muyuan Shen <shmy315@gmail.com>
* Nihar Kapasi <niharkkapasi@gmail.com>
* Peter D. Barnes, Jr <barnes26@llnl.gov>
* Puneet Kumar <puneet23067@iiitd.ac.in>
* Sébastien Deronne <sebastien.deronne@gmail.com>
* Sharan Naribole <sharan.naribole@gmail.com>
* Stefano Avallone <stavallo@unina.it>
* Tolik Zinovyev <tolik@bu.edu>
* Tom Henderson <tomh@tomh.org>
* Tommaso Pecorella <tommaso.pecorella@unifi.it>
* YifanFan17 <yifan.fan.seu@gmail.com>
* Yohei Kojima <yk@y-koj.net>

