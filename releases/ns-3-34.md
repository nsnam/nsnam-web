---
title: ns-3.34
layout: page
permalink: /releases/ns-3-34/
---

**ns-3.34** was released on July 14, 2021, due to contributions from [thirty seven authors](https://www.nsnam.org/releases/ns-3-34/authors/).  This release features a significantly refactored and improved Wi-Fi MAC and PHY design, enabling both **802.11ax uplink and downlink OFDMA models** and an improved internal organization corresponding to the IEEE 802.11 standard.  ns-3.34 contains the following additional new features:

 * A new **phased array antenna** model
 * A **TCP BBRv1** congestion control model
 * A round-robin multi-user **OFDMA scheduler** for Wi-Fi 802.11ax
 * A **Thompson sampling** Wi-Fi rate control algorithm
 * **FQ Cobalt and FQ PIE** queue disc models, and a set associative hash model

Many additional new features, small improvements, and bug fixes are listed in the [RELEASE_NOTES](https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.34/RELEASE_NOTES).

The funding support and technical guidance for 802.11ax features came from Intel Corporation and Cisco Systems, with leadership from Dr. Chittabrata Ghosh (Intel) and Malcolm Smith (Cisco), and key contributions from Dibakar Das and Dmitry Akhmetov (Intel).  The ns-3 implementation and review of the new features was led by Stefano Avallone, Sebastien Deronne, and Getachew Redietab, with key contributions from Davide Magrin.

# Download

The ns-3.34 release download is available from <a href="/releases/ns-allinone-3.34.tar.bz2">this link</a>.  This download is a source archive that contains some additional tools (bake, netanim, pybindgen) in addition to the ns-3.34 source.  The ns-3 source code by itself can also be checked out of our Git repository by referencing the tag 'ns-3.34'.

# Documentation

The documentation is available in several formats from <a href="/releases/ns-3-34/documentation">this link</a>.

  <ul>
    <li>
      What has changed since ns-3.33? Consult the <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.34/CHANGES.html"> changes</a> and <a href="https://gitlab.com/nsnam/ns-3-dev/blob/ns-3.34/RELEASE_NOTES"> RELEASE_NOTES</a> pages for ns-3.34.
    </li>
    <li>
      A patch to upgrade from ns-3.33 to ns-3.34 can be found <a href="/releases/patches/ns-3.33-to-ns-3.34.patch">here</a>
    </li>
    <li>
      Errata containing any late-breaking information about the release can be found <a href="/wiki/Errata">here</a>
    </li>
  </ul>
