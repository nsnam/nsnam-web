---
title: ns-3.22
layout: page
permalink: /releases/ns-3-22/
---
<div>
  <p>
    ns-3.22 was released on 5 February 2015. The main new features of this release are related to WiFi and LTE models. A frame aggregation mechanism for IEEE 802.11n networks, MPDU aggregation, has been added. Two rate adaptation algorithms that jointly control transmission power and data rate have also been added to the WiFi module. The support for vehicular wireless networks (VANET) based on WiFi has been extended by adding models for the channel-access coordination features of IEEE 1609.4, and a comprehensive VANET routing example that includes a Basic Safety Message (BSM) packet generator application and associated statistics counters. Regarding LTE models, in previous releases, the bearer release functionality was only partially supported; a complete release bearer procedure is now implemented. The <a href="http://code.nsnam.org/ns-3.22/file/7e569f59f476/RELEASE_NOTES">RELEASE_NOTES</a> list the many bugs fixed and small improvements made.
  </p>

  <ul>
    <li>
      The latest ns-3.22 source code can be downloaded from <a href="https://www.nsnam.org/release/ns-allinone-3.22.tar.bz2">here</a>
    </li>
    <li>
      What has changed since ns-3.21? Please also view the <a href="http://code.nsnam.org/ns-3.22/raw-file/7e569f59f476/CHANGES.html">changes</a> page. <li>
        The documentation is available in several formats from <a href="/releases/ns-3-22/documentation">here</a>.
      </li>
      <li>
        Errata containing late-breaking information about the release can be found <a href="http://www.nsnam.org/wiki/Errata">here</a>
      </li>
      <li>
        A patch to upgrade from the last release (ns-3.21) can be found <a href="https://www.nsnam.org/release/patches/ns-3.21-to-ns-3.22.patch">here</a>
      </li>
