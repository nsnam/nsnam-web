---
title: Presentations
layout: page
permalink: /documentation/presentations/
---
A lot of our users do presentations on how to use ns-3 for various specific purposes but these documents can be more generally useful and are thus included here for wider consumption.

| Title | Date | Formats |
| ----- | ---- | ------- |
| Workshop on Simulation of TCP Variants Using ns-3 [STVN Workshop brochure](/docs/contributed/tcp-variants-workshop-2016/STVN_Workshop-3.pdf) | April 15-16, 2016 | [Session-1](/docs/contributed/tcp-variants-workshop-2016/1-Session.pdf), [Session-2](/docs/contributed/tcp-variants-workshop-2016/2-Session.pdf), [Session-3](/docs/contributed/tcp-variants-workshop-2016/3-Session.pdf), [Session-4](/docs/contributed/tcp-variants-workshop-2016/4-Session.pdf), [Session-5](/docs/contributed/tcp-variants-workshop-2016/5-Session.pdf) |
| ns-3 overview given to [NITK ns-3 workshop](http://wing.nitk.ac.in/resources/SWN-2014-Brochure.pdf) | July 2014 | [PDF](/docs/ns-3-overview-july-2014.pdf) |
| Tutorials at the ns-3 Consortium kickoff meeting | March 2013 | [PDF](/consortium/activities/annual-meeting-march-2013/) (scroll down to find links) |
| Walid Younes's tutorial | June 2013 | [PDF](/tutorials/ns-3-tutorial-Walid-Younes.pdf) |
| Konstantinos Katsaros's tutorials for PGSDP workshop | 2012 | [Various PDFs](http://personal.ee.surrey.ac.uk/Personal/K.Katsaros/ns3.html) |
| ns-3 tutorial at GENI [GEC 9](http://www.geni.net/?p=1878) | November 4, 2010 | [Part 1 (PDF)](/tutorials/geni-tutorial-part1.pdf), [Part 2 (PDF)](/tutorials/geni-tutorial-part2.pdf), [DCE (PDF)](/tutorials/geni-dce.pdf), [NEPI (PDF)](/tutorials/geni-nepi.pdf) |
| Gustavo Carneiro's lab brief on ns-3 | April 20, 2010 | [PDF](/tutorials/NS-3-LABMEETING-1.pdf), [ODP](/tutorials/NS-3-LABMEETING.odp) |
| George Riley's ACM SpringSim keynote on ns-3 | March 2010 | [PPTX](/tutorials/SpringSim-2010-Riley.pptx) |
| Mathieu Lacage's [Trilogy summer school](http://trilogy-project.org/events/summerschool.html) talks | August 27th, 2009 | [Video](http://inl.info.ucl.ac.be/tutorials/tfiss09-lacage), [PDF](/tutorials/trilogy-summer-school.pdf), [PDF handouts](/tutorials/trilogy-summer-school-handouts.pdf), [Latex/examples source](http://code.nsnam.org/mathieu/tutorial-trilogy) |
| Mathieu Lacage's Tunis tutorial | April 7, 2009 | [PDF](/tutorials/ns-3-tutorial-tunis-apr09.pdf) |
| Workshop on ns-3: Tutorial part 1 (ns-3 features) | March 2, 2009 | [PDF](/workshops/wns3-2009/ns-3-tutorial-part-1.pdf), [Sample helper program](/workshops/wns3-2009/wns3-helper.cc) |
| Workshop on ns-3: Tutorial part 2 (end-to-end system) | March 2, 2009 | [PDF](/workshops/wns3-2009/ns-3-tutorial-part-2.pdf), [Sample low-level program](/workshops/wns3-2009/wns3-lowlevel.cc) |
| [Simutools 08](http://www.simutools.org/ns3.shtml) | March 5, 2008 | [PDF](/tutorials/simutools08/ns-3-tutorial-slides.pdf), [PPTX](/tutorials/simutools08/ns-3-tutorial-slides.ppt), [Handouts (PDF)](/tutorials/simutools08/ns-3-tutorial-handouts.pdf), [Source code](/tutorials/simutools08/ns-3-tutorial.tar.gz) |
