---
title: Report a bug
layout: page
permalink: /support/report-a-bug/
---

There are two primary ways to log bug reports, patches, or feature requests:

  1. email the [ns-developers](/support/mailing-lists) mailing list (you will need to subscribe to the list to be able to post, and first-time posts are moderated); or
  2. enter the bug into the [GitLab.com](https://gitlab.com/nsnam/ns-3-dev/-/issues) database yourself

We strongly prefer the second approach; email to the mailing list runs the risk of falling through the cracks. The second approach requires you to create your own GitLab.com account.

If you think you've found a bug and want to report it, please do the following three things first:

  1. [search issues](https://gitlab.com/nsnam/ns-3-dev/-/issues) to see if someone has entered a similar bug.
  2. [search Google](https://www.google.com/) to see if the bug has been previously encountered and discussed on an email thread
  3. if you are new to reporting bugs, please read the [bug writing guidelines](/bugzilla/page.cgi?id=bug-writing.html) written on our old Bugzilla page (the guidelines pertain to the newer use of GitLab.com)

Note that providing a simple, reduced test case that reliably reproduces the bug is often requested by the maintainers, so please try to craft such a test case and upload it to the issue tracker.
