---
title: Mailing lists
layout: page
permalink: /support/mailing-lists/
---
## List of ns-related mailing lists

The project maintains several mailing lists:

* **ns-3-users**: ns-3 user support. [Google Groups page](https://groups.google.com/forum/?fromgroups#!forum/ns-3-users)
  * Please read our <a href="https://www.nsnam.org/wiki/Ns-3-users-guidelines-for-posting" target="_blank">guidelines for posting</a>.
* **ns-developers**: ns-3 development discussions. [Google Groups page](https://groups.google.com/g/ns-developers).  Prior to November 2022, the ns-developers list was hosted at ISI (archives have recently been removed by ISI, we are working on restoring them).


## ns-3-users: Guidelines for posting

If you are focused on running and analyzing the output of a simulation based on builtin models, if you need to modify, or extend an existing model, if you need to implement a new model, or if you are simply lost and you don't know where to start, you are more than welcome to ask a question on our user mailing-list.

This mailing list is hosted on google groups: you can join and browse the archives [here](http://groups.google.com/group/ns-3-users).

However, before you do post on this mailing list, we ask you to consider the following check list:

  * Did you read our [tutorial](/documentation/latest) ?
  * Did you consult the [other available documentation](/documentation/latest) for our latest release?
  * Did you look at our [FAQ](/support/faq) ?
  * Did you spend at least 10 minutes searching a couple of keywords in a search engine ?

If you cannot answer yes to all the above, chances are high that your email will be politely ignored or, that you will be pointed to these documents. Remember that support on this mailing list is provided for free and that when you ask a question that is already answered in these documents, you use valuable time from other users who could be helping you more productively had you asked the right question.

Finally, don&#8217;t forget to consider the usual netiquette rules before you send an email: ignoring these exposes you to others ignoring your emails.

  * Don&#8217;t use ALL CAPS or exclamation points in your subject line
  * Use meaningful email subject lines
  * Don&#8217;t use abbreviations such as plz, etc.
  * Don&#8217;t ask for someone else to complete your course assignment for you
  * Don&#8217;t include the _urgent_, or worse, _urg_, or even worse, _plz, urg_ keywords
