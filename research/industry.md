---
layout: page
title: Industry talks
permalink: /research/industry/
---

The project is organizing a series of talks from speakers in industry who are using ns-3 to support their work.  These talks will be held on Zoom and are free but registration is requested to obtain the meeting link.

* **<span style="color: #6f9b17">Thursday Dec. 5, 2024, 14:00 UTC:</span> Makarand Kulkarni, Tejas Networks,** "Using ns-3 for Generating Synthetic Data in Cellular Networks"
    * [Registration form](https://forms.gle/WGJGUecLsH4bW6pr5)
    * [Abstract](/wp-content/uploads/2024/Makarand-Kulkarni-Abstract.pdf)
    * [Archived video](https://vimeo.com/1039125286?share=copy)
