---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2014/program-committee/
---
### **General Chair**

  * George Riley, Georgia Institute of Technology <riley@ece.gatech.edu>

### **Technical Program Committee Co-Chairs**

  * Peter D. Barnes, Lawrence Livermore National Laboratory <barnes26@llnl.gov>
  * L.Felipe Perrone, Bucknell University <perrone@bucknell.edu>

### **Technical Program Committee Members**

  * Alexander Afanasyev <alexander.afanasyev@ucla.edu>
  * Nicola Baldo <nbaldo@cttc.es>
  * Peter D. Barnes <pdbarnes@llnl.gov>
  * Carlos Giovanni Carvalho <cgionc@gmail.com>
  * Tom Henderson <tomh@tomh.org>
  * Sam Jansen <sam.jansen@starleaf.com>
  * Mathieu Lacage <mathieu.lacage@gmail.com>
  * Ruben Merz <ruben.merz@ieee.org>
  * Marco Miozzo <marco.miozzo@cttc.es>
  * Gary Pei <guangyu.pei@boeing.com>
  * L. Felipe Perrone <perrone@bucknell.edu>
  * Giuseppe Piro <g.piro@poliba.it>
  * Lalith Suresh Puthalath <suresh.lalith@gmail.com>
  * Ken Renard <kdrenard2@gmail.com>
  * Manuel Ricardo <mricardo@inescport.pl>
  * George Riley <riley@ece.gatech.edu>
  * Giovanni Stea <g.stea@iet.unipi.it>
  * James Sterbenz <jpgs@ittc.ku.edu>
  * Mohit Tahiliani <tahiliani.nitk@gmail.com>
  * Hajime Tazaki <tazaki@sfc.wide.ad.jp>
  * Thierry Turletti <turletti@sophia.inria.fr>
  * Klaus Wehrle <wehrle@comsys.rwth-aachen.de>

### **Steering Committee**

The workshop&#8217;s steering committee is transitioning to [the NS-3 Consortium Steering Committee](http://www.nsnam.org/consortium/governance/). For 2013-14, the steering committee consists of Tom Henderson, Felipe Perrone, and George Riley.
