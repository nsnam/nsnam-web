---
layout: page
title: Program
permalink: /research/wns3/wns3-2014/program/
---


**8:30 &#8212; 9:45:  Introduction and Keynote**
- <a href="http://www.nsnam.org/overview/wns3/wns3-2014/keynote/">Slightly Out-of-the-Box Wireless Networking Research</a> (Ellen W. Zegura)

**9:45 &#8212; 10:45: Congestion Control**
- Implementing Explicit Congestion Notification in ns-3 (B. Swenson)
- TCP CUBIC in ns-3 (B. Levasseur)

**10:45 &#8212; 11:00: Coffee Break**

**11:00 &#8212; 12:30: Wireless Networking Models**
- Extensions to LTE Mobility Functions for ns-3 (B. Herman)
- Experiences Porting a Radio Model to NS-3 from OPNET Modeler (A. White)
- Implementation and Evaluation of WAVE 1609.4/802.11p in ns-3 (J. Bu)

**12:30 &#8212; 14:00: Lunch**

**14:00 &#8212; 15:30: Applications and Optimizations**
- Reliable Communications over Wireless Mesh Networks with Inter and Intra-Flow Network Coding (D. Fernandez)
- Simulation Speedup of ns-3 Using Checkpoint and Restore (K. Harrigan)
- Cyber-Physical Co-Simulation of Smart Grid Applications using ns-3 (M. Tariq): **Awarded Best Paper!**

**15:30 &#8212; 15:45: Poster Preview**

**15:45 &#8212; 16:00: Coffee break**

**16:00 &#8212; 17:00: <a href="http://www.nsnam.org/research/wns3/wns3-2014/accepted-posters-demos-short-talks/">Poster/demo session</a>**
