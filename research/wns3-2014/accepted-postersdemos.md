---
layout: page
title: Accepted Posters/Demos
permalink: /research/wns3/wns3-2014/accepted-posters-demos-short-talks/
---
Current list of accepted posters and demos:

  * **A Relay Application for ns-3**

    Adrian Conway and Donald E. Smith
  * **A Satellite Module for ns-3**

    Jani Puttonen
  * **Performance Evaluation of VANET Routing Protocols Using ns-3**

    Scott Carpenter
  * **Extending the Energy Framework for Network Simulator 3 (ns-3)**

    Cristiano Tapparello, Hoda Ayatollahi and Wendi Heinzelman
  * **MultiPath-TCP in Network Simulator 3**

    Morteza Kheirkhah, George Parisis and Ian Wakeman
