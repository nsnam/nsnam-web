---
layout: page
title: Program Committee
permalink: /research/icns3/icns3-2025/program-committee/
---

# Technical Program Co-Chairs

* Tommaso Pecorella,  Università di Firenze <tommaso.pecorella@unifi.it>
* Mohit Tahiliani, NITK Surathkal <tahiliani.nitk@gmail.com>

# Proceedings Chair

* Eric Gamess, Jacksonville State University <egamess@gmail.com>

# General Chairs

* Taku Noguchi, Ritsumeikan University <noguchi@is.ritsumei.ac.jp>
* Alberto Gallegos Ramonet, Tokushima University <alramonet@is.tokushima-u.ac.jp>

# Technical Program Committee

To be announced.
