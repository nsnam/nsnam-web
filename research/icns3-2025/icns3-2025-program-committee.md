---
layout: page
title: Program Committee
permalink: /research/icns3/icns3-2025/program-committee/
---

# Technical Program Co-Chairs

* Tommaso Pecorella,  Università di Firenze <tommaso.pecorella@unifi.it>
* Mohit Tahiliani, NITK Surathkal <tahiliani.nitk@gmail.com>

# Proceedings Chair

* Eric Gamess, Jacksonville State University <egamess@gmail.com>

# General Chairs

* Taku Noguchi, Ritsumeikan University <noguchi@is.ritsumei.ac.jp>
* Alberto Gallegos Ramonet, Tokushima University <alramonet@is.tokushima-u.ac.jp>

# Technical Program Committee

Name                 | Institution                                 |
---------------------|-----------------------------------------------
Ramon Aguero         | University of Cantabria, Spain |
Eduardo Almeida      | INESC and FEUP, Portugal |
Hany Assasa          | Pharrowtech, Belgium |
Stefano Avallone     | University of Naples “Federico II”, Italy |
Peter Barnes         | Lawrence Livermore National Laboratory, USA |
Douglas Blough       | Georgia Institute of Technology, USA |
Biljana Bojovic      | Centre Tecnològic de Telecomunicacions de Catalunya, Spain |
Federico Chiariotti  | Aalborg University, Denmark |
Sebastien Deronne    | Deronne Software Engineering, Belgium |
Felipe Gomez-Cuba    | University of Vigo, Spain |
Tom Henderson        | Prometheus Computing/NIST, USA |
Pasquale Imputato    | University of Naples “Federico II”, Italy |
Vivek Jain           | |
Sian Jin             | Mathworks, USA |
Katerina Koutlia     | Centre Tecnològic de Telecomunicacions de Catalunya, Spain |
Yuchen Liu           | North Carolina State University, USA |
Sharan Naribole      | Apple Inc., USA |
Michele Polese       | Northeastern University, USA |
Hajime Tazaki        | IIJ Innovation Institute, Japan |
Thierry Turletti     | Inria, France |
Michael Welzl        | University of Oslo, Norway |
Tommaso Zugno        | Huawei Technologies |
