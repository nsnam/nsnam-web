---
layout: page
title: Travel Grants
permalink: /research/icns3/icns3-2025/travel-grants/
---

The following are external grants available for individuals wishing to present at an international conference in Japan, such as this year's ICNS3. Please note that the ICNS3 committee and the ns-3 project as a whole are not involved in the final decision regarding the awarding of these grants. Be sure to carefully review the requirements and submission deadlines for each grant. Typically, applications for these grants are open between April and August each year.

1. [Travel Expense Support for Inviting Overseas Researchers to International Academic Conferences](https://www.yazaki-found.jp/applications/international_aid.html)

2. [International Exchange Grant (for Inviting Researchers)](https://www.marubun-zaidan.jp/en/kokusai.html)

3. [International Information Science Foundation (Visiting researchers and presentations at international conferences)](http://www.iisf.or.jp/index-en.html)

4. [The NEC C&C Foundation (presentations at international conferences)](https://www.candc.or.jp/en/grants_conference.html)

