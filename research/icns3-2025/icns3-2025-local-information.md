---
layout: page
title: ICNS3 2025 Local Information
permalink: /research/icns3/icns3-2025/local-information/
---

# General Chair

Contact [Alberto Gallegos Ramonet](mailto:alramonet@is.tokushima-u.ac.jp) for any specific questions about local information.

# Location of the meeting

The meeting will be held at the [Ritsumeikan University, Osaka Ibaraki Campus](https://en.ritsumei.ac.jp/file.jsp?id=246778&f=.pdf), Japan.

# Visa information

Please look at the [list of countries](https://www.mofa.go.jp/j_info/visit/visa/short/novisa.html) that have a visa exemption agreement with Japan. If your country is included in the list, you usually do not need to apply for an entry visa to Japan.

If your country is not included in the list, you need to obtain a short-term stay visa (“tanki-shouyou visa” in Japanese) from the embassy of Japan in your country. Read carefully the Japanese visa guide provided by the Ministry of Foreign Affairs. For entry into Japan, a visa may be required for attendees from some countries.

Please make sure whether you need to apply for a visa or not on the website of the Ministry of Foreign Affairs of Japan at: http://www.mofa.go.jp/j_info/visit/visa/index.html.

## Visa Invitation Letter

Important information regarding invitation letters (please read):

Your name must be listed exactly as it appears on your passport. Any differences between the name on your passport and the name on your invitation letter or other documentation could lead to a delay and/or denial of your visa.

Please note ICNS3 organizing committee of the conference is not authorized to assist with the visa process beyond providing the Notification of Invitation Letter issued by ICNS3 committee board. Should your application be denied, ICNS3 organizing committee of the conference cannot change the decision of the Ministry of Foreign Affairs, nor will ICNS3 organizing committee of the conference engage in discussion or correspondence with the MOFA or the Embassy on behalf of the applicant. The registration fee cannot be refunded when the visa application of an individual is denied.

It takes time for the Embassy to process the visa application, please register as early as you can to make sure you have enough time.

# Local hotels

More information about options for accommodations will be posted at a later date.

