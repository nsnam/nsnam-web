---
layout: page
title: Call for Papers
permalink: /research/icns3/icns3-2025/call-for-papers/
---

**Updated February 25, 2025 to note that ICNS3 has been accepted into the ACM ICPS conference series, and accepted papers will be published in the ACM Digital Library**

**Updated February 25, 2025 to note that ACM is offering a subsidy to the Open Acces fee structure (detailed below).**

The **International Conference on ns-3 (ICNS3)**, formerly known as Workshop on ns-3 (WNS3), will be held on August 19-21, 2025 at the [Ritsumeikan University, Osaka Ibaraki Campus](https://en.ritsumei.ac.jp/file.jsp?id=246778&f=.pdf) and co-hosted by Tokushima University, Japan. The objective of the conference is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities.

ICNS3 2025 is planned to be an in-person event. The organizers request that an author from each accepted paper plan to attend in person. However, accommodations will be made for authors to present virtually if any hardships (such as visa issues, economic or other travel constraints) arise. Portions of ICNS3 2025 will be made available either live virtually or with archived recordings. Additional events will be planned during the same week (August 19-21), including the annual developers and maintainers meeting, and possibly training and tutorial sessions. More information on this will follow.

The conference has been accepted to the [ACM International Conference Proceeding Series (ICPS)](https://www.acm.org/publications/icps-series), and all papers will be published in the ACM Digital Library.

ACM has introduced a new open-access publishing model for the ICPS conferences. Authors based at institutions that are not yet part of the [ACM Open](https://libraries.acm.org/acmopen/open-participants) program and do not qualify for a full waiver will be required to pay an Article Processing Charge (APC) to publish their article in the [ACM Digital Library](https://dl.acm.org/action/doSearch?AllField=workshop+on+ns-3).

To determine whether or not an APC will be applicable to your article, please follow the detailed guidance [here](https://www.acm.org/publications/icps/author-guidance). From this web page, you will also find out the exact amount of the APC.

For 2025 ACM is providing a subsidy on APC pricing for ICPS, as follows:

- The 2025 subsidized APC rate for ICPS papers will be $350.
- For papers where any of the authors is a member of ACM, the APC rate will be $250.
- For ICPS papers whose corresponding author is based in a lower-middle-income country, the APC rate will be $175.
- For ICPS papers whose corresponding author is based in a lower-middle-income country and where any of the authors is a member of ACM, the APC rate will be $125.
- For ICPS papers whose corresponding author is based in a low-income country, no APC is required.


Two types of papers may be submitted:
- Full papers (7-9 pages)
- Short papers (4-6 pages)

In addition, artifacts for reproducibility will be
evaluated.  Please read below for the acceptable scope for full papers,
short papers, how artifacts evaluation will be handled, and submission
instructions.

# Full paper submission

ICNS3 invites authors to submit original high quality papers presenting
different aspects of developing and using ns-3. In such papers,
reproducibility and methodology will be a key reviewing criteria, as
explained below.

Papers on the validation and verification of ns-3 models or simulation
campaigns are especially encouraged, and may be grouped into a validation
session.

Topics of interest include, but are not limited to, the following:
 * new models, devices, protocols and applications for ns-3
 * using ns-3 in modern networking research
 * comparison with other network simulators and emulators
 * speed and scalability issues for ns-3
 * multiprocessor and distributed simulation with ns-3, including use of GPUs
 * validation of ns-3 models
 * credibility and reproducibility issues for ns-3 simulations
 * user experience issues of ns-3
 * frameworks for the definition and automation of ns-3 simulations
 * post-processing, visualisation and statistical analysis tools for ns-3
 * models ported from other simulators to ns-3
 * using real code for simulation with ns-3 and using ns-3 code in network applications
 * integration of ns-3 with testbeds, emulators, and other simulators or tools
 * using ns-3 API from programming languages other than C++ or Python
 * porting ns-3 to unsupported platforms
 * network emulation with ns-3
 * using ns-3 in education and teaching

Papers submitted between 7-9 pages will be reviewed as full papers
and classified as such in the proceedings.

# Short paper submission

We solicit novel short papers between 4-6 pages in length, for publishing
in the proceedings in a short paper track.  Short papers are intended
to follow a scope similar to full papers, but for work that may not
be lengthy enough for a 7-9 page submission.  In particular, the following
topics are encouraged:

  * Verification, validation, or performance of ns-3 or its models
  * Use of ns-3 in industry
  * Special applications of ns-3
  * Educational use of ns-3
  * Shorter papers that are otherwise within scope of the full paper track

Papers about ns-3 use in industry are suggested to address these questions:
 * What specific R&D questions did you or do you want to answer by simulation?
 * Why and how did you choose ns-3 as the appropriate tool for your application?
 * What surprises did you find, in correctness/behavior? in implementation? in learning curve?
 * What are the remaining barriers to addressing fully your R&D questions?
 * What general capabilities would have made your work easier/faster?

# Submission instructions

Authors should submit papers through
[EasyChair](https://easychair.org/conferences/?conf=icns32025)
in PDF format, complying with
[ACM Primary article template](https://www.acm.org/publications/proceedings-template). Submitted papers must not have been submitted for review or
published (partially or completely) elsewhere.  All papers must be written
in English.

For those authors using [Overleaf](https://overleaf.com), an [ACM template](https://www.overleaf.com/latex/templates/acm-conference-proceedings-primary-article-template/wbvnghjbzwpc) is available.

# Acceptance Criteria

Every paper will be peer reviewed by the technical program committee.
Papers will be accepted based on the relevance, novelty, and impact of the
contribution, as well as the quality of writing and presentation.

Authors presenting new ns-3 models, frameworks, integration setups, etc. are
encouraged to include all traditional parts of a scientific paper:
introduction, motivation, related work, assumptions, verification and
validation, conclusions and references. As a general rule, papers that
only document source code will be rejected.

Authors presenting networking research using ns-3 are encouraged to follow
best simulation practices and focus particularly on the credibility and
[reproducibility](https://www.nsnam.org/wiki/Reproducible_papers) of
simulation results.  We strongly encourage authors of all papers,
demonstrations, and posters to include links to relevant source code and
instructions on how to use it. This will make contributions more useful
for the ns-3 community. If code has been submitted through the ns-3
merge request process, or published as a module in the ns-3 app store,
please list the link(s), this will be considered a plus.  Published,
high-quality artificts will be a consideration in the Best Paper
evaluation, and in the awarding of ACM artifacts badges (see below).

Please do not hesitate to contact the conference chairs if you are uncertain
whether your submission falls within the scope of the conference.

# Artifacts Evaluation

Accepted papers will be reviewed by the technical program committee
for consideration of the awarding of
**[ACM artifacts badges](https://www.acm.org/publications/policies/artifact-review-and-badging-current)**,
according to the criteria listed on the [ICNS3 2025 artifacts page](/research/icns3/icns3-2025/artifacts), and exemplary papers will be noted on the program page.

For an example of a paper from WNS3 2022 that produced high-quality artifacts
enabling the easy reproduction of all paper figures, please see the
paper by **Nicolas Rybowski and Olivier Bonaventure, Evaluating OSPF convergence
with NS3-DCE**.
[paper](https://dl.acm.org/doi/10.1145/3532577.3532597);
[video](https://vimeo.com/732258850);
[slides](https://www.nsnam.org/workshops/wns3-2022/16-rybowski-slides.pdf)

Four examples from [WNS3 2023](/research/wns3/wns3-2023/program/) were listed as exemplary by the artifacts evaluation committee:

1.  Eduardo Nuno Almeida, Helder Fontes, Rui Campos and Manuel Ricardo, Position-Based Machine Learning Propagation Loss Model Enabling Fast Digital Twins of Wireless Networks in ns-3
2.  Shengjie Shu, Wenjun Yang, Jianping Pan and Lin Cai, A Multipath Extension to the QUIC Module for ns-3
3.  Wesley Garey, Tanguy Ropitault, Richard Rouil, Evan Black and Weichao Gao, O-RAN with Machine Learning in ns-3
4.  Juan V. Leon Rosas, Thomas R. Henderson and Sumit Roy, Verification of ns-3 Wi-Fi Rate Adaptation Models on AWGN Channels

Six papers from [WNS3 2024](/research/wns3/wns3-2024/program/) were listed as exemplary by the artifacts evaluation committee.

# Copyright Policy

Accepted papers are planned to be published in the ACM Digital Library, which will
require copyright transfer to ACM under their [normal terms and conditions](https://www.acm.org/publications/policies/publication-rights-and-licensing-policy/). In the spirit
of open source, we encourage authors of published papers to exercise their
right to publish author-prepared versions on their respective home page,
on a publicly accessible server of their employer, or on a pre-print server.
Upon request, we will provide links
from the ICNS3 program on the ns-3 web site to the author-prepared version.

# Plagiarism Policy

We follow the [ACM standards and conduct](http://www.acm.org/publications/policies/plagiarism_policy) regarding plagiarism.

# Use of generative AI

ACM has updated their [policy on authorship](https://www.acm.org/publications/policies/new-acm-policy-on-authorship) regarding the use of generative AI.  Please review this and note that the use of generative AI tools and technologies and create content is permitted but must be fully disclosed.

# Reviewing Process Conflict of Interest Policy

Reviewers will follow the [ACM Conflict-of-Interest Guidelines](https://www.acm.org/publications/policies/conflict-of-interest).
Authors are requested to identify potential conflicts-of-interest among the
conference’s technical program committee.

# Demonstrations and Short Talks

In addition to the published paper track, we will organize an exhibition-style
demonstration and short talks session.
The aim is to foster interactive discussions on work-in-progress,
new problem statements, ideas for future development related to ns-3, and
display innovative prototypes.

A separate Call for Short Talks and Demos will be posted at a later date.
Accepted short talk and demo abstracts will be published on the ns-3 web site.
At least one author of each accepted demo/short talk must register and
present the work at the conference.  Remote presentation may be arranged.

# Awards

One Best Paper will be selected from the full paper track by the TPC
chairs and will be announced at the conference.

# Travel grants

Please see a [separate page](/research/icns3/icns3-2025/travel-grants/) for information on possible travel grants.

# Visa information

Please review the [Local Information](/research/icns3/icns3-2025/local-information/) page to check whether you need to obtain a visa to visit Japan.

# Registration

Registration information will be posted at a later date.
At least one author of each accepted paper must
register and present the work at the conference.

# Technical Program Co-Chairs

* Tommaso Pecorella,  Università di Firenze
* Mohit Tahiliani, NITK Surathkal

# Proceedings Chair

* Eric Gamess, Jacksonville State University

# General Chair

* Taku Noguchi, Ritsumeikan University
* Alberto Gallegos Ramonet, Tokushima University

# Important Dates

* Paper submission deadline: March 21, 2025, 17:00 EST
* Notification of acceptance: late May
* Dates of conference:  August 19-21
