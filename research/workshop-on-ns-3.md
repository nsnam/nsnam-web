---
layout: page
title: Workshop on ns-3
permalink: /research/wns3/
---

The **Workshop on ns-3** was an annual academic workshop focusing on advances
to ns-3 and the design and performance of ns-3.  The workshop was organized
by the [ns-3 Consortium](/consortium/) and ran from 2009-2024.   From 2014-2024, proceedings were published in the
[ACM digital library](https://dl.acm.org).
