---
layout: page
title: Call for Advanced Tutorials
permalink: /research/wns3/wns3-2021/call-for-tutorials/
---

The **Workshop on ns-3 (WNS3)** will be held as a fully virtual event, starting
on June 23, 2021 and tentatively planned for two days.
Additional events will be planned during the week starting June 21, including
advanced tutorials on topics related to ns-3.

WNS3 invites proposals for 1-hour or 2-hour tutorials on advanced topics related
to ns-3. The tutorials, to be presented through an online session, scheduled to take
place during the week starting June 21 (most likely Tuesday and/or Friday). Tutorials
will be open to the ns-3 community at large.

We seek proposals for tutorials that can clearly present advanced
topics related to ns-3, such as, for example, new models or functionalities for ns-3,
best practices for simulations, use cases for ns-3, or integration with external tools.
Tutorials that will tailored to be delivered in an online environment, with working
code examples, and with interactive elements will be particularly appreciated.

# Proposal format

Each tutorial proposal should be written in English and must indicate the following:
 * Tutorial title
 * Names and affiliations of the tutorial speakers
 * Abstract (< 200 words)
 * Format: 1 hour or 2 hours
 * Description: no more than 1 page describing the tutorial content
 * Materials: a description of the additional material (if any) that will provided to the participants (slides, code, etc)
 * Bio sketches: short bios (< 100 words) for each tutorial speaker

# Submission instructions

Speakers should submit tutorial proposal via email (subject "[WNS3] Tutorial proposal"),
copying the WNS3 PC chairs (m.polese@northeastern.edu and stavallo@unina.it)

# Acceptance Criteria

Tutorials will be reviewed by the WNS3 PC chairs, with help of external reviewers
if needed or in case of conflicts. Tutorials will be considered for their relevance,
quality, and timeliness. The WNS3 PC chairs will select tutorial proposals to
avoid duplication of topics and guarantee a diverse set of presentations.

Please do not hesitate to contact the workshop chairs if you are uncertain
whether your tutorial proposal falls within the scope of the workshop.

# Important Dates

Tutorial proposal submission deadline: April 22nd, 17:00 PST.

Notification of acceptance: April 31st.


