---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2021/program-committee/
---

# Technical Program Co-Chairs

* Stefano Avallone <stavallo@unina.it>
* Michele Polese <m.polese@northeastern.edu>

# Proceedings Chair

* Eric Gamess, Jacksonville State University <egamess@gmail.com>

# Technical Program Committee

Name                 | Institution                                        |
---------------------|-----------------------------------------------------
Ramon Aguero         | University of Cantabria, Spain |
Hany Assasa          | Pharrowtech, Belgium |
Peter Barnes         | Lawrence Livermore National Laboratory, USA |
Doug Blough          | Georgia Institute of Technology, USA |
Federico Chiariotti  | Aalborg University, Denmark |
Sébastien Deronne    | Televic Conference, Belgium |
Simone Ferlin        | Ericsson Research, Sweden |
Helder Fontes        | INESC and FEUP |
Eric Gamess          | Jacksonville State University, USA |
Lorenza Giupponi     | Centre Tecnològic de Telecomunicacions de Catalunya, Spain |
Felipe Gomez-Cuba    | University of Vigo, Spain |
Tom Henderson        | University of Washington, USA |
Pasquale Imputato    | University of Napoli Federico II, Italy |
Anil Jangam          | Cisco Systems, USA |
Dong Jin             | Illinois Institute of Technology, USA |
Junseok Kim          | Samsung Electronics, South Korea |
Leonardo Lanante     | Kyushu Institute of Technology, Japan |
Davide Magrin        | University of Napoli Federico II, Italy |
Spyridon Mastorakis  | University of Nebraska, Omaha, USA |
Vedram Miletic       | Heidelberg Institute for Theoretical Studies, Germany |
Mohit Tahiliani      | National Institute of Technology Karnataka, India |
Hajime Tazaki        | IIJ Innovation Institute, Japan |
Thierry Turletti     | Inria, France |
Tommaso Zugno        | University of Padova, Italy |
