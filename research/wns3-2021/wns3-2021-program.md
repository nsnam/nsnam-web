---
layout: page
title: WNS3 2021 Program
permalink: /research/wns3/wns3-2021/program/
---
# Proceedings

The proceedings will be published in the [ACM Digital Library](https://dl.acm.org).

# Schedule

The virtual workshop will span five days (June 21-25) from 9am-1pm EDT (1300-1700 UTC) each day.

# Monday, June 21:  Tutorials

Tutorials are further described (and materials and video links provided) on the [tutorial page](/research/wns3/wns3-2021/tutorials/).

**1300 UTC, Tutorial 1:** Tom Henderson, Using ns-3 with the Powder wireless testbed

**1500 UTC, Tutorial 2:** Hao Yin (University of Washington) and Pengyu Liu (Huazhong University of Science and Technology), Machine Learning in ns-3: ns3-ai module, Introduction and Hands-on Example

# Tuesday, June 22:  Tutorials

Tutorials are further described (and materials and video links provided) on the [tutorial page](/research/wns3/wns3-2021/tutorials/).

**1300 UTC, Tutorial 3:** Davide Magrin, Doing Research with ns-3 and SEM

**1400 UTC, Tutorial 4:** Tom Henderson, How to contribute code to ns-3

**1500 UTC, Tutorial 5:** Sian Jin and Tom Henderson (University of Washington), Efficient PHY Layer Abstraction in ns-3: Principles and Implementation

# Wednesday, June 23:  Paper presentations and short talks

#### Paper Session 1 (1300 UTC):  Wi-Fi/IoT session 1

Session Chair:  Leonardo Lanante, Kyushu Institute of Technology

*  Hany Assasa, Nina Grosheva, Tanguy Ropitault, Steve Blandino, Nada Golmie and Joerg Widmer, Implementation and Evaluation of a WLAN IEEE 802.11ay Model in Network Simulator ns-3 **(Awarded Best Paper)**; [video](https://vimeo.com/576257227); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-1/WNS3_2021_Grosheva.pdf)
*  Davide Magrin, Stefano Avallone, Sumit Roy and Michele Zorzi, Validation of the ns-3 802.11ax OFDMA Implementation; [video](https://vimeo.com/576259861); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-1/wns3_2021_magrin.pdf)
*  Sian Jin, Sumit Roy and Thomas R. Henderson, EESM-log-AR: An Efficient Error Model for OFDM MIMO Systems over Time-Varying Channels in ns-3; [video](https://vimeo.com/576258808); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-1/WNS3_2021_Sian.pdf)

#### Paper Session 2 (1430 UTC):  Wi-Fi/IoT session 2

Session Chair:  Getachew Redietab, Orange

*  Yuchen Liu, Shelby Crisp and Douglas Blough, Performance Study of Statistical and Deterministic Channel Models for mmWave Wi-Fi Networks in ns-3; [video](https://vimeo.com/577827058); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-2/wns3_2021_yuchen.pdf)
*  Vitor Lamela, Helder Fontes, Jose Ruela, Manuel Ricardo and Rui Campos, Reproducible MIMO Operation in ns-3 using Trace-based Wi-Fi Rate Adaptation; [video](https://vimeo.com/577828012); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-2/wns3_2021_HelderFontes.pdf)
*  Martina Capuzzo, Carmen Delgado, Jeroen Famaey and Andrea Zanella, An ns-3 implementation of a battery-less node for energy-harvesting Internet of Things; [video](https://vimeo.com/577833152); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-2/wns3_2021_capuzzo_ns3.pdf)

#### Lightning talks (1600 UTC):  Session 1

Session Chair:  Stefano Avallone, University of Naples Federico II

* Luca Lusvarghi, Assessing NR-V2X Mode 2 Achievable Performance with a Custom ns-3 Module
* Narayan G, Dhanasekar M, Shanthanu S Rai, Leslie Monis, Mohit P. Tahiliani, Validating Congestion Control Mechanisms and Queue Disciplines in ns-3 using NeST
* Mads Smed Enggaard Thomassen, Kasper Stenholt Winkler, Davide Magrin, Michele Albano, Towards simulating the full LORAWAN protocol
* Mohammad Afhamisis, Maria Rita Palattella, Sebastian Barillaro, Simulating a Satellite LoRaWAN Network with ns-3

# Thursday, June 24:  Paper presentations and short talks

#### Paper Session 3 (1300 UTC):  5G

Session Chair:  Richard Rouil, NIST

*  Katerina Koutlia, Biljana Bojovic, Sandra Lagen and Lorenza Giupponi, Novel Radio Environment Map for the ns-3 NR Simulator; [video](https://vimeo.com/577835040); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-3/wns3_2021_REM.pdf)
*  Tommaso Zugno, Matteo Drago, Sandra Lagen, Zoraze Ali and Michele Zorzi, Extending the ns-3 Spatial Channel Model for Vehicular Scenarios; [video](https://vimeo.com/577837992); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-3/wns3_2021_zugno.pdf)
*  Biljana Bojovic, Sandra Lagen and Lorenza Giupponi, Realistic beamforming design using SRS-based channel estimate for ns-3 5G-LENA module; [video](https://vimeo.com/577842244); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-3/wns3_2021_bojovic.pdf)

#### Paper Session 4 (1430 UTC):  Applications and Tools

Session Chair:  Mohit Tahiliani, NITK Surathkal

*  Mattia Lecci, Andrea Zanella and Michele Zorzi, An ns-3 Implementation of a Bursty Traffic Framework for Virtual Reality Sources; [video](https://vimeo.com/577868394); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-4/wns3_2021_lecci.pdf)
*  Evan Black, Samantha Gamboa and Richard Rouil, NetSimulyzer: a 3D Network Simulation Analyzer for ns-3; [video](https://vimeo.com/577871928); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-4/wns3_2021_black.pdf)
*  Cedrik Schüler, Manuel Patchou, Benjamin Sliwa and Christian Wietfeld, Robust Machine Learning-Enabled Routing for Highly Mobile Vehicular Networks with PARRoT in ns-3; [video](https://vimeo.com/577876213); [slides](https://www.nsnam.org/wp-content/uploads/2021/wns3-session-4/wns3_2021_schueler.pdf)

#### Lightning talks (1600 UTC):  Session 2

Session Chair:  Michele Polese, Northeastern University

* Jianfei Shao, Minh Vu, Mingrui Zhang, Lisong Xu, Symbolic NS-3 for efficient exhaustive testing
* Ben Newton, Clipping for More Efficient Large-Scale Simulations in ns-3
* Jacob Fenger, Path towards Faster PDES of wireless networks: Load-balancing with Charades

# Friday, June 25:  Tutorial and consortium meeting

Tutorials are further described (and materials and video links provided) on the [tutorial page](/research/wns3/wns3-2021/tutorials/).

**1300 UTC, Tutorial 6:** Evan Black, An Intro to the NetSimulyzer Visualizer

**1500 UTC, [ns-3 Consortium](https://www.nsnam.org/consortium/) Annual Meeting**
