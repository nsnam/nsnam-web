---
layout: page
title: WNS3 2023 Local Information
permalink: /research/wns3/wns3-2023/local-information/
---

# General Chair

Contact [Tom Henderson](mailto:tomhend@uw.edu) for any specific questions about local information.

# Location of the meeting

The meeting was held at the [Holiday Inn Arlington At Ballston](https://www.ihg.com/holidayinn/hotels/us/en/arlington/wasfx/hoteldetail?cm_mmc=GoogleMaps-_-HI-_-US-_-WASFX).

# Airports

Both Dulles (IAD) and Washington Reagan (DCA) airports are recommended to fly to the region.

# Local public transportation

The hotel is within walking distance of the [Ballston-MU](https://www.wmata.com/rider-guide/stations/ballston.cfm) Orange and Silver line Metro trains.

* The [Washington DC Metro](https://www.wmata.com/schedules/maps/upload/June-3-2023-System-Map-English.pdf) system serves the Dulles International Airport using the Silver line.  The Ballston-MU stop (closest to meeting hotel) is on the silver line. Information about how to find the Metro terminal at the Dulles airport can be found [here](https://www.flydulles.com/parking-transportation/dulles-airport-metrorail-station)

*   [Google Map view](https://www.google.com/maps/dir/Ballston-MU,+4230+Fairfax+Dr,+Arlington,+VA+22201/Holiday+Inn+Arlington+at+Ballston,+an+IHG+Hotel,+Fairfax+Drive,+Arlington,+VA/@38.8819055,-77.1170099,17z/data=!4m14!4m13!1m5!1m1!1s0x89b7b4250609e53b:0x28eb804b1bb2ab4a!2m2!1d-77.111419!2d38.8819855!1m5!1m1!1s0x89b7b43bae938353:0xcde1d3bcf9f2af79!2m2!1d-77.1174939!2d38.8816548!3e2?hl=en-US&entry=ttu) of the approximately 7 minute walk from the Ballston-MU  to the Holiday Inn hotel.
