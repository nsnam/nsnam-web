---
layout: page
title: Call for Lightning Talks
permalink: /research/wns3/wns3-2023/call-for-lightning-talks/
---

The **Workshop on ns-3 (WNS3)** will be held on June 28-29, 2023 at the
[Holiday Inn Arlington at Ballston](https://www.ihg.com/holidayinn/hotels/us/en/arlington/wasfx/hoteldetail).  In addition to the paper presentations and lightning talks, additional ns-3 events will be held on June 26-27, 2023 at the same hotel, as part of the overall ns-3 Annual Meeting.

Do you have some work in progress that you would like to share with the ns-3 community?  The Technical Program Committee co-chairs are soliciting [lightning talk](https://en.wikipedia.org/wiki/Lightning_talk) proposals for the Workshop on ns-3.

TPC chairs will select roughly 10 such talks based on timeliness and relevance (the exact number will depend on the duration of the paper presentations).  Submissions are open until mid-May and speakers will be notified by the end of May. We encourage the submission of talks that will spark some discussion with the broader community during the meeting.  In-person talks are preferred, but virtual talks are also acceptable if necessary.

# Proposal format

Each lightning talk proposal should be written in English and must indicate the following:
 * Title
 * Names and affiliation of the presenter
 * Abstract (< 200 words)
 * Whether the speaker plans to attend in person or present virtually

# Submission instructions

Speakers should submit lightning talk proposals via email (subject "[WNS3] Lightning talk"),
to both of the WNS3 PC chairs (p.imputato@gmail.com and yliu322@ncsu.edu).

# Acceptance Criteria

It is anticipated that there will be space for roughly ten such talks.  The TPC
co-chairs will decide on the accepted talks based on their assessment of
relevance and interest to the community.

# Important Dates

<s>Lightning talk proposal submission deadline: May 15th, 2023.</s>

<s>Notification of acceptance:  Rolling; no later than May 31st.</s>
