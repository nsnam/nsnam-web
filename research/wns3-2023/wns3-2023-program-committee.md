---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2023/program-committee/
---

# Technical Program Co-Chairs

* Pasquale Imputato <p.imputato@gmail.com>
* Yuchen Liu <yuchen.liu@ncsu.edu>

# Proceedings Chair

* Eric Gamess, Jacksonville State University <egamess@gmail.com>

# Technical Program Committee

Name                 | Institution                                 |
---------------------|-----------------------------------------------
Ramon Aguero         | University of Cantabria, Spain |
Hany Assasa          | Pharrowtech, Belgium |
Stefano Avallone     | University of Naples “Federico II”, Italy |
Peter Barnes         | Lawrence Livermore National Laboratory, USA |
Douglas Blough       | Georgia Institute of Technology, USA |
Federico Chiariotti  | Aalborg University, Denmark |
Sébastien Deronne    | Deronne Software Engineering, Belgium |
Helder Fontes        | INESC and FEUP |
Eric Gamess          | Jacksonville State University, USA |
Felipe Gomez-Cuba    | University of Vigo, Spain |
Tom Henderson        | University of Washington, USA |
Anil Jangam          | Cisco Systems, USA |
Yubing Jian          | Qualcomm, USA |
Dong Jin             | Illinois Institute of Technology, USA |
Sian Jin             | Mathworks, USA |
Junseok Kim          | Samsung Electronics, South Korea |
Sandra Lagen         | Centre Tecnològic de Telecomunicacions de Catalunya, Spain |
Sharan Naribole      | Apple Inc., USA |
Michele Polese       | Northeastern University, USA |
Spyridon Mastorakis  | University of Nebraska, Omaha, USA |
Mohit Tahiliani      | National Institute of Technology Karnataka, India |
Hajime Tazaki        | IIJ Innovation Institute, Japan |
Thierry Turletti     | Inria, France |
Tommaso Zugno        | Huawei Technologies |

# Artifacts Evaluation Committee

To be provided.
