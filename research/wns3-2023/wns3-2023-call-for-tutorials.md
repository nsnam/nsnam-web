---
layout: page
title: Call for Tutorials
permalink: /research/wns3/wns3-2023/call-for-tutorials/
---

**Note:** The call for tutorials is now closed; two tutorials have been accepted.

The **Workshop on ns-3 (WNS3)** will be held on June 28-29, 2023 at the
[Holiday Inn Arlington at Ballston](https://www.ihg.com/holidayinn/hotels/us/en/arlington/wasfx/hoteldetail).  In addition to the paper presentations and lightning talks, additional ns-3 events will be held on June 26-27, 2023 at the same hotel, as part of the overall ns-3 Annual Meeting.

WNS3 invites proposals for 1-hour or 2-hour tutorials on topics related
to ns-3. Tutorials will be scheduled sometime between June 26-29 (scheduling to be worked out with the meeting organizers).  Although ns-3 will be a hybrid event, tutorial presenters are encouraged to attend in person.

We seek proposals for tutorials that can clearly present advanced
topics related to ns-3, such as, for example, new models or functionalities for ns-3,
best practices for simulations, use cases for ns-3, or integration with external tools.
Tutorials with working code examples and with interactive elements will be
particularly appreciated.  Please see past editions of WNS3 for examples of past tutorials.

# Proposal format

Each tutorial proposal should be written in English and must indicate the following:
 * Tutorial title
 * Names and affiliations of the tutorial speakers
 * Abstract (< 200 words)
 * Format: 1 hour or 2 hours
 * Venue: In person presentation or virtual (in person attendance is preferred)
 * Description: no more than 1 page describing the tutorial content
 * Materials: a description of the additional material (if any) that will provided to the participants (slides, code, etc)
 * Bio sketches: short bios (< 100 words) for each tutorial speaker

# Submission instructions

Prospective speakers should submit tutorial proposals via email (subject "[WNS3] Tutorial proposal"),
to both of the WNS3 PC chairs (p.imputato@gmail.com and yliu322@ncsu.edu).

# Acceptance Criteria

Tutorials will be reviewed by the WNS3 PC chairs, with help of external reviewers
if needed or in case of conflicts. Tutorials will be considered for their relevance to the user community and for speaker experience with ns-3.

Please do not hesitate to contact the workshop chairs if you are uncertain
whether your tutorial proposal falls within the scope of the workshop.

# Important Dates

<s>Tutorial proposal submission deadline: April 3, 2023.</s>

<s>Notification of acceptance: Rolling (no later than April 21st)</s>

