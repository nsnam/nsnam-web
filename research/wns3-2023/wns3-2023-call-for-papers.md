---
layout: page
title: Call for Papers
permalink: /research/wns3/wns3-2023/call-for-papers/
---
**Updated December 21, 2022 to add call for short papers**

**Updated January 12, 2023 to clarify plans for proceedings**

**Updated January 17, 2023 to extend the paper deadline to Feb. 12**

**Updated February 6, 2023 to note that accepted papers will be published in ACM Digital Library**

**Updated February 7, 2023 to extend the paper deadline to Feb. 19**

**Updated March 30, 2023 to list the expected notification date**

**Updated April 5, 2023 to list the notification date as April 6 (notifications will be sent out Thursday April 6)**

The **Workshop on ns-3 (WNS3)** will be held on June 28-29, 2023 at the
[Holiday Inn Arlington at Ballston](https://www.ihg.com/holidayinn/hotels/us/en/arlington/wasfx/hoteldetail).
The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities.

WNS3 2023 is planned to be an in-person workshop.  The organizers request that
an author from each accepted paper plan to attend in-person.  However,
accommodations will be made for authors to present virtually if any
hardships (such as visa issues, economic or other travel constraints) arise.
Portions of WNS3 2023 will be made available either live virtually or
with archived recordings.
Additional events will be planned during the same week,
including developer meetings and the annual ns-3 Consortium meeting, and
possibly training and tutorial sessions. More information on this will follow.

The workshop has been accepted to the [ACM International Conference Proceeding Series (ICPS)](https://www.acm.org/publications/icps-series), and accepted WNS3 papers will be published in the ACM Digital Library.

Two types of papers may be submitted:
- Regular papers (7-9 pages)
- **(new for 2023)** Short papers (4-6 pages)

In addition (also new for 2023), artifacts for reproducibility will be
evaluated.  Please read below for the acceptable scope for regular papers,
short papers, how artifacts evaluation will be handled, and submission
instructions (deadline in early February 2023).

# Regular paper submission

WNS3 invites authors to submit original high quality papers presenting
different aspects of developing and using ns-3. In such papers,
reproducibility and methodology will be a key reviewing criteria, as
explained below. Topics of interest include, but are not limited to,
the following:
 * new models, devices, protocols and applications for ns-3
 * using ns-3 in modern networking research
 * comparison with other network simulators and emulators
 * speed and scalability issues for ns-3
 * multiprocessor and distributed simulation with ns-3, including use of GPUs
 * validation of ns-3 models
 * credibility and reproducibility issues for ns-3 simulations
 * user experience issues of ns-3
 * frameworks for the definition and automation of ns-3 simulations
 * post-processing, visualisation and statistical analysis tools for ns-3
 * models ported from other simulators to ns-3
 * using real code for simulation with ns-3 and using ns-3 code in network applications
 * integration of ns-3 with testbeds, emulators, and other simulators or tools
 * using ns-3 API from programming languages other than C++ or Python
 * porting ns-3 to unsupported platforms
 * network emulation with ns-3
 * using ns-3 in education and teaching

Papers submitted between 7-9 pages will be reviewed as regular papers
and classified as such in the proceedings.

# Short paper submission

We solicit novel short papers between 4-6 pages in length, for publishing
in the proceedings in a short paper track.  Short papers are intended
to follow a scope similar to regular papers, but for work that may not
be lengthy enough for a 7-9 page submission.  In particular, the following
topics are encouraged:

  * Verification, validation, or performance of ns-3 or its models
  * Use of ns-3 in industry
  * Special applications of ns-3
  * Educational use of ns-3
  * Shorter papers that are otherwise within scope of the regular paper track

Papers about ns-3 use in industry are suggested to address these questions:
 * What specific R&D questions did you or do you want to answer by simulation?
 * Why and how did you choose ns-3 as the appropriate tool for your application?
 * What surprises did you find, in correctness/behavior? in implementation? in learning curve?
 * What are the remaining barriers to addressing fully your R&D questions?
 * What general capabilities would have made your work easier/faster?

# Submission instructions

Authors should submit papers through
[EasyChair](https://easychair.org/conferences/?conf=wns32023)
in PDF format, complying with
[ACM “sigconf” Proceedings format](http://www.acm.org/publications/proceedings-template). Submitted papers must not have been submitted for review or
published (partially or completely) elsewhere.  All papers must be written
in English.

# Acceptance Criteria

Every paper will be peer reviewed by the technical program committee.
Papers will be accepted based on the relevance, novelty, and impact of the
contribution, as well as the quality of writing and presentation.

Authors presenting new ns-3 models, frameworks, integration setups, etc. are
encouraged to include all traditional parts of a scientific paper:
introduction, motivation, related work, assumptions, verification and
validation, conclusions and references. As a general rule, papers that
only document source code will be rejected.

Authors presenting networking research using ns-3 are encouraged to follow
best simulation practices and focus particularly on the credibility and
[reproducibility](http://www.nsnam.org/wiki/Reproducible_papers) of
simulation results.  We strongly encourage authors of all papers,
demonstrations, and posters to include links to relevant source code and
instructions on how to use it. This will make contributions more useful
for the ns-3 community. If code has been submitted through the ns-3
merge request process, or published as a module in the ns-3 app store,
please list the link(s), this will be considered a plus.  Published,
high-quality artifacts will be a consideration in the Best Paper
evaluation, and in the awarding of ACM artifacts badges (see below).

Please do not hesitate to contact the workshop chairs if you are uncertain
whether your submission falls within the scope of the workshop.

# Artifacts Evaluation

Accepted papers will be reviewed by an Artifacts Evaluation Committee
for consideration of the awarding of
**[ACM artifacts badges](https://www.acm.org/publications/policies/artifact-review-and-badging-current)**,
according to the criteria listed by ACM.

For an example of a paper from WNS3 2022 that produced high-quality artifacts
enabling the easy reproduction of all paper figures, please see the
paper by **Nicolas Rybowski and Olivier Bonaventure, Evaluating OSPF convergence
with NS3-DCE**.
[paper](https://dl.acm.org/doi/10.1145/3532577.3532597);
[video](https://vimeo.com/732258850);
[slides](https://www.nsnam.org/workshops/wns3-2022/16-rybowski-slides.pdf)

Another example of a paper from WNS3 2022 that provides full code including
scripts and documentation to reproduce the plots is the paper by **Hao Yin, Sumit Roy and Sian Jin, IEEE WLANs in 5 vs 6 GHz: A Comparative Study**.
[paper](https://dl.acm.org/doi/10.1145/3532577.3532580);
[video](https://vimeo.com/732254410);
[slides](https://www.nsnam.org/workshops/wns3-2022/04-yin-slides.pdf);
[code](https://github.com/Mauriyin/ns3/tree/unequal)

# Copyright Policy

Accepted papers will be published in the ACM Digital Library, which will
require copyright transfer to ACM under their [normal terms and conditions](https://www.acm.org/publications/policies/publication-rights-and-licensing-policy/). In the spirit
of open source, we encourage authors of published papers to exercise their
right to publish author-prepared versions on their respective home page,
on a publicly accessible server of their employer, or on a pre-print server.
Upon request, we will provide links
from the WNS3 program on the ns-3 web site to the author-prepared version.

# Plagiarism Policy

We follow the [ACM standards and conduct](http://www.acm.org/publications/policies/plagiarism_policy) regarding plagiarism.

# Reviewing Process Conflict of Interest Policy

Reviewers will follow the [IEEE/ACM Transactions on Networking
Conflict-of-Interest Guidelines](https://newslab.ece.ohio-state.edu/ton/people.html#coi).
Authors are requested to identify potential conflicts-of-interest among the
workshop’s technical program committee.

# Demonstrations and Short Talks

In addition to the published paper track, we will organize an exhibition-style
demonstration and short talks session.
The aim is to foster interactive discussions on work-in-progress,
new problem statements, ideas for future development related to ns-3, and
display innovative prototypes.

A separate Call for Short Talks and Demos will be posted at a later date.
Accepted short talk and demo abstracts will be published on the ns-3 web site.
At least one author of each accepted demo/short talk must register and
present the work at the workshop.

# Awards

One Best Paper will be selected from the regular paper track by the TPC
chairs and will be announced at the workshop.

# Registration

Registration information will be posted at a later date.
At least one author of each accepted paper must
register and present the work at the workshop.

# Technical Program Co-Chairs

* Pasquale Imputato, University of Naples Federico II
* Yuchen Liu, North Carolina State University

# Proceedings Chair

* Eric Gamess, Jacksonville State University

# General Chair

* Tom Henderson, University of Washington

# Important Dates

Paper submission deadline: <s>Sunday, February 5, 2023, 17:00 EST (22:00 UTC)</s><br><b>extended to:</b> Sunday, February 19, 2023, 17:00 EST (22:00 UTC).

Notification of acceptance: Thursday April 6

