---
layout: page
title: International Conference on ns-3
permalink: /research/icns3/
---

The **International Conference on ns-3 (ICNS3)** is an annual academic conference focusing on advances
to ns-3 and the design and performance of ns-3.  This event was formerly known as the Workshop on ns-3.  Proceedings are published in the
[ACM digital library](https://dl.acm.org).
