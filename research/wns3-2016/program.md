---
layout: page
title: Program
permalink: /research/wns3/wns3-2016/program/
---
# 15th June (Wednesday), 2016

WNS3 Opening

**8:30-9:30 &nbsp; &nbsp;Traffic control&nbsp;(session chair: Hajime Tazaki)**

- Design and implementation of the traffic control module in ns-3 (Pasquale Imputato and Stefano Avallone&nbsp;([presentation slides](/workshops/wns3-2016/wns3-traffic-control.pdf)))

- Implementation and Evaluation of Proportional Integral controller Enhanced (PIE) Algorithm in ns-3 (Mohit Tahiliani, Shravya K. S and Smriti Murali&nbsp;([presentation slides](/workshops/wns3-2016/Implementation-and-Evaluation-of-PIE-algorithm-in-ns-3.pdf)))

**9:30-10:30 &nbsp; &nbsp; Congestion control&nbsp;(session chair: Hajime Tazaki)**

- An Implementation of Scalable, Vegas, Veno, and YeAH Congestion Control Algorithms in ns-3 (Truc Anh Nguyen, Siddharth Gangadhar, Md Moshfequr Rahman and James Sterbenz)


- TCP Evaluation Suite for ns-3 (Mohit Tahiliani, Dharmendra Kumar Mishra and Pranav Vankar&nbsp;([presentation slides](/workshops/wns3-2016/TCP-Evaluation-Suite-for-ns-3.pdf)))


**10:30-11:00 &nbsp; &nbsp; Break**

**11:00-12:00 &nbsp; &nbsp;Frameworks for integrating ns-3 with SDN&nbsp;(session chair: Brian Swenson)**

- OFSwitch13: Enhancing ns-3 with OpenFlow 1.3 support (Luciano Jerez Chaves, Islene Calciolari Garcia and Edmundo Roberto Mauro Madeira&nbsp;([presentation slides](http://www.lrc.ic.unicamp.br/~luciano/publications/wns316-pres.pdf))&nbsp;([author-prepared paper](http://www.lrc.ic.unicamp.br/~luciano/publications/wns316.pdf))&nbsp;([code](http://www.lrc.ic.unicamp.br/ofswitch13/index.html)))

- Analysis of Programming Language Overhead in DCE (Jared Ivey and George Riley)

**12:00-13:00 &nbsp; &nbsp; Lunch**

**13:00-14:30 &nbsp; &nbsp; Wi-Fi&nbsp;(session chair: Brian Swenson)**

- Implementation and validation of an IEEE 802.11ah module for NS-3 (Le Tian, S&eacute;bastien Deronne, Steven Latre and Jeroen Famaey&nbsp;([presentation slides](/workshops/wns3-2016/Implementation_and_validation_of_an_IEEE_80211ah_module_for_NS-3.pdf))&nbsp;([author-prepared paper](https://www.researchgate.net/publication/301328811_Implementation_and_validation_of_an_IEEE_80211ah_module_for_NS-3)))


- Implementation and Evaluation of a WLAN IEEE 802.11ad Model in NS-3 (Hany Assasa and Joerg Widmer&nbsp;([presentation slides](/workshops/wns3-2016/assasa-wns3-2016-presentation.pdf))&nbsp;([author-prepared paper](http://eprints.networks.imdea.org/id/eprint/1272))&nbsp;([code](https://github.com/hanyassasa87/ns3-802.11ad))&nbsp;([wiki page for code](https://github.com/hanyassasa87/ns3-802.11ad/wiki)))

- Investigation and Improvements to the Physical Layer Abstraction for Wi-Fi in ns-3 (Hossein-Ali Safavi-Naeini, Farah Nadeem and Sumit Roy&nbsp;([presentation slides](/workshops/wns3-2016/nadeem-wns3-presentation.pdf)))

**14:30 &#8211; 15:00 &nbsp; &nbsp; coffee break**

**15:00-16:30 &nbsp; &nbsp; Wireless&nbsp;(session chair: Lorenza Giupponi)**

- LL SimpleWireless: A Controlled MAC/PHY Wireless Model to Enable Network Protocol Research (Patricia Deutsch, Leonid Veyster and Bow-Nan Cheng &nbsp;([presentation slides](/workshops/wns3-2016/LL-SimpleWireless.pdf)))

- ns-3 based framework for simulating Communication Based Train Control (CBTC) systems (Abdulhalim Dandoush, Alina Tuholukova, Sara Alouf, Giovanni Neglia, Sebastien Simoens, Pascal Derouet and Pierre Dersin **_Note: Moved from Thursday morning session_**)

- A Framework for End-to-End Evaluation of 5G mmWave Cellular Networks in ns-3 (Russell Ford, Menglei Zhang, Sourjya Dutta, Marco Mezzavilla, Sundeep Rangan and Michele Zorzi)


**16:30-18:00 &nbsp; &nbsp; poster/demo session**

# 16th June (Thursday), 2016

**8:30-10:00 &nbsp; &nbsp; Models and Tools (session chair:&nbsp;Peter D. Barnes)**


- NS-3 Web-Based User Interface &ndash; Power Grid Communications Planning and Modeling Tool (Kurt Derr&nbsp;([presentation slides](/workshops/wns3-2016/derr-ns3workshop-final.pdf)))

- Getting Kodo: Network Coding for the ns-3 simulator (Nestor Hernandez, Morten Pedersen, Peter Vingelmann, Janus Heide, Daniel Lucani and Frank Fitzek)

- Improving ns-3 Emulation Performance for Fast Prototyping of Network Protocols (Helder Fontes, Tiago Cardoso and Manuel Ricardo)


**10:00-10:30 &nbsp; &nbsp; coffee break**

**10:30-12:00 &nbsp; &nbsp; Mobile Systems (session chair:&nbsp;George F Riley)**


- A Realistic MAC and Energy Model for 802.15.4 (Vishwesh Rege and Tommaso Pecorella **_Note: Moved from Wednesday session_**)

- Implementation of 3D Obstacle Compliant Mobility Models for UAV networks in ns-3 (Paulo Regis, Suman Bhunia and Shamik Sengupta)

- Topology Simulation for Aeronautical Communication Protocols with ns-3 and DCE (Andreas Lehmann, Matthias Kreuzer, J&ouml;rg Deutschmann, Ulrich Berold and Johannes Huber)
