---
title: Accepted Abstracts
layout: page
permalink: /research/wngw/wngw-2019/accepted-abstracts/
---
Several participants submitted Extended Abstracts for inclusion in the proceedings to be published in the <a href="https://dl.acm.org/" target="_blank">ACM Digital Library</a>.   The following lists the abstracts accepted by the technical program committee.

  * **Weihua Jiang, Sumit Roy and Colin McGuire,** Efficient Link-to-System Mappings for MU-MIMO Channel D Scenarios in 802.11ac WLANs

  * **Qiang Hu, Yuchen Liu, Yan Yan and Douglas Blough,** End-to-end Simulation of MmWave Out-of-band BackhaulNetworks in ns-3

  * **Ashish Kumar Sultania, Carmen Delgado and Jeroen Famaey,** Implementation of NB-IoT Power Saving Schemes in NS-3

  * **Peter Barnes, Steven Smith, David Jefferson, Matthew Bielejeski, David Wright, Lorenza Giupponi, Katerina Koutlia and Colby Harper,** S3: the Spectrum Sharing Simulator

  * **Hany Assasa, Joerg Widmer, Tanguy Ropitault, Anuraag Bodi and Nada Golmie,** High Fidelity Simulation of IEEE 802.11ad in ns-3 Using a Quasi-deterministic Channel Model

  * **Hany Assasa, Joerg Widmer, Jian Wang, Tanguy Ropitault and Nada Golmie,** An Implementation Proposal for IEEE 802.11ay SU/MU-MIMO Communication in ns-3

  * **Serena Santi, Le Tian and Jeroen Famaey,** Evaluation of the Co-Existence of RAW and TWT Stations in IEEE 802.11ah using NS-3

  * **Francesca Nizzi, Tommaso Pecorella, Mattia Bastianini, Carlo Cerboni, Alessandra Buzzigoli and Andrea Fratini,** The role of network simulator in the 5G experimentation

  * **Francesca Nizzi and Tommaso Pecorella,** Protocol prototype implementation using ns-3 -- a use-case

  * **Tommaso Zugno, Michele Polese, Mattia Lecci and Michele Zorzi,** Simulation of Next-Generation Cellular Networks with ns-3: Open Challenges and New Directions"


