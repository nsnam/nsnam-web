---
layout: page
title: Program Committee
permalink: /research/wngw/wngw-2019/program-committee/
---

# Organizers

Stefano Avallone (UNINA), Sebastien Deronne (Televic), Lorenza Giupponi (CTTC), Tom Henderson (UW), Tommaso Pecorella (UNIFI), Sumit Roy (UW), Michele Zorzi (UNIPD)

# General Chair

* Tommaso Pecorella, DINFO - UniFi, <tommaso.pecorella@unifi.it>

# Technical Program Co-Chairs

* Stefano Avallone, UNINA, <stavallo@unina.it>
* Tom Henderson, Univ. of Washington, <tomhend@u.washington.edu>

# Proceedings Chair

* Eric Gamess, Jacksonville State University, <egamess@gmail.com>

# Technical Program Committee

The following individuals reviewed abstracts for WNGW 2019.

Name                 | Institution                                        |
---------------------|-----------------------------------------------------
Zoraze Ali           | Centre Tecnològic Telecomunicacions Catalunya, Spain |
Gabriel Arrobo       | Intel |
Stefano Avallone     | University of Naples "Federico II" |
Biljana Bojovic      | Centre Tecnològic Telecomunicacions Catalunya, Spain |
Vinicius Da Silva Goncalves | Rice University |
Eric Gamess          | Jacksonville State University, USA |
Lorenza Giupponi     | Centre Tecnològic Telecomunicacions Catalunya, Spain |
Tom Henderson        | University of Washington |
Pasquale Imputato    | DIETI - Università di Napoli Federico II |
Alexander Krotov     | IITP RAS |
Tommaso Pecorella    | Universita di Firenze |
Michele Polese       | University of Padova |
Getachew Redietab    | Orange |
Manuel Ricardo       | INESC TEC, Portugal      |
Sumit Roy            | University of Washington |
Mohit Tahiliani      | National Institute of Technology Karnataka, India |
Michele Zorzi        | University of Padova |
