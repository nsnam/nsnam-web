---
layout: page
title: Program
permalink: /research/wns3/wns3-2013/program/
---

| Time | Chair | Session |
| ---- | ----- | ------- |
| 9:00 -- 10:00 | | Introduction |
| | | Olivier Dalle. Using Computer Simulations for Producing Scientific Results: Are We There Yet? |
| 10:00 -- 10:30 | | Coffee Break |
| 10:30 -- 12:30 | Chair: Vedran Miletić | Hajime Tazaki, Frederic Urbani and Thierry Turletti. DCE Cradle: Simulate Network Protocols with Real Stacks for Better Realism |
| | | Brian Swenson and George Riley. Simulating Large Topologies in ns-3 using BRITE and CUDA Driven Global Routing |
| | | Siddharth Gangadhar, Truc Anh Nguyen, Greeshma Umapathi and James Sterbenz. TCP Westwood Protocol Implementation in ns-3 |
| | | Peter Barnes, Betty Abelev, Eddy Banks, James Brase, David Jefferson, Sergei Nikolaev, Steven Smith and Ron Soltz. Integrating ns3 Model Construction, Description, Preprocessing, Execution, and Visualization |
| 12:00 -- 13:30 | | Lunch |
| 13:30 -- 15:30 | Chair: Luiz Felipe Perrone | Yufei Cheng, Egemen Çetinkaya and James Sterbenz. Transactional Traffic Generator Implementation in ns-3 |
| | | Sebastien Deronne, Veronique Moeyaert and Sebastien Bette. Simulation of 802.11 Radio-over-Fiber Networks using ns-3 |
| | | Xiuchao Wu, Ken Brown, Cormac Sreenan, Pedro Alvarez, Marco Ruffini, Nicola Marchetti, David Payne and Linda Doyle. An XG-PON Module for the NS-3 Network Simulator |
| 15:30 -- 17:00 | | Coffee Break and Poster/Demo Session |
| 16:30 -- 18:00 | Chair: Tommaso Pecorella | Giuseppe Piro, Luigi Alfredo Grieco, Gennaro Boggia and Pietro Camarda. Nano-Sim: simulating electromagnetic-based nanonetworks in the Network Simulator 3 |
| | | Dizhi Zhou, Nicola Baldo and Marco Miozzo. Implementation and Validation for LTE Downlink Schedulers |
| | | David Gomez Fernandez, Ramón Agüero Calvo, Marta García-Arranz and Luis Muñoz Gutiérrez. Replication of the Bursty Behavior of Indoor WLAN Channels |
