---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2013/program-committee/
---
### **Technical Program Committee Chairs**

  * Vedran Miletic, University of Rijeka <vmiletic@inf.uniri.hr>
  * Tommaso Pecorella, Università degli Studi di Firenze <tommaso.pecorella@unifi.it>

### **Technical Program Committee Members**

  * Dean Armstrong, Virscient
  * Nicola Baldo, CTTC
  * Peter D. Barnes, LLNL
  * Pavel Boyko, Telum
  * Sam Jansen, StarLeaf
  * Ruben Merz, Deutsche Telekom Laboratories
  * Giuseppe Piro, Politecnico di Bari
  * George Riley, Georgia Tech
  * Giovanni Stea, University of Pisa
  * Lalith Suresh, Deutsche Telekom Laboratories
  * Hajime Tazaki, NICT
