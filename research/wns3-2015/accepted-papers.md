---
layout: page
title: Accepted Papers
permalink: /research/wns3/wns3-2015/accepted-papers/
---
  * **Sérgio Conceição, Filipe Ribeiro, Rui Campos and Manuel Ricardo** <br />
  Novel ns-3 Model Enabling Simulation of Electromagnetic Wireless Underground Networks
  * <strong>Jared Ivey, Brian Swenson and George Riley</strong><br /> PHOLD Performance of Conservative Synchronization Methods for Distributed Simulation in ns-3
  * <strong>Scott Carpenter and Mihail Sichitiu</strong><br /> An Obstacle Model Implementation for Evaluating Radio Shadowing with ns-3
  * <strong> Alina Quereilhac, Damien Saucez, Thierry Turletti and Walid Dabbous</strong><br /> Automating ns-3 Experimentation in Multi-Host Scenarios
  * <strong> Ben Newton, Jay Aikat and Kevin Jeffay</strong><br /> Simulating Large-Scale Airborne Networks with ns-3
  * <strong> Tiago Cerqueira and Michele Albano</strong><br /> RoutesMobilityModel: easy realistic mobility simulation using external information services
  * <strong> Rohit Gupta, Bjoern Bachmann, Russell Ford, Sundeep Rangan, Nikhil Kundargi, Amal Ekbal, Karamvir Rathi, Maria Isabel Sanchez Bueno, Antonio De La Oliva and Arianna Morelli</strong><br /> NS-3-based Real-time emulation of LTE Testbed using LabVIEW platform for Software Defined Networking (SDN) in CROWD
  * <strong> Maurizio Casoni, Carlo Augusto Grazia, Martin Klapez and Natale Patriciello</strong><br /> Implementation and Validation of TCP Options and Congestion Control Algorithms for ns-3
  * <strong> Rubén Martínez Vidal, Thomas R. Henderson and Joan Borrell</strong><br /> Implementation and Evaluation of Licklider Transmission Protocol (LTP) in ns-3
  * <strong> Lampros Katsikas, Konstantinos Chatzikokolakis and Nancy Alonistioti</strong><br /> Implementing Clustering for Vehicular Ad-hoc Networks in ns-3
  * <strong>Dmitry Chalyy</strong><br /> Implementation of Stateless Transport Protocols in ns-3
  * <strong> Christopher Hood and George Riley</strong><br /> On Predicting the Performance Characteristics of the ns-3 Distributed Simulator for Scale-free Internet Models
  * <strong> Mohammed Alenazi, Yufei Cheng, Dongsheng Zhang and James Sterbenz</strong><br /> Epidemic Routing Protocol Implementation in ns-3
  * <strong>Piotr Gawłowicz, Nicola Baldo and Marco Miozzo</strong><br /> An extension of the ns-3 LTE module to simulate Fractional Frequency Reuse Algorithms
  * <strong>Sergei Nikolaev, Eddy Banks, Peter Barnes, David Jefferson and Steve Smith</strong><br /> Pushing the Envelope in Distributed ns-3 Simulations: Toward Planetary-Scale Simulations
  * <strong> Steven Smith, David Jefferson, Peter Barnes and Sergei Nikolaev</strong><br /> Improving Per Processor Memory Use of ns-3 to Enable Large Scale Simulations
  * **Sérgio Sakai, Gilberto Gambugge, Ricardo Takaki, Jorge Seki, Juliano Bazzo and João Paulo Miranda** <br />
  Performance Comparison of a Custom Emulation-based Test Environment Against a Real-world LTE Testbed
