---
title: Accepted Posters, Demos, Short talks
layout: page
permalink: /research/wns3/wns3-2015/accepted-posters-demos-short-talks/
---
  * **Andreas Lehmann, Jörg Deutschmann, Johannes Hampel and Johannes Huber.** Porting Power Line Protocols from Hardware into ns-3 DCE [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_32.pdf)]
  * **Christopher Hepner, Arthur Witt and Roland Muenzner.** Validation of the ns-3 802.11s model and proposed changes compliant to IEEE 802.11-2012 [[paper](https://www.nsnam.org/wp-content/uploads/2015/05/WNS3_2015_submission_33.pdf)]
  * **Christopher Hepner, Arthur Witt and Roland Muenzner.** A new ns-3 WLAN error rate model – Definition, validation of the ns-3 implementation and comparison to physical layer measurements with AWGN channel [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_34.pdf)]
  * **João Loureiro, Michele Albano, Tiago Cerqueira, Raghuraman Rangarajan and Eduardo Tovar.** A module for the XDense architecture in ns-3 [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_35.pdf)]
  * **Fábio Oliveira, Ricardo Garibay-Martínez, Tiago Cerqueira, Michele Albano and Luis Lino Ferreira.** A module for the FTT-SE protocol in ns-3 [<a href="https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_37.pdf">paper</a>]</span>
  * **Nicola Baldo, Nikolaos Bartzoudis, Josep Mangues-Bafalluy, Miquel Payaró, Marco Miozzo and Paolo Dini.** An integrated testbed for full-stack 5G experimentation [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_38.pdf)]
  * **Jared Ivey, Michael Riley and George Riley.** A Software-Defined Spanning Tree Application for ns-3 [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_39.pdf)]
  * **Michele Albano, Tiago Cerqueira and Stefano Chessa.** A module for Data Centric Storage in ns-3 [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_40.pdf)]
  * **Sourjya Dutta, Menglei Zhang and Marco Mezzavilla.** Millimeter wave module for ns-3 network simulator [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_41.pdf)]
  * **Truc Anh Nguyen and James Sterbenz.** An Implementation of the SACK-Based Conservative Loss Recovery Algorithm for TCP in ns-3 [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_42.pdf)]
  * **Amir Modarresi and James Sterbenz.** Simple Forwarding over Trajectory (SiFT) Implementation in ns-3 [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_43.pdf)]
  * **Maurizio Casoni, Carlo Augusto Grazia, Martin Klapez and Natale Patriciello.** A Validation Tool for PPDR Architectures based on ns-3 [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_44.pdf)]
  * **Sérgio Sakai, Gilberto Gambugge, Ricardo Takaki, Jorge Seki, Juliano Bazzo, and João Paulo Miranda.** Demonstration of a Custom LTE Emulation-based Test Environment [[paper](https://www.nsnam.org/wp-content/uploads/2015/04/WNS3_2015_submission_45.pdf)]
