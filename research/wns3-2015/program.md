---
title: Program
layout: page
permalink: /research/wns3/wns3-2015/program/
---
Bibtex (ACM Digital Library) citations of the below papers can be found [on the ns-3 publication page](https://www.nsnam.org/overview/publications/#2015-2).

# 13th May (Wed), 2015

**9:00  &#8211; 9:30** WNS3 2015 opening

- Automating ns-3 Experimentation in Multi-Host Scenarios (Alina Quereilhac, Damien Saucez, Thierry Turletti and Walid Dabbous.) [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/20150513_wns3_nepi_ns3.pdf">slides</a>] [<a href="https://hal-amu.archives-ouvertes.fr/INRIA/hal-01141000v1">paper</a>] (<span style="color: #000080;"><strong>Awarded best paper</strong></span>)

**9:30 &#8211; 10:30**  Wireless

- Novel ns-3 Model Enabling Simulation of Electromagnetic Wireless Underground Networks (Sérgio Conceição, Filipe Ribeiro, Rui Campos and Manuel Ricardo [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/presentation-underground-networks.pdf">slides</a>])

- An Obstacle Model Implementation for Evaluating Radio Shadowing with ns-3 (Scott Carpenter and Mihail Sichitiu. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/WNS3_2015_Obstacle_Model.pdf">slides</a>] [<a href="http://www4.ncsu.edu/~mlsichit/Research/Publications/ns3ObstacleModelScott.pdf">paper</a>])

**10:30 &#8211; 11:00** Routing

- Implementing Clustering for Vehicular Ad-hoc Networks in ns-3 (Lampros Katsikas, Konstantinos Chatzikokolakis and Nancy Alonistioti. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/Implementing-Clustering-for-Vehicular-Ad-hoc-Networks-in-ns-3_Final.pdf">slides</a>])


**11:00 &#8211; 11:30** Coffee break

**11:30 &#8211; 12:30** Mobility model

- Simulating Large-Scale Airborne Networks with ns-3 (Ben Newton, Jay Aikat and Kevin Jeffay. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/SiimulatingLargeScaleAirborneNetworksWithNs3.pdf">slides</a>])

- RoutesMobilityModel: easy realistic mobility simulation using external information services (Tiago Cerqueira and Michele Albano. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/routesmobilitymodel_wns3_V1.pdf">slides</a>] [<a href="http://www.cister.isep.ipp.pt/docs/routesmobilitymodel__easy_realistic_mobility_simulation_using_external_information_services/1060/attach.pdf">paper</a>])

**12:30 &#8211; 14:00** Lunch break

**14:00 &#8211; 16:00** Distributed simulation

- PHOLD Performance of Conservative Synchronization Methods for Distributed Simulation in ns-3 (Jared Ivey, Brian Swenson and George Riley. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/Ivey_WNS3_PHOLD.pdf">slides</a>])

- On Predicting the Performance Characteristics of the ns-3 Distributed Simulator for Scale-free Internet Models (Christopher Hood and George Riley. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/wns3-chris-hood.pdf">slides</a>])

- Improving Per Processor Memory Use of ns-3 to Enable Large Scale Simulations (Steven Smith, David Jefferson, Peter Barnes and Sergei Nikolaev. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/WNS3-Barnes-2015-05-v1-LLNL-CONF-667822.pdf">slides</a>])

- Pushing the Envelope in Distributed ns-3 Simulations: One Billion Nodes (Sergei Nikolaev, Eddy Banks, Peter Barnes, David Jefferson and Steve Smith. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/WNS3_2015-clean.pdf">slides</a>])

**16:00 &#8211; 16:30** Coffee break

**16:30 &#8211; 17:00** Poster introductions (lightning talk)

**17:00 &#8211; 18:00** Poster session

- [poster/demonstration list](http://www.nsnam.org/research/wns3/wns3-2015/accepted-posters-demos-short-talks/ "Accepted Posters, Demos, Short talks")

# 14th May (Thr), 2015

**9:00 &#8211; 10:00** Delay Tolerant Network

- Implementation and Evaluation of Licklider Transmission Protocol (LTP) in ns-3 (Rubén Martínez Vidal, Thomas R. Henderson and Joan Borrell. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/ltp-wns3-2015.pdf">slides</a>])

- Epidemic Routing Protocol Implementation in ns-3 (Mohammed Alenazi, Yufei Cheng, Dongsheng Zhang and James Sterbenz.)

**10:00 &#8211; 11:30** LTE

- NS-3-based Real-time emulation of LTE Testbed using LabVIEW platform for Software Defined Networking (SDN) in CROWD Project (Rohit Gupta, Bjoern Bachmann, Russell Ford, Sundeep Rangan, Nikhil Kundargi, Amal Ekbal, Karamvir Rathi, Maria Isabel Sanchez Bueno, Antonio De La Oliva and Arianna Morelli. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/wns3_crowd_testbed_talk.pdf">slides</a>])

- An extension of the ns-3 LTE module to simulate Fractional Frequency Reuse Algorithms (Piotr Gawłowicz, Nicola Baldo and Marco Miozzo. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/ffr_presentation.pdf">slides</a>])

- Performance Comparison of a Custom Emulation-based Test Environment Against a Real-world LTE Testbed (Sérgio Sakai, Gilberto Gambugge, Ricardo Takaki, Jorge Seki, Juliano Bazzo and João Paulo Miranda [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/wns3-2015.pdf">slides</a>])

**11:30 &#8211; 12:00** Coffee break

**12:00 &#8211; 13:00** Transport protocol

- Implementation and Validation of TCP Options and Congestion Control Algorithms for ns-3 (Maurizio Casoni, Carlo Augusto Grazia, Martin Klapez and Natale Patriciello.<br />[<a href="http://netlab.ing.unimo.it/pdf/2015-tcp-wns3.pdf" target="_blank">paper</a>][<a href="https://www.nsnam.org/docs/contributed/wns3-2015-tcp-paper-presentation.pdf" target="_blank">slides</a>])

- Implementation of Stateless Transport Protocols in ns-3 (Dmitry Chalyy. [<a href="https://www.nsnam.org/wp-content/uploads/2015/03/ns3-2015-chalyy.pdf">slides</a>])
