---
layout: page
title: WNS3 2022 Tutorials
permalink: /research/wns3/wns3-2022/tutorials/
---

Links to tutorial videos are provided below.

# NR, NR-U and NR-V2X for ns-3: Models and usage

* **Date/time:** Tuesday, June 21, 1330 UTC (9:30am Eastern Daylight Time)
* **Duration:** 3 hours
* **Presenters:** Zoraze Ali (Ericsson, Sweden) and Biljana Bojovic (CTTC, Spain)
* **Abstract:** In 2017 CTTC started working on the NR module for ns-3, otherwise known as 5G-LENA, in the context of a collaboration with Interdigital. In 2019, CTTC released its first release NR-v0.1, and in June of 2022 released NR-v2.2, compatible with ns-3.36.1. Since its beginning, the NR module has gained more than 1000 GitLab users, among which are many companies. Over five years of 5G-LENA development, the NR module has added many new features, and extensions, such as NR-U and NR-V2X-- both pioneering open-source simulator implementations for corresponding technologies. The goal of this tutorial is to allow a broader ns-3 community to learn the NR module, and all the features that have been developed over time, including the NR-U and NR-V2X extensions, and to learn how to use the NR module and its extensions.
* **Topics:**
    * The NR module introduction (45 minutes, Biljana Bojovic): The theoretical introduction to the NR module, covering the NR module architecture and design, the NR protocol stack, and the main features in the NR module.
    * The NR demo example (30 minutes, Biljana Bojovic): A hands-on session using a typical NR module example, with a goal to explain how to build the desired topology, configure the NR parameters, go through the NR module traces and logs.
    * The NR-U module intro and example (30 minutes, Biljana Bojovic): Explain the NR-U implementation and go through a simple example of NR-U and Wi-Fi coexistence.
    * The NR-V2X design and implementation (45 minutes, Zoraze Ali):  The introduction to NR-V2X focuses on the high-level design and implementation details of the NR V2X feature developed on top of CTTC 5G-LENA and ns-3 LTE modules.
    * The NR-V2X example (30 minutes, Zoraze Ali):  A hands-on session using an example included in the NR-V2X module.
* **Materials:**
    * [Video archive](https://vimeo.com/showcase/9642239)
    * [NR slides (Biljana Bojovic)](https://www.nsnam.org/tutorials/consortium22/WNS3-2022-NR-Tutorial-Biljana-Bojovic.pdf)
    * [NR-V2X slides (Zoraze Ali)](https://www.nsnam.org/tutorials/consortium22/WNS3-2022-NR-V2X-Tutorial-Zoraze-Ali.pdf)

# Visualizing a Scenario Using the NetSimulyzer Visualizer
* **Date/time:** Tuesday, June 21, 1700 UTC (1pm Eastern Daylight Time)
* **Presenter:** Evan Black (National Institute of Standards and Technology)
* **Abstract:** This tutorial covers the NetSimulyzer, an open-source, 3D ns-3 visualizer. It begins with a brief presentation introducing the tool, and some new features with copy-able code snippets and screenshots. Then we walk through creating a ns-3 scenario, integrating the tool along the way. We will conclude with some best practices and Q&A.
* **Materials:**
    * [Video archive](https://vimeo.com/showcase/9642239)
    * [link to NetSimulyzer tutorial from last year](https://vimeo.com/570087427)
