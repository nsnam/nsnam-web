---
layout: page
title: Call for Lightning Talks
permalink: /research/wns3/wns3-2022/call-for-lightning-talks/
---

The **Workshop on ns-3 (WNS3)** will be held as a fully virtual event, starting
on June 22, 2022 and tentatively planned for two days.
Additional events will be planned during the same week, including
advanced tutorials on topics related to ns-3.

Do you have some work in progress that you would like to share with the ns-3 community?  The Technical Program Committee co-chairs are soliciting [lightning talk](https://en.wikipedia.org/wiki/Lightning_talk) proposals for a session at the virtual Workshop on ns-3 (exact date and time TBD).

TPC chairs will select roughly 10 such talks based on timeliness and relevance.  Submissions are open until mid-May and speakers will be notified by the end of May. We encourage the submission of talks that will spark some discussion with the broader community during the meeting.

# Proposal format

Each lightning talk proposal should be written in English and must indicate the following:
 * Title
 * Names and affiliation of the presenter
 * Abstract (< 200 words)

# Submission instructions

Speakers should submit lightning talk proposals via email (subject "[WNS3] Lightning talk"),
copying the WNS3 PC chairs (m.polese@northeastern.edu and yuchen.liu@gatech.edu)

# Acceptance Criteria

It is anticipated that there will be space for roughly ten such talks.  The TPC
co-chairs will decide on the accepted talks based on their assessment of
relevance and interest to the community.

# Important Dates

Lightning talk proposal submission deadline: May 13th, 17:00 PST.

Notification of acceptance: May 31st.
