---
title: Accepted Papers
layout: page
permalink: /research/wns3/wns3-2022/accepted-papers/
---

  * **Pascal Jörke, Tim Gebauer and Christian Wietfeld,** From LENA to LENA-NB: Implementation and Performance Evaluation of NB-IoT and Early Data Transmission in ns-3

  * **Biljana Bojovic, Zoraze Ali, Sandra Lagen and Katerina Koutlia,** ns-3 and 5G-LENA extensions to support dual-polarized MIMO

  * **Jianfei Shao, Minh Vu, Mingrui Zhang, Asmita Jayswal and Lisong Xu,** Symbolic NS-3 for Efficient Exhaustive Testing: Design, Implementation, and Simulations

  * **Emily Ekaireb, Xiaofan Yu, Kazim Ergun, Quanling Zhao, Kai Lee, Muhammad Huzaifa and Tajana Rosing,** ns3-fl: Simulating Federated Learning with ns-3

  * **Parth Pratim Chatterjee and Thomas R. Henderson,** Revitalizing ns-3’s Direct Code Execution

  * **Benjamin Newton, Michael Scoggin, Anand Ganti, Uzoma Onunkwo and Vincent Hietala,** Clipping for Faster Wireless Network Simulation in ns-3

  * **Hao Yin, Sumit Roy and Sian Jin,** IEEE WLANs in 5 vs 6 GHz: A Comparative Study

  * **Andrea Ramos, Yanet Estrada, Miguel Cantero, Jaime Romero, David Martín-Sacristán, Saúl Inca, Manuel Fuentes and José F. Monserrat,** Implementation and Calibration of the 3GPP Industrial Channel Model for ns-3

  * **Nicolas Rybowski and Olivier Bonaventure,** Evaluating OSPF convergence with NS3-DCE

  * **Eduardo Nuno Almeida, Mohammed Rushad, Sumanth Reddy Kota, Akshat Nambiar, Hardik L. Harti, Chinmay Gupta, Danish Waseem, Gonçalo Santos, Helder Fontes, Rui Campos and Mohit P. Tahiliani.** Machine Learning based Propagation Loss Module for enabling Digital Twins of Wireless Networks in ns-3

  * **Argha Sen, Sashank Bonda, Jay Jayatheerthan and Sandip Chakraborty,** Implementation of mmWave-energy Module and Power Saving Schemes in ns-3

  * **Muhammad Naeem, Michele Albano, Davide Magrin, Brian Nielsen and Kim Guldstrand Larsen,** A Sigfox Module for the Network Simulator 3

  * **Biljana Bojovic and Sandra Lagen,** Enabling NGMN mixed traffic models for ns-3

  * **Shyam Krishnan Venkateswaran, Ching-Lun Tai, Yoav Ben-Yehezkel, Yaron Alpert and Raghupathy Sivakumar,** IEEE 802.11 PSM in ns-3: A Novel Implementation and Evaluation under Channel Congestion

  * **Juan Leon, Yacoub Hanna and Kemal Akkaya,** Integration of WAVE and OpenFlow for Realization of SDN-based VANETs in NS-3

  * **Peng Zhang, Pedro Forero, Daniel Yap and Dusan Radosevic,** Evaluation of Reinforcement-Learning Queue Management Algorithm for Undersea Acoustic Networks Using NS3

  * **Matteo Drago, Tommaso Zugno, Federico Mason, Marco Giordani, Mate Boban and Michele Zorzi,** Artificial Intelligence in Vehicular Wireless Networks: A Case Study Using ns-3

