---
layout: page
title: WNS3 2019 Program
permalink: /research/wns3/wns3-2019/program/
---
# Wednesday, June 19, 2019
*Department of Information Engineering, University of Florence, Italy*

**09:00 to 10:30 am:  Transport**

- Alvise De Biasio, Federico Chiariotti, Michele Polese, Andrea Zanella and Michele Zorzi, A QUIC Implementation for ns-3 **(Awarded Best Paper)**
- Shikha Bakshi and Mohit P. Tahiliani, Recent Acknowledgement support for ns-3 TCP
- Harsh Patel, Hrishikesh Hiraskar and Mohit P. Tahiliani, Extending Network Emulation Support in ns-3 using DPDK

**10:30 to 11:00 am:**  Break

**11:00 am to 12:30 pm: Potpourri**

- Akshay Raman, Kimberly Chou and Spyridon Mastorakis, A Simulation Framework for Peer-to-Peer File Sharing in Named Data Networking
- Michele Polese, Tommaso Zugno and Michele Zorzi, Implementation of Reference Public Safety Scenarios in ns-3
- Helder Fontes, Vitor Lamela, Rui Campos and Manuel Ricardo, ns-3 NEXT: Towards a Reference Platform for Offline and Augmented Wireless Networking Experimentation

**12:30 to 2:00 pm:**  Lunch break

**2:00 to 4:00 pm:  Wireless**

- M. Shahwaiz Afaqui, Cristina Cano, Vincent Kotzsch, Clemens Felber and Walter Nitzold,Implementation of the 3GPP LTE-WLAN Inter-working Protocols in NS-3
-  Hany Assasa, Joerg Widmer, Tanguy Ropitault and Nada Golmie, Enhancing the ns-3 IEEE 802.11ad Model Fidelity: Beam Codebooks, Multi-Antenna Beamforming Training, and Quasi-Deterministic mmWave Channel
- Natale Patriciello, Sandra Lagen, Lorenza Giupponi and Biljana Bojovic, An Improved MAC Layer for the 5G NR ns-3 module
- Leonardo Jr Lanante, Sumit Roy, Scott Carpenter and Sébastien Deronne, Improved Abstraction for Clear Channel Assessment in ns-3 802.11 WLAN Model

**4:30 to 6:00 pm: Demo and poster session**

- Sampath Edirisinghe, Chathurika Ranaweera, Elaine Wong, Christina Lim and Ampalavanapillai, ns-3 Module for Optical Wireless Communication
- Lucas Coelho de Almeida, Andre Araujo de Medeiros, Prof. Claudia Jacy Barenco Abbas, Honeypot Application with ns-3 Emulation
- Davide Magrin, Dizhi Zhou, Michele Zorzi, A Simulation Execution Manager for ns-3: Encouraging reproducibility and simplifying statistical analysis of ns-3 simulations,
- Clemens Felber, Prototyping real world communication scenarios using ns-3 LTE and WIFI modules with NI’s real-time SDR platform
- Kevin Fall, Emulation with astrolink integration
