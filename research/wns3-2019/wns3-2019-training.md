---
layout: page
title: WNS3 2019 Training
permalink: /research/wns3/wns3-2019/training/
---

ns-3 training sessions will be held on Monday June 17 and Tuesday June 18, prior to the Workshop on ns-3. Several ns-3 maintainers will conduct the training.

Monday June 17 will be an introduction to ns-3, for users just starting with ns-3.  We will provide a comprehensive overview of the most commonly used parts of ns-3.

Tuesday June 12 will contain four more advanced sessions, including sessions on Wi-Fi, LTE, WPAN, and TCP.

Please view the **[training flyer](https://www.nsnam.org/docs/consortium/training/ns-3-training-2019.pdf)** for more information, as well as the **[wiki page](https://www.nsnam.org/wiki/AnnualTraining2019)** for the latest updates.  **[Registration](/research/wns3/wns3-2019/registration/)** is required to attend.
