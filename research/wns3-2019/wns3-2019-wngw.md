---
layout: page
title: Workshop on Next-Generation Wireless
permalink: /research/wns3/wns3-2019/wngw/
---

The **Workshop on Next-Generation Wireless with ns-3** was held on
Friday, June 21 2019, following WNS3 2019, and hosted by
the [University of Florence, Department of Information Engineering](https://www.dinfo.unifi.it/changelang-eng.html).

# Organizers

Stefano Avallone (UNINA), Sebastien Deronne (Televic), Lorenza Giupponi (CTTC), Tom Henderson (UW), Tommaso Pecorella (UNIFI), Sumit Roy (UW), Michele Zorzi (UNIPD)

# Local Arrangements

Information about the meeting location and VISA requirements can be found on
the [WNS3 local information page](/research/wns3/wns3-2019/local-information/).
Contact Tommaso Pecorella, DINFO - UniFi, (tommaso.pecorella at unifi.it) with
any other questions.

# Call for Participation

The [call for participation](https://www.nsnam.org/workshops/wns3-2019/Workshop-ns-3-wireless-2019-cfp.pdf) contains details about the workshop goals, organization of topics, and call for submissions.

# Submissions

Submissions (due by April 22, 2019, extended from original date of April 15) can be made at this [EasyChair site](https://easychair.org/conferences/?conf=wngw2019).  Please consult the call for participation for details on submissions.

# Registration

Registration information is found on [a separate page](/research/wns3/wns3-2019/registration/).
