---
layout: page
title: WNS3 2019 Local Information
permalink: /research/wns3/wns3-2019/local-information/
---

# General Chair

Contact Tommaso Pecorella, DINFO - UniFi (tommaso.pecorella at unifi.it) for any specific questions about local information.

# VISA requirements

The official web form to check if you need a VISA, and outlining the procedures to obtain one if so, is found at [this link](http://vistoperitalia.esteri.it/home/en).

If you need a VISA to attend, use the "Business" type.  You will need [this document](https://www.nsnam.org/workshops/wns3-2019/WNS3-invitation.docx) signed by T. Pecorella.  Please modify and send it to [tommaso.pecorella@unifi.it](mailto:tommaso.pecorella@unifi.it) and [francesca.nizzi@unifi.it](mailto:francesca.nizzi@unifi.it), along with a physical address and a phone number for the courier.

# Location of the meeting

The meeting will be held at:  Viale Giovanni Battista Morgagni, 40, 50134 Firenze FI, Italy.  The closest Tramvia stop is: T1 - "Morgagni - Università".

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2518.518531989298!2d11.245800600058056!3d43.802502929609105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a568417b78171%3A0x654bf8c1e3fb63f2!2sLearning+Center+Morgagni!5e0!3m2!1sen!2sus!4v1552596397152" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

* [Google map direct link](https://goo.gl/maps/Dj2JcZdYZ6s)

# Local hotel information

UniFi maintains [this fact sheet](https://www.unifi.it/upload/sub/relazioni_internazionali/accommodation_florence.pdf) regarding possible accommodations, although it does not appear to have been updated yet for 2019.

We have found some hotels that are close to the WNS3:

* [Hotel Corolle](https://goo.gl/maps/xNfMs4nQCLS2)
* [Albergo Raffaello](https://goo.gl/maps/sKMnq75YWJS2)
* [Hotel Careggi](https://goo.gl/maps/2TBSUSs6aHn)

However, any hotel nearby the [Tramvia](https://en.wikipedia.org/wiki/Trams_in_Florence) will do, as there is a stop right in front of the WNS3 location.

