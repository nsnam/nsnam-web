---
title: Accepted Papers
layout: page
permalink: /research/wns3/wns3-2019/accepted-papers/
---

  * **Shikha Bakshi and Mohit P. Tahiliani,** Recent Acknowledgement support for ns-3 TCP

  * **Alvise De Biasio, Federico Chiariotti, Michele Polese, Andrea Zanella and Michele Zorzi,** A QUIC Implementation for ns-3  **(Awarded Best Paper)**

  * **M. Shahwaiz Afaqui, Cristina Cano, Vincent Kotzsch, Clemens Felber and Walter Nitzold,** Implementation of the 3GPP LTE-WLAN Inter-working Protocols in NS-3

  * **Hany Assasa, Joerg Widmer, Tanguy Ropitault and Nada Golmie,** Enhancing the ns-3 IEEE 802.11ad Model Fidelity: Beam Codebooks, Multi-Antenna Beamforming Training, and Quasi-Deterministic mmWave Channel

  * **Leonardo Jr Lanante, Sumit Roy, Scott Carpenter and Sébastien Deronne,** Improved Abstraction for Clear Channel Assessment in ns-3 802.11 WLAN Model

  * **Natale Patriciello, Sandra Lagen, Lorenza Giupponi and Biljana Bojovic,** An Improved MAC Layer for the 5G NR ns-3 module

  * **Michele Polese, Tommaso Zugno and Michele Zorzi,** Implementation of Reference Public Safety Scenarios in ns-3

  * **Helder Fontes, Vitor Lamela, Rui Campos and Manuel Ricardo,** ns-3 NEXT: Towards a Reference Platform for Offline and Augmented Wireless Networking Experimentation

  * **Harsh Patel, Hrishikesh Hiraskar and Mohit P. Tahiliani,** Extending Network Emulation Support in ns-3 using DPDK

  * **Akshay Raman, Kimberly Chou and Spyridon Mastorakis,** A Simulation Framework for Peer-to-Peer File Sharing in Named Data Networking

