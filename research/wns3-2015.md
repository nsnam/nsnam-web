---
layout: page
title: WNS3 2015
permalink: /research/wns3/wns3-2015/
---
The Workshop on ns-3 (WNS3) is a one and one-half day workshop to be held on May 13-14, 2015 in Castelldefels (Barcelona), Spain. The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities.

WNS3 is part of a week-long event that will be hosted by Centre Tecnològic de Telecomunicacions de Catalunya (CTTC). ns-3 training sessions will be held first two days (Monday-Tuesday). On Thursday afternoon, the annual meeting of the ns-3 Consortium will take place, and remaining time Friday will be devoted to informal developer meetings and coding.
