---
layout: page
title: WNS3 2020 Program
permalink: /research/wns3/wns3-2020/program/
---
# Proceedings

The proceedings are published in the [ACM Digital Library](https://dl.acm.org/doi/proceedings/10.1145/3389400).

# Wednesday, June 17, 2020, 13:00 UTC

Links to slides and to pre-recorded videos are provided below for each paper.

#### Paper presentations

  1) **Sian Jin, Sumit Roy, Weihua Jiang, and Thomas R. Henderson,** Efficient Abstractions for Implementing TGn Channel and OFDM-MIMO Links in ns-3; [video](https://drive.google.com/file/d/1vUSUVd8Bv60XGW0ib26td3Os5aElE8x6/view); [slides](https://drive.google.com/file/d/1lkE897PghysTW3QWTwIobuioEfNT3tLS/view)

  2) **Tommaso Zugno, Michele Polese, Natale Patriciello, Biljana Bojovic, Sandra Lagen, and Michele Zorzi,** Implementation of a Spatial Channel Model for ns-3; [video](https://drive.google.com/file/d/1xgvRyNwzdi-n9-vlaxsbdrfseqYPa3aA/view); [slides](https://drive.google.com/file/d/1f4QBMbDUqOWFc-7i3OkQPbbAR8To0ZaF/view) **(Awarded Best Paper)**

  3) **Matteo Drago, Tommaso Zugno, Michele Polese, Marco Giordani, and Michele Zorzi,** MilliCar - An ns-3 Module for mmWave NR V2X Networks; [video](https://drive.google.com/file/d/1I105PjCUJWk7lh-4zdEYPdu6KdkO2zmj/view); [slides](https://drive.google.com/file/d/1ddcMSXzPFQngFxpNLhWQTB9d1mO3BQvj/view)

  4) **Benjamin Sliwa, Mahuel Patchou, Karsten Heimann, and Christien Wietfeld,** Simulating Hybrid Aerial- and Ground-based Vehicular Networks with ns-3 and LiMoSim; [video](https://drive.google.com/file/d/1buPW8CXKp7Xm4TL2lw6uHgQ1FyCQtkPF/view); [slides](https://drive.google.com/file/d/1yjevB5kKZ7p07VqAEKtysNWCGN_at_2n/view?usp=sharing)

  5) **Oscar Bautista Chia and Kemal Akkaya,** Extending IEEE 802.11s Mesh Routing for 3-D Mobile Drone Applications in NS-3; [video](https://drive.google.com/file/d/1-PWm_dADOFEWSqqHvg1yWxkuEOUhBHnA/view); [slides](https://drive.google.com/file/d/1cs_N-ohAOxIMCGfMNHwFUPbP_Vm99ZkO/view)

  6) **Ahmet Kurt and Kemal Akkaya,** Connectivity Maintenance Extensions to IEEE 802.11s MAC Layer in NS-3; [video](https://drive.google.com/file/d/174K4_FsMgGW3wC20Ifq3k81lKRncTgTu/view); [slides](https://drive.google.com/file/d/1aIB7oUNznR-WDdHRqb9xmIPGhMsuKeDd/view)

# Thursday, June 18, 2020, 13:00 UTC

#### Paper presentations

  7) **Renato Cruz, Helder Fontes, José Ruela, Manuel Ricardo, and Rui Campos,** On the Reproduction of Real Wireless Channel Occupancy in ns-3; [video](https://drive.google.com/file/d/1arFlzh4xJYbIoTWjxH3lx_dX6CE-T6w0/view); [slides](https://drive.google.com/file/d/1DAltPOjAwnUFBTZso6dW8zgah4QpdmP5/view)

  8) **Vivek Jain, Thomas R. Henderson, Shravya K. S., and Mohit P. Tahiliani,** Data Center TCP in ns-3:  Implementation, Validation, and Evaluation; [video](https://drive.google.com/file/d/1ryQmCZx4Q-yxuL79Lyv4BzjAMJ9BtdQd/view); [slides](https://drive.google.com/file/d/1ChtPO-oYb9LxdrhVtotfMDsR6YwUMcr_/view)

  9) **Hao Yin, Pengyu Liu, Keshu Liu, Liu Cao, Lytianyang Zhang, Yayu Gao, and Xiaojun Hei,** NS3-AI:  Fostering Artificial Intelligence Algorithms for Networking Research; [video](https://drive.google.com/file/d/1jJ6aO5PhHs3sU4XalIZ_exWToFHqFoOT/view); [slides](https://drive.google.com/file/d/1jqM12j9V2r32WyRVpwe4iVMl_JR3FDHq/view)

#### Lightning talk session

Lightning talks are brief 5-minute talks, and are not part of the published proceedings.

  1) **Arsh Goyal,** Custom Link Adaptation Algorithms (not presented due to technical difficulties)

  2) **Stefano Avallone,** Future plans for the ns-3 wifi module

  3) **Sharan Naribole,** IEEE 802.11be Multi-link Operation in ns-3

  4) **Szymon Szott,** nETFRAStructure: an Exemplary Text-based Framework for Running and Analyzing Simulations in ns-3

  5) **Muhammad Naeem,** Simulation of a Sigfox-based Case Study

  6) **Lorenza Giupponi,** 5G NR: activities and roadmap

  7) **Ben Newton,** Spatial indexing and clipping for accelerating large-scale ns-3 simulations

  8) **Anand Ganti,** Clustering as a load balancing method to speed up PDES in ns-3

  9) **Shyam Parekh,** Software Defined Mobile Load Balancing (MLB) in LTE & 5G Networks

  10) **Davide Magrin,** Validation of the ns-3 802.11ax OFDMA implementation

#### ns-3 Consortium Annual Meeting

The workshop concluded with the annual meeting of the [ns-3 Consortium](https://www.nsnam.org/consortium/); [slides](https://www.nsnam.org/wp-content/uploads/2020/ns-3-consortium-talk-2020-final.pdf).
