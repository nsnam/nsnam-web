---
layout: page
title: Call for Lightning Talks
permalink: /research/wns3/wns3-2020/call-for-lightning-talks/
---

Do you have some work that you would like to share with the ns-3 community?  The Technical Program Committee co-chairs are soliciting [lightning talk](https://en.wikipedia.org/wiki/Lightning_talk) proposals for a session to immediately follow the paper presentations on Thursday June 18.  We will accept up to 10 lightning talks (first come, first served); to apply, please contact the TPC co-chairs by June 10 with the title of your talk.

### Technical Program Co-Chairs

* Stefano Avallone, <stavallo@unina.it>
* Matthieu Coudron, <mattator@gmail.com>

