---
layout: page
title: Registration
permalink: /research/wns3/wns3-2020/registration/
---

The workshop will be held as a Zoom meeting.  Please
[register in advance](https://zoom.us/meeting/register/tJwof-GuqDwpHdfYzkGABMtXf10ty2m1UrM_) for this meeting.

After registering, you will receive a confirmation email containing information about joining the meeting.
