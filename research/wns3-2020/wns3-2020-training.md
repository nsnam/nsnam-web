---
layout: page
title: WNS3 2020 Training
permalink: /research/wns3/wns3-2020/training/
---

Due to the rescheduling of the annual meeting, we are not sure about whether training will be offered this year.  Materials from past training sessions are available [here](https://www.nsnam.org/wiki/Training).
