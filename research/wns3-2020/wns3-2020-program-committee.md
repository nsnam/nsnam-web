---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2020/program-committee/
---

# Technical Program Co-Chairs

* Stefano Avallone <stavallo@unina.it>
* Matthieu Coudron <mattator@gmail.com>

# Proceedings Chair

* Eric Gamess, Jacksonville State University <egamess@gmail.com>

# Technical Program Committee

Name                 | Institution                                        |
---------------------|-----------------------------------------------------
Alexander Afanasyev  | Florida International University, USA |
Ramon Aguero         | University of Cantabria, Spain |
Hany Assasa          | IMDEA Networks Institute, Spain |
Peter Barnes         | Lawrence Livermore National Laboratory, USA |
Doug Blough          | Georgia Institute of Technology, USA |
Sébastien Deronne    | Televic Conference, Belgium |
Simone Ferlin        | Ericsson Research, Sweden |
Helder Fontes        | INESC and FEUP, Portugal |
Eric Gamess          | Jacksonville State University, USA |
Lorenza Giupponi     | Centre Tecnològic de Telecomunicacions de Catalunya, Spain |
Tom Henderson        | University of Washington, USA |
Pasquale Imputato    | University of Napoli Federico II |
Anil Jangam          | Cisco Systems, USA |
Sam Jansen           | StarLeaf Ltd., United Kingdom |
Dong Jin             | Illinois Institute of Technology, USA |
Leonardo Lanante     | Kyushu Institute of Technology, Japan |
Davide Magrin        | University of Padova, Italy |
Spyridon Mastorakis  | University of Nebraska, Omaha, USA |
Vedran Miletić       | Heidelberg Institute for Theoretical Studies, Germany |
Michele Polese       | University of Padova, Italy |
Mohit Tahiliani      | National Institute of Technology Karnataka, India |
Hajime Tazaki        | IIJ Innovation Institute, Japan |
Thierry Turletti     | Inria, France |
