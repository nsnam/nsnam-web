---
title: Accepted Papers
layout: page
permalink: /research/wns3/wns3-2024/accepted-papers/
---

  * **Felipe Gómez Cuba and Manuel Pena Sande,** Successive Interference Cancellation Implementation for ns-3 MU-MIMO mmWave Module

  * **George Frangulea, Biljana Bojovic and Sandra Lagen,** NR-U and WiFi Coexistence in sub-7 GHz bands: Implementation and Evaluation of NR-U Type 1 Channel Access in NS3

  * **José Antônio Junior and Marcelo Carvalho,** An ns-3 Model for Simultaneous Wireless Information and Power Transfer (SWIPT) over IEEE 802.11ah Networks

  * **Erfan Mozaffariahrar, Michael Menth and Stefano Avallone,** Implementation and Evaluation of IEEE 802.11ax Target Wake Time Feature in ns-3

  * **Muyuan Shen, Jie Zhang, Hao Yin, Sumit Roy and Yayu Gao,** Delay in Multi-Link Operation in ns-3: Validation & Impact of Traffic Splitting

  * **Shyam Krishnan Venkateswaran, Ching-Lun Tai, Roshni Garnayak, Yoav Ben Yehezkel, Yaron Alpert and Raghupathy Sivakumar,** IEEE 802.11ax Target Wake Time: Design and Performance Analysis in ns-3

  * **Neco Villegas, Ana Larrañaga, Luis Diez, Katerina Koutlia, Sandra Lagén and Ramón Agüero,** Extending QoS-aware scheduling in ns-3 5G-LENA: A Lyapunov based solution

