---
layout: page
title: WNS3 2024 Program
permalink: /research/wns3/wns3-2024/program/
---

The **Workshop on ns-3** was a single day workshop on Wednesday June 5, included as part of the **ns-3 annual meeting** which ran from June 3 through June 6.

The annual meeting was an in-person event, with a few sessions
recorded for later archiving on Vimeo.  The following sessions were be available via a free Zoom meeting:

* Tuesday June 4, 16h00, **ns-3 Consortium Annual Meeting**
* Wednesday, June 5, 16h00, **WNS3 Lightning Talks**
* Thursday, June 6, all-day, **ns-3 development meeting**

# Monday, June 3:  ns-3 training

ns-3 training is an introduction to ns-3, for users just starting with ns-3. We will provide a comprehensive overview of the most commonly used parts of ns-3.  This session has not been offered since 2019; videos and slides of
[past sessions](https://www.nsnam.org/consortium/activities/training/) from 2019 and earlier are archived.

# Tuesday, June 4:  Invited talks and advanced tutorials

This day consisted off invited talks and advanced tutorials.

09:00-10:30: **5G NR module: A general overview and new NR 3.0 MIMO models and usage (Biljana Bojovic)**: In this session, we will overview 5G-LENA history, highlight the main features, and provide insights into the recently integrated MIMO model. We will provide also a brief discussion on the issues related to tradeoffs between the PHY layer model fidelity and simulator scalability.  Finally, we will walk through a MIMO example simulation program.

* [video](https://vimeo.com/1021329376?share=copy)
* [slides](/tutorials/consortium24/NR-Tutorial-2024.pdf)
* [slides SU-MIMO portion](/tutorials/consortium24/NR-Tutorial-SU-MIMO.pdf)
* [command line history](/tutorials/consortium24/command-line-history.docx)

11:00-11:30: **5G NR module (continued) (Biljana Bojovic)**:

11:30-12:30: **5G NR Sidelink, ProSe, and Public Safety Communications models (Tom Henderson)**:  This talk will focus on recently published ns-3 extension modules, including the latest 5G NR V2X release, a SIP module, and 5G Proximity Services and Public Safety Communications modules from NIST.

* [slides](/tutorials/consortium24/wns3-2024-NR-Sidelink-update-final.pdf)
* [NR ProSe code](https://apps.nsnam.org/app/nr-prose)
* [Public Safety Communications code](https://apps.nsnam.org/app/psc)

14:00-15:00: **Wi-Fi models and new features (Tom Henderson)**:  This talk will summarize recent and upcoming additions to the ns-3 Wi-Fi module that the speaker is involved with, including new trace helpers, preamble detection, fading error models, and low-latency queueing.  Next, we will summarize some pending features in the tracker, such as CSI exchange, and signal clipping.  Finally, some brief updates will be provided regarding features that the Wi-Fi maintainers are working on.

* [slides](/tutorials/consortium24/wns3-2024-wi-fi-update-final.pdf)

15:00-15:30: **L4S and Low-Latency Wi-Fi (Greg White)**: Low Latency, Low Loss, and Scalable throughput (L4S) is a framework using active queue management (AQM) and end-to-end congestion control to enable low-latency networking.  This talk will summarize recent work involving the use of ns-3 to study the application of L4S to Wi-Fi networks.

16:00-17:30: **ns-3 Consortium annual meeting**

The annual business meeting of the [ns-3 Consortium](https://www.nsnam.org/consortium/) was held at the end of the day.  This meeting summarized project activity over the past year and discussed future plans for ns-3.

# Wednesday, June 5:  WNS3 paper presentations and lightning talks

All papers awarded with the [ACM Artifacts Evaluated](https://www.acm.org/publications/policies/artifact-review-and-badging-current) badge are indicated with the badge ![symbol](/wp-content/uploads/2023/artifacts-available.png).  Four papers were also credited with having exemplary artifacts.

09:00-10:00: Introductions and keynote talk

**Keynote talk: Marcos Martínez Vázquez, Director of Standards Engineering at MaxLinear:** MaxLinear experience using ns-3 for development of new wireless silicon components

10:00-10:30:  Paper presentation

* **(Awarded Best Paper)** ![badge](/wp-content/uploads/2023/artifacts-available.png) **José Antônio Junior and Marcelo Carvalho,** An ns-3 Model for Simultaneous Wireless Information and Power Transfer (SWIPT) over IEEE 802.11ah Networks

11:00-12:30:  Three paper presentations

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Felipe Gómez Cuba and Manuel Pena Sande,** Successive Interference Cancellation Implementation for ns-3 MU-MIMO mmWave Module

* **George Frangulea, Biljana Bojovic and Sandra Lagen,** NR-U and WiFi Coexistence in sub-7 GHz bands: Implementation and Evaluation of NR-U Type 1 Channel Access in NS3

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Neco Villegas, Ana Larrañaga, Luis Diez, Katerina Koutlia, Sandra Lagén and Ramón Agüero,** Extending QoS-aware scheduling in ns-3 5G-LENA: A Lyapunov based solution

14:00-15:30:  Three paper presentations

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Muyuan Shen, Jie Zhang, Hao Yin, Sumit Roy and Yayu Gao,** Delay in Multi-Link Operation in ns-3: Validation & Impact of Traffic Splitting

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Erfan Mozaffariahrar, Michael Menth and Stefano Avallone,** Implementation and Evaluation of IEEE 802.11ax Target Wake Time Feature in ns-3

* ![badge](/wp-content/uploads/2023/artifacts-available.png) **Shyam Krishnan Venkateswaran, Ching-Lun Tai, Roshni Garnayak, Yoav Ben Yehezkel, Yaron Alpert and Raghupathy Sivakumar,** IEEE 802.11ax Target Wake Time: Design and Performance Analysis in ns-3

16:00-17:30:  **Lightning talks**

* **Gabriel Ferreira,** "NR and LTE Performance Improvements"
* **Gabriel Ferreira,** "Educational use of ns-3".
* **Evan Black,** "NetSimulyzer update"
* **Andrea Lacava, Matteo Pagin,  Tommaso Pietrosanti and Michele Polese,** "Toward large-scale online reinforcement learning with sem and ns-3"
* **Jason Pandian,** "Modeling the Astrodynamics of Space Missions in ns-3 for the High-Fidelity  Simulations of Deep Space Communications"
* **Kavya Bhat,** "DHCPv6 project, Google Summer of Code"
* **Hyerin Kim,** "Enhancement of RL Approach Accessibility in NR, Google Summer of Code"
* **Joao Albuquerque,** "5G NR Module Benchmark and Analysis for Distinct Channel Models, Google Summer of Code"

Lightning talks are brief talks (5-10 minutes) to update the ns-3 community on works-in-progress, updates to previously released models, or new ideas.  This session was hybrid with remote participation.

# Thursday, June 6:  Software development discussions

Thursday was an informal day for software development discussions among maintainers and any interested attendees.  There was no formal registration for attending this day, and attendance was open to anyone interested.

**Agenda and minutes**: [https://www.nsnam.org/wiki/MaintainersJune2024](https://www.nsnam.org/wiki/MaintainersJune2024)
