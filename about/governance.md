---
title: Governance
layout: page
permalink: /about/governance/
---

ns-3 is an open source software project developed and maintained by
a community of contributors.

The project is primarily led by the software maintainers, including a
lead maintainer, and there are other roles and groups defined.  A
steering council (consisting largely but not exclusively of maintainers)
oversees other decisions for the project, including expenditures, and
other contributor roles, such as community managers and treasurer, are
also defined.  The project is associated with
[Software in the Public Interest (SPI)](https://www.spi-inc.org/),
which acts as the project's fiscal sponsor.
The pages under this section provide the bylaws, policies, and
contributor roles for the project.

This governance model has been adopted as of September 2024.

* [Bylaws](/about/governance/bylaws/)
* [Policies](/about/governance/policies/)
* [Maintainers](/about/governance/maintainers/)
* [Steering Council](/about/governance/council/)
* [Other Project Roles](/about/governance/other/)

