---
title: Software
layout: page
permalink: /about/consortium/activities/software/
---

<b>Note:</b> This page describes the software funding from the University of
Washington NS-3 Consortium which is no longer operating, and this page
is for historical reference.

The Consortium funded the following software development activities:

In 2021-22, we funded **WiGig module maintenance** via maintainer Sebastien Deronne.  The outcome of this work was the WiGig module in the ns-3 app store.
  * **initial phase** (through Sept. 2021): **[Progress report](https://www.nsnam.org/wp-content/uploads/2021/wigig-report-sept-2021.pdf)**
  * **second phase** (initiated Oct. 2021): Port WiGig classes to new architecture, add DMG PHY entity, add DMG MAC FEM.

In 2020, we funded **ns-3 wifi module maintenance** via maintainer Sebastien Deronne.  The work was conducted in two phases, as summarized in the following reports:

  * **Phase 1** (through May 2020): **[PHY abstraction, Bianchi example, and rate controls](https://www.nsnam.org/wp-content/uploads/2020/ns-3-wifi-module-upgrade-phase-1.pdf)**
  * **Phase 2** (through December 2020): **[LAA-Wifi coexistence, PHY error models, and PHY refactoring work](https://www.nsnam.org/wp-content/uploads/2021/ns-3-wifi-module-upgrade-phase-2.pdf)**

We have also funded **[website development and content migration](https://finance.uw.edu/c2/design-web/getting-started)** for [the main web site](https://www.nsnam.org).

The previously organized ns-3 Consortium (which ran from 2012-2018) funded the following.

  * The port of [ns-3 for Visual Studio 2012](http://www.nsnam.org/wiki/Ns-3_on_Visual_Studio_2012) was organized by the Consortium in 2013.
