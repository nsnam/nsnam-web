---
title: Meetings
layout: page
permalink: /about/consortium/activities/meetings/
---
<b>Note:</b> This page describes meeting materials and minutes from the University of
Washington NS-3 Consortium which is no longer operating, and this page
is for historical reference.

This page provides meeting materials for the current ns-3 Consortium, starting
in June 2019.

 * April 1, 2022:  <a href="https://www.nsnam.org/wp-content/uploads/2022/minutes-040122.pdf" target="_blank">advisory board meeting minutes</a>
 * February 8, 2022:  <a href="https://www.nsnam.org/wp-content/uploads/2022/minutes-020822.pdf" target="_blank">advisory board meeting minutes</a>
 * Sept 29, 2021:  <a href="https://www.nsnam.org/wp-content/uploads/2021/minutes-092921.pdf" target="_blank">advisory board meeting minutes</a>
 * June 2021:  <a href="https://www.nsnam.org/wp-content/uploads/2021/ns-3-consortium-talk-2021.final.pdf" target="_blank">annual meeting slides</a>
 * February 24, 2021:  <a href="https://www.nsnam.org/wp-content/uploads/2021/minutes-022421.pdf" target="_blank">advisory board meeting minutes</a>
 * Sept 21, 2020:  <a href="https://www.nsnam.org/wp-content/uploads/2020/minutes-092120.pdf" target="_blank">advisory board meeting minutes</a>
 * Sept 3, 2020:  <a href="https://www.nsnam.org/wp-content/uploads/2020/minutes-090320.pdf" target="_blank">advisory board meeting minutes</a>
 * June 2020:  <a href="https://www.nsnam.org/wp-content/uploads/2020/ns-3-consortium-talk-2020-final.pdf" target="_blank">annual meeting slides</a>
 * June 2019:  <a href="https://www.nsnam.org/wp-content/uploads/2019/ns-3-annual-meeting-slides.pdf" target="_blank">annual meeting slides</a>
