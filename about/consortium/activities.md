---
title: Activities
layout: page
permalink: /about/consortium/activities/
---
<b>Note:</b> This page describes the activities of the University of
Washington NS-3 Consortium which is no longer operating, and this page
is for historical reference.

The main activities are focused on promoting the development and maintenance of ns-3 software.  However, the ns-3 project operates separately as an open source project.

The activities that the Consortium oversees are as follows:

 * organize an **annual meeting** for Consortium members; this meeting may coincide with a technical conference or may be an independent event.  This event is typically the annual **[Workshop on ns-3](/research/wns3/)**.
 * organize and provide for an **[annual training](/about/consortium/activities/training/)** course for Consortium members, typically held with the annual Workshop on ns-3.
 * recommend or fund **[software development](/about/consortium/activities/software/),** tests, or documentation based on member contributions.
 * **administrative** items dealing with Consortium finances and project maintenance activities.
