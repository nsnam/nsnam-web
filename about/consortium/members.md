---
title: Members
layout: page
permalink: /about/consortium/members/
---
<b>Note:</b> This page describes the organization of the University of
Washington NS-3 Consortium which is no longer operating, and this page
is for historical reference.

**The Founding Executive Members are:**

<a href="http://www.inria.fr/centre/sophia/" target="_blank"><img src="/wp-content/uploads/2013/01/inr_logo_cherch_UK_coul.png" alt="INRIA" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.ee.washington.edu" target="_blank"><img src="/wp-content/uploads/2013/01/University_of_Washington_Seal-100pixels.png" alt="University of Washington" /></a>

**Additional Executive Members are:**

<a href="http://www.cttc.es" target="_blank"><img src="/wp-content/uploads/2013/02/CTTC_logo_square.png" alt="CTTC" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.inesctec.pt/en/" target="_blank"><img src="/wp-content/uploads/2019/INESCTEC_Logo.jpg" alt="INESC TEC" /></a>

<a href="http://www.nitk.ac.in/" target="_blank"><img src="/wp-content/uploads/2016/04/NITK_100_by_100.png" alt="NITK Surathkal" /></a>&nbsp;&nbsp;<a href="http://www.nitk.ac.in/" target="_blank"><b><font size="3">NITK Surathkal</font></b></a>&nbsp;&nbsp;&nbsp;<font size="4"><a href="http://www.ece.gatech.edu/" target="_blank">Georgia Institute of Technology</a></font>

**Consortium Members:**

<a href="https://www.cablelabs.com" target="_blank"><img src="/wp-content/uploads/2018/09/CableLabs-logo-scaled-225.png" alt="CableLabs" /></a>

<a href="http://www.nitk.ac.in/" target="_blank"><img src="/wp-content/uploads/2018/03/Hustseals_100x100.png" alt="HUST" /></a>&nbsp;&nbsp;<a href="http://english.hust.edu.cn/" target="_blank"><b><font size="3">Huazhong University of Science and Technology</font></b></a>

