---
layout: page
title: Former Consortia
permalink: /about/consortium/
---
The project was supported by two consortia during the 2013-2024 timeframe.
The first consortium was organized by Inria and the University of Washington,
and operated between 2013-18.  The second consortium was led by the University
of Washington and operated between 2018-2024.  The University of Washington
Consortium ceased operation in July 2024, and the main activity (organization
of the annual meeting) will continue under the new [governance structure](/about/governance/).  These web pages are provided for
historical reference.  Please contact the <a href="mailto:council@nsnam.org">Steering Council</a> if you are interested in supporting the ns-3 project.

The University of Washington NS-3 Consortium was a collection of organizations cooperating to support the open source project by organizing activities such as the [ns-3 annual meeting](/research/wns3/) including the Workshop on ns-3 and ns-3 training sessions, by providing a point of contact between industry and ns-3 developers, and by supporting administrative activities and purchases necessary to conduct a large project.

Membership to the Consortium was open to those institutions that signed the membership agreement and paid the annual dues.  The Consortium was overseen by an Advisory Board consisting of all members of the Consortium.

