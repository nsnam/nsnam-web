---
title: Direct Code Execution
layout: page
permalink: /about/projects/direct-code-execution/
---

### What is Direct Code Execution?

Direct Code Execution (DCE) is a framework for ns-3 that provides facilities to execute, within ns-3, existing implementations of userspace and kernelspace network protocols or applications without source code changes. For example, instead of using ns-3's implementation of a ping-like application, you can use the real ping application. You can also use the Linux networking stack in simulations.

### News

  * 8th September, 2021: [DCE 1.11](https://github.com/direct-code-execution/ns-3-dce/releases/tag/dce-1.11) released.
  * 26th February, 2018: [DCE 1.10](https://github.com/direct-code-execution/ns-3-dce/releases/tag/dce-1.10) released.
  * 21 February, 2018: DCE repositories are moved to [GitHub](https://github.com/direct-code-execution).
  * 17th October, 2016: [DCE 1.9](https://github.com/direct-code-execution/ns-3-dce/releases/tag/dce-1.9) released.
  * 25th March, 2016: [DCE 1.8](https://github.com/direct-code-execution/ns-3-dce/releases/tag/dce-1.8) released.

### Why use Direct Code Execution?

  * If you don't want to maintain multiple implementations of a single protocol, or reimplement complicated protocols such as OSPF or BGP routing,
  * If you want to debug and test your code within a controlled environment,
  * If you want to create a miniature network (let's say, a model of an ISP network) in a single node,

DCE promises to help for these use cases!

### How to use it?

Currently, you need a Linux machine to run DCE with your applications.

More instruction is available on [Quick start document](http://ns-3-dce.readthedocs.io/en/latest/getting-started.html) and the [User's guide](http://ns-3-dce.readthedocs.io/en/latest/dce-user-doc.html).

### Instructions

  * Quick start.

    **bake** will take care of your DCE installation. See the [Quick start guide](http://ns-3-dce.readthedocs.io/en/latest/getting-started.html)&nbsp;for more detail.
  * How can I use my own application in ns-3?

    The first step to using your applications on DCE is recompiling your executable with special compile/link flags. A Position Independent Executable (PIE) is required to run over DCE. The detailed instructions are available in [User's guide](http://ns-3-dce.readthedocs.io/en/latest/dce-user-doc.html).
  * What should I do if my application doesn't run on DCE?

    If the issue is in your application, you can debug your code with gdb. The instructions are also available in [User's guide](http://ns-3-dce.readthedocs.io/en/latest/dce-user-doc.html).
  * How can I extend DCE ?

    DCE provides much of the POSIX API, but not a complete implementation. If your application uses unsupported system calls, socket options, or libc functions, you can extend DCE so that your applications are able to run over DCE. More information is available in the [document](http://ns-3-dce.readthedocs.io/en/latest/dce-user-doc.html). Patches to extend DCE for more applications are encouraged.
  * How can I learn more about DCE?

    All of DCE related information is listed in this page. Other detailed documents are also [available](http://ns-3-dce.readthedocs.io/en/latest/dce-subprojects.html).

<!-- Place this tag after the last +1 button tag. -->





<!-- FB button -->



<!-- div id="fb-root"></div -->



<!-- twitter -->





<!-- facebook insight -->
