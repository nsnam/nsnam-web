---
title: Steering Council
layout: page
permalink: /about/governance/council/
---

Although most project activity is driven by the maintainers, a Steering
Council is defined in the bylaws to organize and lead activities and
decisions that do not primarily pertain to software decisions.
The Steering Council and its members play a special role in certain
situations. In particular, the council may:

-   Make decisions about the overall scope, vision, and direction of the
    project.
-   Make decisions about strategic collaborations with other
    organizations or individuals.
-   Make decisions about legal issues.
-   Make decisions about the Services that are run by The Project and
    manage those Services for the benefit of The Project and Community.
-   Make decisions when regular community discussion does not produce
    consensus on an issue in a reasonable time frame.
-   Create processes to empower community consensus building on important
    policy questions.
-   Create and update policy documents.
-   Approve expenditures of Project funds.
-   Appoint a Lead Maintainer.

The current Steering Council consists of:

* <b>Eduardo Almeida (chair)</b>
* <b>Peter Barnes</b>
* <b>Biljana Bojovic</b>
* <b>Tom Henderson</b>
* <b>Mohit Tahiliani</b>

The steering council can be reached at <a href="mailto:council@nsnam.org">council@nsnam.org</a>.
