---
title: Other Project Roles
layout: page
permalink: /about/governance/other/
---

# Community Managers

* Amir Ashtari (ns-3 LinkedIn page maintainer)

* Mohit Tahiliani (Industrial outreach, forums and talks)

_We are looking for additional community managers_ to help with outreach
in general, and also for the areas of documentation, educational resources,
bibliography, website management and development, and tutorials and training.
If you would like to serve as an ns-3 community manager, please contact
the lead maintainer or steering council chair to express interest.

# Release Manager

* Tom Henderson

# Treasurer and SPI Liaison

* Tom Henderson

# Organizational committee for the 2025 annual meeting

The organizational committee for the 2025 annual meeting consists of:

* Alberto Gallegos Ramonet (General Chair)
* Eric Gamess (Proceedings Chair)
* Tommaso Pecorella (co-TPC Chair)
* Mohit Tahiliani (co-TPC Chair)
* Tom Henderson

# 2024 Google Summer of Code Mentors

* Amir Ashtari
* Biljana Bojovic
* Gabriel Ferreira
* Katerina Koutlia
* Tommaso Pecorella
* Alberto Gallegos Ramonet
* Manoj Kumar Rana
