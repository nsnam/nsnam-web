---
title: Bylaws
layout: page
permalink: /about/governance/bylaws/
---

The purpose of this document is to formalize the governance process used
by the ns-3 project, and
to clarify how decisions are made and how the various members of our
community interact, including the relationship between open source
collaborative development and work that may be funded by for-profit or
non-profit entities.

Specific procedures are described in policy documents, which are adopted
by the Steering Council defined herein. The full set of current policies
is available on the [policies](/about/governance/policies/)
page. This document, together with the set of current policies, completely
describe operation of the Project.

## The Project

The ns-3 project (The Project) is a free and open source software project.
The goal of The Project is to develop open source software for discrete-event
network simulation, and, in particular, the ns-3 simulator and related
components. The software developed by The Project is released under free
open source software licenses, developed openly and hosted on public
repositories. The Project is focused on providing software and documentation
with an emphasis on supporting reproducible and technically credible research,
via peer production and review, for users with varying programming skills.

The Project is developed by a team of distributed developers, called
Contributors. Contributors are individuals who have contributed code,
documentation, designs, or other work to The Project. Anyone can be a
Contributor. Contributors can be affiliated with any legal entity or
none. Contributors participate in the project by submitting, reviewing,
and discussing software proposals and technical issues, and by participating
in public project discussions on source code repository sites, mailing lists,
and other channels. The foundation of Project participation is openness and
transparency.

The Project Community consists of all Contributors and Users of the
Project. Contributors work on behalf of and are responsible to the
larger Project Community, and we strive to keep the barrier between
Contributors and Users as low as possible.

The Project is not a legal entity. The Contributors can be considered
to be an unincorporated association of individuals working to further
the goals of The Project. The Project can associate with other legal
entities that can provide services to The Project, including but not
limited to fiscal, legal, administrative, software development, and
business services.

## Governance

This section describes the governance and leadership model of The
Project. The foundations of project governance are:

-   openness and transparency
-   active contribution
-   institutional neutrality

Traditionally, project leadership has been provided by a
subset of Contributors, called Maintainers, whose active and consistent
contributions have been recognized by their receiving "commit rights"
to The Project software repositories. Henceforth, the Project is organized
as follows. A subset of
Contributors are also named as Maintainers, Community Managers,
and/or Steering Council members, with one person serving in a Lead
Maintainer role. Administrative and fiscal decisions
are handled by the Steering Council, while software decisions
are handled by Maintainers. The Lead Maintainer has the
final authority for decisions of both groups. The list of members
serving in each capacity is maintained on The Project website.

In general, all Project decisions not delegated to the Steering
Council are
made through consensus among the Maintainers, with input from the
Community. If consensus can not be achieved, a decision is taken by
the Lead Maintainer.

The positions defined herein are:

- Lead Maintainer
- Maintainer
- Release Manager
- Steering Council Member
- Council Chair
- Treasurer
- Community Manager

An individual may simultaneously serve in more than
one position. The current list for all positions is maintained on
the project website.

### Lead Maintainer

The Project has a Lead Maintainer who has the authority to
make all final decisions for The Project. The Lead Maintainer, in
practice, chooses to defer that authority to the consensus of the
community discussion channels and Maintainers, and to the Steering Council.
It is expected that the Lead Maintainer will only
rarely assert their final authority. Because rarely used, we refer to
Lead Maintainer's final authority as a "special" or "overriding" vote. When it does
occur, the Lead Maintainer override typically happens in situations where there is
a deadlock in the Steering Council or if the Steering Council asks the
Lead Maintainer to make a decision on a specific matter. To ensure the benevolence
of the Lead Maintainer, The Project encourages others to fork the project if they
disagree with the overall direction the Lead Maintainer is taking. The Lead Maintainer
may delegate their authority on a particular decision or set of decisions
to any other Council member or ad hoc committee at their discretion.

The Lead Maintainer is appointed by the Steering Council and their term
is indefinite.
The Lead Maintainer can recommend their successor to the Steering Council.
When filling this position, the Steering Council will decide on a candidate,
either by consensus or by majority vote if needed. This decision process and
discussion thereof is private to the Steering Council. Once a candidate is
selected, the Council then requests the Chair to sound out the candidate on
their willingness to serve. If the candidate agrees, the Steering Council
votes in public on the selected candidate.

Note that the Lead Maintainer can step down at any time, and acting in good faith,
will also listen to serious calls to do so. Also note that the Lead Maintainer is
more a role for fallback decision making rather than that of a
director/CEO.

### Maintainer

A Maintainer has responsibility and authority to oversee different portions of the codebase.
Maintainers work with Contributors to marshal in patches or extensions to their respective
scope of maintenance. They also participate in timely code reviews, bug fixing, and respond
to questions on mailing lists, pertaining to their respective scope of maintenance.

Maintainers coordinate with other maintainers for patches that have larger scope than their
scope of maintenance. Maintainers can (and many times do) also contribute to modules that are not their direct responsibility. Maintainers may also help with platform-specific issues, tooling,
or packaging. Maintainers are expected to participate in cross-cutting or broadly scoped
technical discussions among the group of Maintainers.

Membership in the group of Maintainers is managed by a policy approved by the Council, which
is detailed in a separate document [Note: link to be provided in future].
The initial group of Maintainers are those who, as of September 2024, have commit privileges to
the GitLab.com ns-3-dev repository and who have participated in at least one commit, merge,
or tracker discussion in the past two years.

### Release Manager

A Release Manager is a Maintainer who has has final responsibility for coordinating a software release.
This includes:

-   Proposing and deciding on the timing of a release.
-   Gather proposals from the maintainers and contributors on the content of a release.
-   Determining the content of a release in case there is no consensus
    on a particular change or feature.
-   Coordinate the integration and timing of the introduction of new features.
-   Creating the release and announcing it on the relevant public
    channels.

For more details on what those responsibilities look like in practice,
see the section on making releases in the ns-3 documentation.

The Lead Maintainer may either appoint Release Managers or may serve
as the default Release Manager as needed.

### Steering Council Member

The Project has a Steering Council with members drawn from the set of Project
Contributors who have produced contributions that are substantial in
quality and quantity, and sustained over at least one year. The overall
role of the Council is to ensure, through working with the Lead Maintainer and
taking input from the Community, the long-term well-being of the
project, both technically and as a community. The Steering Council
is expected to consist of active Maintainers, but may also
include Members who may not be as deeply active with software development
and review. In other words, substantial contributions are not merely
defined as software contributions.

The Council has a Chair (described below) who is tasked with keeping the
organizational aspects of the functioning of the Council and The Project
on track.

During the everyday project activities, Council Members participate in
all discussions, code review, and other project activities as peers with
all other Contributors and the Community. In these everyday activities,
Council Members do not have any special power or privilege through their
membership on the Council. However, it is expected that because of the
quality and quantity of their contributions and their expert knowledge
of The Project software and services, Council Members provide
useful guidance, both technical and in terms of project direction, to
potentially less experienced contributors.

The Steering Council and its Members play a special role in certain
situations. In particular, the Council may:

-   Make decisions about the overall scope, vision, and direction of the
    project.
-   Make decisions about strategic collaborations with other
    organizations or individuals.
-   Make decisions about legal issues.
-   Make decisions about the Services that are run by The Project and
    manage those Services for the benefit of The Project and Community.
-   Make decisions when regular community discussion does not produce
    consensus on an issue in a reasonable time frame.
-   Create processes to empower community consensus building on important
    policy questions.
-   Update policy documents, such as this one.
-   Approve expenditures of Project funds.
-   Appoint a Lead Maintainer.

#### Council membership

The Council will be initially formed from a subset of project Maintainers
who, as of September 2024, have been significantly active in the open source
project community over the last two years and who have expressed interest
to serve on the Council. Council membership will subsequently be governed by
the following rules.

The Lead Maintainer is an ex-officio voting member of the Steering Council.
The Treasurer (defined below) may be a Steering Council member or
alternatively may be an ex-officio, non-voting member. No other ex-officio
membership to the council is defined.

Potential Steering Council Members must be nominated by an existing Council
Member. Project contributors may be invited to the Council or may indicate
their interest in seeking a nomination by contacting the Council Chair.
Nominations must be acted upon by the Council Chair by a reasonable time
(typically within one or two Steering Council meeting cycles).

When considering potential Steering Council Members, the Council will look
at candidates
with a comprehensive view of their contributions. This will include, but
is not limited to, code, code review, infrastructure work, mailing list
and chat participation, community help/building, education and outreach,
design work, etc. We have deliberately avoided setting arbitrary quantitative
metrics (like "100 commits in this repo") to avoid encouraging behavior
that plays to the metrics rather than the project's overall well-being.
We want to encourage a diverse array of backgrounds, viewpoints, and
talents in our team, which is why we explicitly do not define code as
the sole metric on which council membership will be evaluated.

The Council reserves the right to eject current Members or any other
Contributors if they are deemed
to be actively harmful to the project's well-being, and attempts at
communication and conflict resolution have failed.

Council Members may resign their position at any time.

### Council Chair

The Chair is appointed by the Steering Council by a majority vote
and must be a current member of the Council. The Chair's term is
indefinite. The Chair may resign or be removed by vote of the
Council at any time. The Chair is responsible for:

-   At least annually, reviewing the technical direction of the project.
-   At least annually, summarizing any relevant
    organizational updates and issues in the preceding period, and
    asking for feedback/suggestions on the mailing list.
-   Ensuring that the composition of the Steering Council stays current,
    as outlined above.
-   Ensuring that matters discussed in private by the Steering Council are
    summarized on the mailing list to keep the Community informed.
-   Ensuring that policies adopted or removed by the Steering Council are
    publicly posted on The Project website.
-   Ensuring that other important organizational documents (e.g., Code of
    Conduct) stay current after they are added.

### Treasurer

The Treasurer is the main point-of-contact with fiscal sponsors
and with any other organization holding Project funding. The Treasurer
may be a Council Member or a non-voting ex-officio member of the Council.

### Community Manager

The Steering Council may also name one or more individuals to the
role of Community Manager. Typical roles of a Community Manager
include managing relationships within the project and between the
project and outside entities, outreach, new member onboarding,
and project advocacy.

Like Maintainers, Community Managers may or may not also be members
of the Steering Council. Community Managers should periodically update
the Steering Council on their activities (as needed).

### Other roles

The Steering Council may create other named roles in support of the project,
and such roles may be filled by any Contributor. The Steering
Council should also periodically review whether such roles should
continue and are still actively being worked on.

### Inactivity

If a Contributor appointed to a role becomes generally inactive in their role
(e.g., generally not attending meetings, not addressing issues or reviewing
code within their scope of maintenance), they can be considered for removal.
Before removal, the inactive Contributor will be approached by the Lead
Maintainer or Council Chair (as appropriate) to see if they plan on
returning to active participation. If not, they may be removed
immediately upon a Council vote. If they plan on returning to active
participation soon, they will be given a grace period of one year. If
they do not return to active participation within that time period they
will be removed by vote of the Council without a further grace period. All
former Contributors can be considered for roles again at any
time in the future, like any other project Contributor. Past Contributors
who have served in some capacity will be listed on the project
website, acknowledging the period during which they were active.

### Conflict of interest

It is expected that the Lead Maintainer and Council Members will be employed
at a wide range of companies, universities, and non-profit organizations.
Because of this, it is possible that Members will have a conflict of
interest. Such conflicts of interest include, but are not limited to:

-   Financial interest, such as investments, employment or contracting
    work, outside of The Project that may influence their work on The
    Project.
-   Access to proprietary information of their employer that could
    potentially leak into their work with The Project.
-   Relationships that could be considered a conflict of interest
    in the realm of scholarly activities such as publishing.

Any office holder of The Project shall disclose to the rest of
the Council any conflict of interest they may have. Members with a
conflict of interest in a particular issue may participate in Council
discussions on that issue, but must recuse themselves from voting on the
issue. If the Lead Maintainer has recused theirself for a particular decision,
the Council will appoint a substitute Lead Maintainer for that decision.

It may not be possible for a member to disclose the reason for a conflict of
interest in all circumstances. Therefore, a member may recuse themselves
from specific votes by simply stating that they are recusing for a reason
that they cannot disclose.

### Private communications of the Council

Unless specifically required, all Council discussions and activities
will be public and done in collaboration and discussion with The Project
Contributors and Community. The Council will have a private mailing list
that will be used sparingly and only when a specific matter requires
privacy, as decided by the Council Chair. When private communications
and decisions are needed, the
Council will do its best to summarize those to the Community after
removing personal/private/sensitive information that should not be
posted to the public internet.

### Council decision making

It is expected that most of the business of the Council will be conducted
by consensus building and decisions informally taken by consensus, but
if it becomes necessary for the Steering Council to produce a formal
decision, then they will, by default, use a voting process of one
vote per member, binary yes or no. A reasonable amount of time
should be allowed (such as one week) for a formal vote to allow members
not present in a meeting to vote, and voting may be conducted online.
In the case of a tie outcome, the vote of the Lead Maintainer
(or, if unavailable, the vote of the Council Chair) will break the tie.
The Council may adopt an additional Policy on decision making and voting
that supplements or overrides the above process if it chooses to pursue
a more nuanced voting process.

## Institutional relationships and funding

The Steering Council is the primary leadership for the project. No
outside institution, individual, or legal entity has the ability to own,
control, usurp, or influence The Project other than by participating in
The Project as Contributors. However, because
institutions can be an important funding mechanism for the project, it
is important to formally acknowledge institutional participation in The
Project. The Steering Council is responsible for deciding upon
appropriate recognition of the contributions from any external
institutions, such as acknowledgment on the project website. The
Steering Council may adopt additional policies in the future regarding
institutional contributors, consistent with the above guidelines.

## Changing the Governance Document

Change proposals to this governance document must be submitted by a pull or merge request to
The Project's governance document's repository. There are two phases to the process:

The discussion phase begins when the person first opens a pull or merge request. During this time,
the request must be in a draft state. The pull or merge request is refined in response to public
comment and review, with the goal being consensus in the community.

The pull or merge request author may call a vote when they believe enough feedback and iteration
has occurred. This is triggered by moving the pull or merge request from the draft state to an
active state. This triggers the voting phase.

The voting phase begins when the request enters an active state. The proposed changes in the
request are frozen, and may not be substantively modified after voting has begun. During the
voting phase, the Steering Council votes on whether the changes are ratified and the pull or
merge request merged (accepting the proposed changes) or that the pull or merge request be
closed without merging (rejecting the proposed changes).

All votes are limited in time to 4 weeks after the voting phase begins. At the end of 4 weeks,
the proposal passes if 2/3 of the votes are in favor;
otherwise the proposal is rejected and the request is closed. Prior to the
four-week limit, if at least 80% of the Steering Council has voted and 2/3 of
the votes are in favor, the proposal passes. Since the Lead
Maintainer holds ultimate authority in The Project, the Lead Maintainer has authority to act
alone in accepting or rejecting changes or overriding Steering Council decisions.

## Dissolution

Any member of the Steering Council may move to dissolve The Project by following the above
process to change the governance document and by specifying in the pull or merge request
how any financial assets (after obligations have been paid) shall be distributed and how
any non-financial assets, including but not limited to the domain name registration and
software repository commit access, shall be handled.

## Document history

* September 2024:  Initial posting of these bylaws

## Acknowledgements

Substantial portions of this document were adapted from the
[SciPy project\'s governance document](https://docs.scipy.org/doc/scipy/dev/governance.html).
SciPy's governance document itself was adapted from the
[Jupyter/IPython project\'s governance
document](https://github.com/jupyter/governance/blob/master/governance.md)
and [NumPy\'s governance
document](https://github.com/numpy/numpy/blob/main/doc/source/dev/governance/governance.rst).

## License

This document is available under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.
