---
title: Maintainers
layout: page
permalink: /about/governance/maintainers/
---

* Do not remove this line (it will not be displayed)
{:toc}

ns-3 maintainers have responsibility and authority to oversee different portions of the codebase. Maintainers work with ns-3 contributors to marshal in patches or extensions to their respective modules. They also participate in timely code reviews, bug fixing, and respond to questions on mailing lists, pertaining to their respective modules.

Maintainers coordinate with other maintainers for patches that have larger scope than their particular module. Maintainers may also, in the future, help us with platform-specific issues (e.g., ns-3 Windows maintainer) or packaging.

If you would like to serve as an ns-3 maintainer, please first get involved
in source code review and bug fixing of the parts of the simulator that you
are interested in.  The core maintenance team may then approach you at some
point about possibly moving to a role of maintainer.  We have several modules
missing current/active maintainers (see below).

# Current maintainers

The lead maintainer is Tom Henderson.  Additional maintainers include:

* Eduardo Almeida
* Abhijith Anilkumar
* Stefano Avallone
* Peter Barnes
* Biljana Bojovic
* Sebastien Deronne
* Ameya Deshpande
* Gabriel Ferreira
* Alberto Gallegos
* Federico Guerra
* Vivek Jain
* Tommaso Pecorella
* Mishal Shah
* Mohit Tahiliani

# Module maintainers

The current ns-3 maintainers, by module or topic, include:

[@abhijithanilkumar]: https://gitlab.com/abhijithanilkumar
[Abhijith Anilkumar]: mailto:abhijithabhayam@gmail.com

[Alberto Gallegos]: mailto:alramonet@is.tokushima-u.ac.jp
[@shattered.feelings]: https://gitlab.com/shattered.feelings

[Ameya Deshpande]: mailto:ameyanrd@outlook.com
[@ameyanrd]: https://gitlab.com/ameyanrd

[Biljana Bojovic]: mailto:bbojovic@cttc.es
[@biljkus]: https://gitlab.com/biljkus

[Eduardo Almeida]: mailto:enmsa@outlook.pt
[@edalm]: https://gitlab.com/edalm

[Federico Guerra]: mailto:fedwar@gmail.com
[@FedericoGuerra]: https://gitlab.com/FedericoGuerra

[Gabriel Ferreira]: mailto:gabrielcarvfer@gmail.com
[@Gabrielcarvfer]: https://gitlab.com/Gabrielcarvfer

[Mishal Shah]: mailto:shahmishal1998@gmail.com
[@mishal23]: https://gitlab.com/mishal23

[Mohit Tahiliani]: mailto:tahiliani.nitk@gmail.com
[@mohittahiliani]: https://gitlab.com/mohittahiliani

[Peter Barnes]: mailto:barnes26@llnl.gov
[@pdbj]: https://gitlab.com/pdbj

[Sebastien Deronne]: mailto:sebastien.deronne@gmail.com
[@sderonne]: https://gitlab.com/sderonne

[Stefano Avallone]: mailto:stavallo@unina.it
[@stavallo]: https://gitlab.com/stavallo

[Tom Henderson]: mailto:tomh@tomh.org
[@tomhenderson]: https://gitlab.com/tomhenderson

[Tommaso Pecorella]: mailto:tpecorella@mac.com
[@tommypec]: https://gitlab.com/tommypec

[Vivek Jain]: mailto:jain.vivek.anand@gmail.com
[@Vivek-anand-jain]: https://gitlab.com/Vivek-anand-jain

| Module | Person(s) | GitLab Handle |
| ------ | --------- | ------------- |
| antenna | [Biljana Bojovic] | [@biljkus] |
| aodv | None | None |
| applications | None | None |
| bake build system | [Tom Henderson] | [@tomhenderson] |
| bridge | None | None |
| buildings | [Biljana Bojovic] | [@biljkus] |
| click | None | None |
| CMake build system | [Gabriel Ferreira] | [@Gabrielcarvfer] |
| config-store | None | None |
| core | [Peter Barnes] | [@pdbj] |
| csma | None | None |
| csma-layout | None | None |
| dsdv | None | None |
| dsr | None | None |
| energy | [Alberto Gallegos] | [@shattered.feelings] |
| fd-net-device | None | None |
| flow-monitor | [Tommaso Pecorella](mailto:tommaso.pecorella@unifi.it) | [@tommypec] |
| internet (IP, ICMP, UDP) | [Tommaso Pecorella] | [@tommypec] |
| internet (routing) | [Tom Henderson] | [@tomhenderson] |
| internet (TCP) | [Mohit Tahiliani] and [Vivek Jain] | [@mohittahiliani] and [@Vivek-anand-jain] |
| internet-apps | [Tommaso Pecorella] | [@tommypec] |
| lr-wpan | [Alberto Gallegos] and [Tommaso Pecorella] | [@shattered.feelings] and [@tommypec] |
| lte | [Biljana Bojovic] | [@biljkus] |
| mesh | None | None |
| mobility | None | None |
| mpi | [Peter Barnes] | [@pdbj] |
| netanim | None | None |
| network | None | None |
| nix-vector | [Ameya Deshpande] | [@ameyanrd] |
| olsr | None | None |
| openflow | None | None |
| point-to-point | None | None |
| point-to-point-layout | None | None |
| propagation | None | None |
| sixlowpan | [Tommaso Pecorella] | [@tommypec] |
| spectrum | [Biljana Bojovic] | [@biljkus] |
| stats | None | None |
| tap-bridge | None | None |
| test framework | None | None |
| tools | None | None |
| topology-read | [Tommaso Pecorella](mailto:tommaso.pecorella@unifi.it) | [@tommypec] |
| traffic-control | [Stefano Avallone] and [Mohit Tahiliani] | [@stavallo] and [@mohittahiliani] |
| uan | [Federico Guerra] | [@FedericoGuerra] |
| virtual-net-device | None | None |
| visualizer (pyviz) | None | None |
| wifi | [Sebastien Deronne] and [Stefano Avallone] | [@sderonne] and [@stavallo] |
| wimax | None | None |
| zigbee | [Alberto Gallegos] | [@shattered.feelings] |
| Python bindings | [Gabriel Ferreira] | [@Gabrielcarvfer] |
| tutorial | [Tom Henderson] | [@tomhenderson] |
| manuals | [Tom Henderson] and [Peter Barnes] | [@tomhenderson] and [@pdbj] |
| Doxygen | [Peter Barnes] | [@pdbj] |
| wiki | [Tom Henderson] | [@tomhenderson] |
| app store | [Abhijith Anilkumar] and [Mishal Shah] | [@abhijithanilkumar] and [@mishal23] |
| coding style and static analysis | [Eduardo Almeida] | [@edalm] |

# Past maintainers

* John Abraham
* Zoraze Ali
* Kirill Andreev
* Ghada Badawy
* Nicola Baldo
* Raj Bhattacharjea
* Pavel Boyko
* Junling Bu
* Elena Buchatskaya
* Daniel Camara
* Gustavo Carneiro
* Yufei Cheng
* Ankit Deepak
* Craig Dowell
* Tom Goff
* Budiarto Herman
* Mohamed Amine Ismail
* Sam Jansen
* Konstantinos Katsaros
* Joe Kopena
* Alexander Krotov
* Flavio Kubota
* Mathieu Lacage
* Daniel Lertpratchya
* Faker Moatamri
* Vedran Miletic
* Marco Miozzo
* Hemanth Narra
* Natale Patriciello
* Josh Pelkey
* Alina Quereilhac
* Getachew Redieteab
* Manuel Requena
* Matias Richart
* George Riley
* Lalith Suresh
* Brian Swenson
* Adrian S. W. Tam
* Hajime Tazaki
* Cristiano Tapparello
* Frederic Urbani
* Mitch Watrous
* Florian Westphal
* Dizhi Zhou
