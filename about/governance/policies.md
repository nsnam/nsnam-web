---
title: Policies
layout: page
permalink: /about/governance/policies/
---

# Code of conduct

The ns-3 project follows the [Contributor Covenant](https://www.contributor-covenant.org/version/2/1/code_of_conduct/).  Enforcement is the responsibility of the [Steering Council](/about/governance/other/); please refer any concerns to that group.

# ns-3-users guidelines for posting

* Available on the [wiki](/wiki/Ns-3-users-guidelines-for-posting/).

# ns-developers guidelines for posting

* Available on the [wiki](/wiki/Ns-developers-guidelines-for-posting/).

