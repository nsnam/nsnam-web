---
title: Council Meeting Minutes
layout: page
permalink: /about/governance/council/minutes/
---

# September 20, 2024

<b>Attendees:</b> Eduardo Almeida, Biljana Bojovic, Tom Henderson, Mohit Tahiliani

Absent: Peter Barnes

Minutes taken by Tom Henderson

## Agenda

1. Finalize bylaws and review draft website revisions
2. Discuss choice of code-of-conduct policy
3. Select steering council chair

## Minutes

- We graciously accepted Eduardo's volunteering to be steering council chair. Other roles (Lead Maintainer, Treasurer, Community Manager) were agreed via email during the interim.

- On the draft website, we discussed elevating 'Steering Council' to a separate page, renaming 'Consortium' to 'Former Consortium' and doing more work to make clear that it is historic.

- We agreed on code of conduct (<a href="https://www.contributor-covenant.org/version/2/1/code_of_conduct/">Contributor Covenant</a>) and on how enforcement is listed on the webpage.

- Tom will make various small web page updates that were discussed in the meeting over the weekend, and organize a final review next week, with goal of making it live possibly by next Friday.

- We agreed to hold all future council meetings as open meetings (announced to community) and only hold private meetings when circumstances dictate.  We will also post meeting minutes.

- We had discussions about how to catalyze more industry involvement and fix some of the bigger picture issues with ns-3 usability/learning curve and new model development.  Mohit revived his idea about having bi-monthly industrial (possibly also academic) seminar/discussion.  He will explore starting this with one such session before year's end.  Tom suggested maintaining a slide deck that we can share to outsiders about priorities and activities we are trying to promote.  Discussion will continue on this in other forums like the WNS3 2025 planning meeting that is upcoming (early Oct.).

- We agreed to create a named rule like 'Industry Liaison' for Mohit

- Tom is working on migrating the ns-3 app store out of a University of Washington office to commercial hosting, and we plan to move the main web site out of Ga. Tech.  Mohit said that some students could possibly help with webmaster and content improvement duties.  Eduardo recommends a container-driven workflow for managing the website and will help when the time comes.

# August 9, 2024


<b>Attendees:</b> Tom Henderson, Biljana Bojovic, Eduardo Almeida, Mohit Tahiliani

Absent: Peter Barnes

Minutes taken by Tom Henderson

## Background
The ns-3 Steering Council is a new group of ns-3 maintainers formed as part of a new
governance structure for the ns-3 open source project. Five maintainers (above) volunteered to form
the initial steering council. The draft bylaws are available in a GitLab.com merge request.

## Agenda

1) WNS3 2024 discussion and 2025 next steps
2) formalizing the new governance model and bylaws
3) policy documents needed in addition to bylaws
4) any other business

## Minutes

We first discussed the WNS3 2024 outcome and plans for WNS3 2025 This was a brainstorming
discussion. Tom shared his goal of focusing the annual meeting on improving ns-3 (bringing people
into the project if possible, orienting talks and papers towards improvements or validations/benchmarks
of ns-3). We have some leftover consortium funding to fund some travel grants, and could raise more.
Eduardo mentioned that perhaps companies could be invited to give talks on how they use ns-3 and
what changes they would like to see. He said there was a good company talk about this in WNS3 2023,
and Tom mentioned two such talks (MaxLinear and CableLabs) in 2024. Past efforts to have an
industrial paper track have failed-- perhaps invited talks are the way to go here. Tom also pointed out
that it would be nice to mentor a paper or talk or contribution from students who start to become active
in ns-3 maintenance.
Alberto previously mentioned prioritizing remaining ns-3 consortium funds towards maintainer travel
grants for next year, and Tom said that he would ask maintainers about their plans.
We have a tentative date (third week of August) and venue (Kansai area of Japan) for next year's
meeting. We can work on announcing a call for papers. We discussed the need for a new TPC co-chair
(since the discussion, Tommaso Pecorella has accepted an invitation). We need to decide on whether to
rename the event, and work on the agenda and call for papers if we want it to be different from this
year's workshop. Tom proposed a small committee for this, consisting of the TPC co-chairs, Alberto,
Eric Gamess, and himself. Others are welcome to join if interested.

We discussed the governance document, and Eduardo said it was in pretty good shape regarding the
decoupling of exact policies from the governance structure. Regarding code-of-conduct, we agreed to
look at the samples that people circulated, and again, Biljana and Eduardo expressed views that
something simple and possibly standardized on other projects would be nice.
Tom mentioned that he intended to publish the governance and currently filled and vacant roles on the
website in September. We discussed the need to possibly advertise the availability of roles like
community manager. We will probably need to name a Treasurer. We did not talk about steering council
chair role.

Mohit had a new idea about organizing a bi-monthly industry webinar where we invite companies to
talk about how they use ns-3 (generally), what modules they use, etc. He had two companies in mind to
start. This led to discussion that we could possibly catalyze some mentored student projects around
these ideas (possibly GSoC projects) and get industry involved to help with mentoring and with
requirements.

Finally, Biljana advocated for the restoration of published development roadmaps. Tom mentioned that we
used to do this but they fell out of date and were not followed and became a bit useless but we could try
to revive -- possibly as part of release manager duties at the beginning of a cycle.

The next meeting will be in early September. The agenda will be the selection of designated officers
for some of the following roles:
* Lead Maintainer
* Release Manager
* Steering Council Chair
* Treasurer
* Community Manager

We also plan to discuss finalizing the bylaws and a code of conduct policy, and the posting of
governance plan documents on the main web site.

